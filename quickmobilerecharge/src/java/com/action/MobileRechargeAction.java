package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.MobileRechargeDao;
import com.model.PaymentsDao;
import com.opencsv.CSVReader;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.Contact;
import com.persistance.CsvFileInfo;
import com.persistance.MRGroupName;
import com.persistance.MobileMoneyHistory;
import com.persistance.MobileRechargeDebit;
import com.persistance.MobileRechargeHistory;
import com.persistance.MobileRechargeTransactionHistory;
import com.persistance.UserInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class MobileRechargeAction extends ActionSupport implements Runnable {

    /**
     * @return the mobileRechargeThreadRunning
     */
    public static boolean isMobileRechargeThreadRunning() {
        return mobileRechargeThreadRunning;
    }

    /**
     * @param aMobileRechargeThreadRunning the mobileRechargeThreadRunning to
     * set
     */
    public static void setMobileRechargeThreadRunning(boolean aMobileRechargeThreadRunning) {
        mobileRechargeThreadRunning = aMobileRechargeThreadRunning;
    }

    private String phone;
    private String trid;
    private Integer type;
    private Double amount;
    private String pin;
    private String operator;
    private String pinStatus;
    private String resellerId;
    private String uID;
    private String allChildReseller;
    //
    private Integer mobileRechargeId;
    private Integer mobileRechargeDebitId;
    private Integer mrTransactionHistoryId;
    private Integer contactId;
    private String contactName;
    private String contactNumber;
    //
    private String groupName;
    private Integer mrGNameId;
    private String groupFormString;
    //    
    private String phoneArray;
    private Integer typeArray;
    private Integer amountArray;
    private Integer modemArray;
    //   
    private String egPhone;
    private String egType;
    private String egAmount;
    private String egModem;
    //    
    private File uploadFile;
    private String uploadFileFileName;
    private String uploadFilePath;
    //
    private String clickedDate;
    private String showMrType;
    //    
    private String fieldName;
    private String fieldValue;
    //    
    private String mes;
    private String messageString;
    private String messageColor;
    //    
    private CommonList commonList = null;
    private Connection connection = null;
    //    
    private String apiAccountID = "01114612230";
    private String apiAccountIDPassword = "123123";
    //    
    private String apiBalanceCheckCommand = "1";
    //    
    private String userBalanceRechargeCommand = "2";
    //    
    private String userBalanceRechargeStatusCheckCommand = "3";
    //
    private String[] csvInfoArray = null;
    private List<CsvFileInfo> csvFileInfoList = null;
    private List<MRGroupName> mrGroupInfoList = null;
    private List<MRGroupName> singleMRGroupInfoList = null;
    private List<Contact> contactInfoList = null;
    private List<Contact> cnInfoList = null;
    //    
    private List<MobileRechargeHistory> lastMRechargeHInfoList = null;
    private List<MobileRechargeHistory> allMRechargeHInfoList = null;
    private List<MobileRechargeHistory> allMRHInfoByAdmin = null;
    private List<MobileRechargeHistory> singleMRHInfoList = null;
    private List<MobileRechargeHistory> mrHByDateList = null;
    private List<MobileRechargeDebit> currentMrBalInfoList = null;
    private List<MobileRechargeTransactionHistory> mrPaymentReceiveInfoList = null;
    private List<MobileRechargeTransactionHistory> singleMrPaymentReceiveInfoList = null;
    private List<MobileRechargeTransactionHistory> mrPaymentMadeInfoList = null;
    private List<MobileRechargeTransactionHistory> singleMrPaymentMadeInfoList = null;
    //    
    private List<MobileMoneyHistory> lastMMoneyHInfoList = null;
    private List<MobileMoneyHistory> mmHByDateList = null;
    private MobileMoneyAction mobileMoneyActionInfo = null;
    private PaymentsDao paymentsDao = null;
    //    
    private MobileRechargeDao mobileRechargeDao = null;
    private ResellerAction resellerAction = null;
    private List<UserInfo> resellerByParentList = null;
    //
    private static volatile boolean mobileRechargeThreadRunning = false;
    private volatile boolean isMRThreadRunning;

    /**
     * *************************************************************************
     *********************** Mobile Recharge Thread Part
     *
     * @return
     */
    public String startMobileRechargeThread() {

        Thread thread = new Thread(this);

        setMobileRechargeThreadRunning(true);

        if (isMobileRechargeThreadRunning() == true) {

            thread.start();

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                setMobileRechargeThreadRunning(false);
                System.out.println(e);
            }
            messageColor = "success";
            messageString = "Mobile Recharge Successfully Started";
            return SUCCESS;
        } else {
            messageColor = "danger";
            messageString = "Mobile Recharge Start Failed";
            return ERROR;
        }
    }

    public String stopMobileRechargeThread() {

        setMobileRechargeThreadRunning(false);

        if (isMobileRechargeThreadRunning() == false) {
            messageColor = "success";
            messageString = "Mobile Recharge Successfully Stopped";
            return SUCCESS;
        } else {
            messageColor = "danger";
            messageString = "Mobile Recharge Stop Failed";
            return ERROR;
        }
    }

    public String mrThreadButtonUpdate() {
        setIsMRThreadRunning(isMobileRechargeThreadRunning());
        return SUCCESS;
    }

    @Override
    public void run() {

        while (isMobileRechargeThreadRunning() == true) {

            String apiUrl = "http://anypay.my/software/apiintegration/commands.php?";

            if (connection == null) {
                connection = DBConnection.getMySqlConnection();
            }

            if (commonList == null) {
                commonList = new CommonList();
            }

            if (connection != null) {

                List<MobileRechargeHistory> watingInfo = commonList.getMobileRechargeWatingStatusInfo(connection);

                if (!watingInfo.isEmpty()) {

                    for (MobileRechargeHistory mrh : watingInfo) {

                        PreparedStatement ps = null;

                        List<MobileRechargeDebit> userCurBal = commonList.mrCurBal(connection, mrh.getUserInfo().getUserId());

                        try {

                            String apiBalanceCheckXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + apiBalanceCheckCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword></Data>";

                            String apiBalanceCheckXmlResponse = communicationWithServer(apiUrl, apiBalanceCheckXml);

//                            System.out.println("apiBalanceCheckXmlResponse = " + apiBalanceCheckXmlResponse);
                            Double response[] = responseRedear(apiBalanceCheckXmlResponse);

                            if (response != null) {

//                            System.out.println("api bal check status = " + response[0]);
//                            System.out.println("api bal = " + response[1]);
                                // Api Balance Check
                                if ((response[0] == 0) && (response[1] > mrh.getGivenBalance())) {

                                    for (MobileRechargeDebit mrd : userCurBal) {

                                        // User Balance Check
                                        if (mrd.getMrCurrentBalance() > mrh.getGivenBalance()) {

                                            String userBalanceRechargeXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + userBalanceRechargeCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword><OperatorCode>" + mrh.getMobileOperatorCodeInfo().getOperatorCode() + "</OperatorCode><ReloadNumber>" + mrh.getReceiver() + "</ReloadNumber><Amt>" + mrh.getGivenBalance() + "</Amt><OrderReqId>" + mrh.getTrid() + "</OrderReqId></Data>";
                                            String userBalanceRechargeXmlResponse = communicationWithServer(apiUrl, userBalanceRechargeXml);

//                                        System.out.println("userBalanceRechargeXmlResponse = " + userBalanceRechargeXmlResponse);
                                            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                                            InputSource src = new InputSource();
                                            src.setCharacterStream(new StringReader(userBalanceRechargeXmlResponse));
                                            Document doc = builder.parse(src);

                                            String userBalanceRechargeSentStatus = doc.getElementsByTagName("Response").item(0).getTextContent();

                                            String sentStatus = userBalanceRechargeSentStatus.substring(8, 20);
//                                        System.out.println(sentStatus);

                                            if (sentStatus.equalsIgnoreCase("Request Sent")) {

                                                String mrStatusQuery = " UPDATE mobile_recharge_history "
                                                        + " SET active_status =  'S' "
                                                        + " WHERE mobile_recharge_id = '" + mrh.getMobileRechargeId() + "' ";

                                                ps = connection.prepareStatement(mrStatusQuery);
                                                ps.executeUpdate();

                                            } else {
//                                            System.out.println("mr recharge not sent");
                                            }
                                        } else {
//                                        System.out.println("User Balance Not Enough");
                                        }
                                    }
                                } else if ((response[0] > 0)) {
//                                System.out.println("Api Balance Not Enough !");
                                } else {
//                                System.out.println("Api Balance Not Enough2 !");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
//                    System.out.println("mr wating status id not found");
                }
            }

//            System.out.println("MR_thread_running");
            run(apiUrl);
        }
    }

    public void run(String apiUrl) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (paymentsDao == null) {
            paymentsDao = new PaymentsDao();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            List<MobileRechargeHistory> mrSentInfo = commonList.getMobileRechargeSentStatusInfo(connection);

            if (!mrSentInfo.isEmpty()) {

                for (MobileRechargeHistory mrh : mrSentInfo) {

                    PreparedStatement ps = null;

                    try {

                        String userBalanceRechargeStatusCheckXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + userBalanceRechargeStatusCheckCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword><OrderReqId>" + mrh.getTrid() + "</OrderReqId></Data>";

                        String userBalanceRechargeStatusCheckXmlResponse = communicationWithServer(apiUrl, userBalanceRechargeStatusCheckXml);

//                        System.out.println("userBalanceRechargeStatusCheckXmlResponse = " + userBalanceRechargeStatusCheckXmlResponse);
                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                        InputSource src = new InputSource();

                        src.setCharacterStream(new StringReader(userBalanceRechargeStatusCheckXmlResponse));

                        Document doc = builder.parse(src);

                        int userBalanceRechargeStatus = Integer.parseInt(doc.getElementsByTagName("RespStatus").item(0).getTextContent());

                        String userBalanceRechargeResponseDescription = doc.getElementsByTagName("RespDesc").item(0).getTextContent();

//                        System.out.println(userBalanceRechargeStatus);
//                        System.out.println(userBalanceRechargeResponseDescription);
                        if ((userBalanceRechargeStatus == 0) && (userBalanceRechargeResponseDescription.equals("Success"))) {

                            String userBalanceRechargeTxnID = doc.getElementsByTagName("TxnID").item(0).getTextContent();

                            String userBalanceRechargeOrderReqId = doc.getElementsByTagName("OrderReqId").item(0).getTextContent();

//                            System.out.println(userBalanceRechargeTxnID);
//                            System.out.println(userBalanceRechargeOrderReqId);
                            String mrStatusQuery = " UPDATE "
                                    + " mobile_recharge_history "
                                    + " SET "
                                    + " active_status =  'Y',"
                                    + " tnxid = '" + userBalanceRechargeTxnID + "' "
                                    + " WHERE mobile_recharge_id = '" + mrh.getMobileRechargeId() + "' ";

                            ps = connection.prepareStatement(mrStatusQuery);
                            int status = ps.executeUpdate();

                            if (status > 0) {
                                paymentsDao.updateMRBalParent(connection, mrh.getUserInfo().getUserId(), mrh.getGivenBalance());
                            }

                        } else if ((userBalanceRechargeStatus == 100) && (userBalanceRechargeResponseDescription.equalsIgnoreCase("InProcess"))) {

                            String mrStatusQuery = " UPDATE mobile_recharge_history "
                                    + " SET active_status =  'S' "
                                    + " WHERE mobile_recharge_id = '" + mrh.getMobileRechargeId() + "' ";

                            ps = connection.prepareStatement(mrStatusQuery);
                            ps.executeUpdate();
                        } else if ((userBalanceRechargeStatus == 101) && (userBalanceRechargeResponseDescription.equalsIgnoreCase("Refunded"))) {

                            String mrStatusQuery = " UPDATE mobile_recharge_history "
                                    + " SET active_status =  'S' "
                                    + " WHERE mobile_recharge_id = '" + mrh.getMobileRechargeId() + "' ";

                            ps = connection.prepareStatement(mrStatusQuery);
                            ps.executeUpdate();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
//                System.out.println("mr sent status id not found");
            }
        }
    }

    public String refundMobileRecharge() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (paymentsDao == null) {
            paymentsDao = new PaymentsDao();
        }

        if (connection != null) {

            String apiUrl = "http://anypay.my/software/apiintegration/commands.php?";

            List<MobileRechargeHistory> mrInfo = commonList.mobileRechargeRefund(connection, trid);

            if (!mrInfo.isEmpty()) {

                for (MobileRechargeHistory mrh : mrInfo) {

                    List<MobileRechargeDebit> userCurBal = commonList.mrCurBal(connection, mrh.getUserInfo().getUserId());

                    PreparedStatement ps = null;

                    try {

                        String apiBalanceCheckXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + apiBalanceCheckCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword></Data>";
                        String apiBalanceCheckXmlResponse = communicationWithServer(apiUrl, apiBalanceCheckXml);

                        Double response[] = responseRedear(apiBalanceCheckXmlResponse);

                        if (response != null) {

                            if ((response[0] == 0) && (response[1] > mrh.getGivenBalance())) {

                                for (MobileRechargeDebit mrd : userCurBal) {

                                    if (mrd.getMrCurrentBalance() > mrh.getGivenBalance()) {

                                        String userBalanceRechargeXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + userBalanceRechargeCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword><OperatorCode>" + mrh.getMobileOperatorCodeInfo().getOperatorCode() + "</OperatorCode><ReloadNumber>" + mrh.getReceiver() + "</ReloadNumber><Amt>" + mrh.getGivenBalance() + "</Amt><OrderReqId>" + mrh.getTrid() + "</OrderReqId></Data>";
                                        String userBalanceRechargeXmlResponse = communicationWithServer(apiUrl, userBalanceRechargeXml);

                                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                                        InputSource src = new InputSource();
                                        src.setCharacterStream(new StringReader(userBalanceRechargeXmlResponse));
                                        Document doc = builder.parse(src);

                                        String userBalanceRechargeSentStatus = doc.getElementsByTagName("Response").item(0).getTextContent();
                                        String sentStatus = userBalanceRechargeSentStatus.substring(8, 20);

                                        if (sentStatus.equalsIgnoreCase("Request Sent")) {

                                            String userBalanceRechargeStatusCheckXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + userBalanceRechargeStatusCheckCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword><OrderReqId>" + mrh.getTrid() + "</OrderReqId></Data>";
                                            String userBalanceRechargeStatusCheckXmlResponse = communicationWithServer(apiUrl, userBalanceRechargeStatusCheckXml);

                                            DocumentBuilder bld = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                                            InputSource source = new InputSource();
                                            source.setCharacterStream(new StringReader(userBalanceRechargeStatusCheckXmlResponse));
                                            Document document = bld.parse(source);
                                            int userBalanceRechargeStatus = Integer.parseInt(document.getElementsByTagName("RespStatus").item(0).getTextContent());
                                            String userBalanceRechargeResponseDescription = document.getElementsByTagName("RespDesc").item(0).getTextContent();

                                            if ((userBalanceRechargeStatus == 0) && (userBalanceRechargeResponseDescription.equals("Success"))) {

                                                String userBalanceRechargeTxnID = document.getElementsByTagName("TxnID").item(0).getTextContent();

                                                // String userBalanceRechargeOrderReqId = document.getElementsByTagName("OrderReqId").item(0).getTextContent();
                                                // txnid status success after successfully send money
                                                //                                            
                                                String mrStatusQuery = " UPDATE "
                                                        + " mobile_recharge_history "
                                                        + " SET "
                                                        + " active_status =  'Y',"
                                                        + " tnxid = '" + userBalanceRechargeTxnID + "' "
                                                        + " WHERE mobile_recharge_id = '" + mrh.getMobileRechargeId() + "' ";

                                                ps = connection.prepareStatement(mrStatusQuery);
                                                int status = ps.executeUpdate();
                                                // reseller balance reduce when it success
                                                if (status > 0) {
                                                    int chk = paymentsDao.updateMRBalParent(connection, mrh.getUserInfo().getUserId(), mrh.getGivenBalance());
                                                    if (chk > 0) {
                                                        messageColor = "success";
                                                        messageString = "Balance successfully refunded";
                                                    }
                                                }
                                            } else if ((userBalanceRechargeStatus == 100) && (userBalanceRechargeResponseDescription.equalsIgnoreCase("InProcess"))) {
                                                // in process status
                                                String mrStatusQuery = " UPDATE mobile_recharge_history "
                                                        + " SET active_status =  'S' "
                                                        + " WHERE mobile_recharge_id = '" + mrh.getMobileRechargeId() + "' ";
                                                ps = connection.prepareStatement(mrStatusQuery);
                                                int chkd = ps.executeUpdate();
                                                if (chkd > 0) {
                                                    messageColor = "danger";
                                                    messageString = "Refund in process";
                                                }
                                            } else if ((userBalanceRechargeStatus == 101) && (userBalanceRechargeResponseDescription.equalsIgnoreCase("Refunded"))) {
                                                // refunded status
                                                String mrStatusQuery = " UPDATE mobile_recharge_history "
                                                        + " SET active_status =  'S' "
                                                        + " WHERE mobile_recharge_id = '" + mrh.getMobileRechargeId() + "' ";
                                                ps = connection.prepareStatement(mrStatusQuery);
                                                int chked = ps.executeUpdate();
                                                if (chked > 0) {
                                                    messageColor = "danger";
                                                    messageString = "Refund in process";
                                                }
                                            }
                                        }
                                    } else {
                                        messageColor = "danger";
                                        messageString = "Insufficient resellers balance";
                                    }
                                }
                            } else {
                                messageColor = "danger";
                                messageString = "Insufficient balance";
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Invalid password/id/IP address.";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return SUCCESS;
    }

    public String manuallyUpdateStatus() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (paymentsDao == null) {
            paymentsDao = new PaymentsDao();
        }

        if (connection != null) {

            PreparedStatement ps = null;

            List<MobileRechargeHistory> mrInfo = commonList.mobileRechargeRefund(connection, trid);

            if (!mrInfo.isEmpty()) {

                for (MobileRechargeHistory mrh : mrInfo) {

                    List<MobileRechargeDebit> userCurBal = commonList.mrCurBal(connection, mrh.getUserInfo().getUserId());

                    for (MobileRechargeDebit mrd : userCurBal) {

                        if (mrd.getMrCurrentBalance() > mrh.getGivenBalance()) {

                            try {

                                String mrStatusQuery = " UPDATE "
                                        + " mobile_recharge_history "
                                        + " SET "
                                        + " active_status =  'Y' "
                                        + " WHERE "
                                        + " trid = '" + mrh.getTrid() + "' ";

                                ps = connection.prepareStatement(mrStatusQuery);
                                int status = ps.executeUpdate();

                                if (status > 0) {
                                    int chk = paymentsDao.updateMRBalParent(connection, mrh.getUserInfo().getUserId(), mrh.getGivenBalance());
                                    if (chk > 0) {
                                        messageColor = "success";
                                        messageString = "Balance Status successfully updated";
                                    } else {
                                        messageColor = "danger";
                                        messageString = "Balance Status update failed";
                                    }
                                } else {
                                    messageColor = "danger";
                                    messageString = "Balance Status update failed";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Insufficient resellers balance";
                        }
                    }
                }
            } else {
                messageColor = "danger";
                messageString = "Operator problem";
            }
        }

        return SUCCESS;
    }

    public static void main(String[] args) {

//        654313444321
//        66206cd20ec78d34eae37a3ae38d496a 5833e65658e87308c28e77fd8d4ce4de
//                5833e65658e87308c28e77fd8d4ce4de
        try {
            MobileRechargeAction m = new MobileRechargeAction();
            m.balChk();
//            m.childResellerByParent("razu");
//            m.bdac();

//            String userBalanceRechargeXmlResponse = "<Response>Status: Request Sent, Reload No.: 0102974775, Operator: Digi, Amount: MYR5.0, Balance: MYR95.00</Response>";
//
//            System.out.println("userBalanceRechargeXmlResponse = " + "<Response>Status: Request Sent, Reload No.: 0102974775, Operator: Digi, Amount: MYR5.0, Balance: MYR95.00</Response>");
//
//            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//
//            InputSource src = new InputSource();
//
//            src.setCharacterStream(new StringReader(userBalanceRechargeXmlResponse));
//
//            Document doc = builder.parse(src);
//
//            String reloadStatus = doc.getElementsByTagName("Response").item(0).getTextContent();
//
//            System.out.println(reloadStatus.length());
//
//            String sta = reloadStatus.substring(0, 6);
//
//            String sta1 = reloadStatus.substring(8, 20);
//
//            if (sta1.equalsIgnoreCase("Request Sent")) {
//                System.out.println(sta);
//                System.out.println(sta1);
//            } else {
//                System.out.println("not sent");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String bdac() {

        String d = "";

        try {

            String apiUrl = "http://eezee2load.com/eApi";

            System.out.println(apiUrl);

//            String apiBalanceCheckXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + apiBalanceCheckCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword></Data>";
//
//            String apiBalanceCheckXmlResponse = communicationWithServer(apiUrl, apiBalanceCheckXml);
//            System.out.println("apiBalanceCheckXmlResponse = " + apiBalanceCheckXmlResponse);
//
//            Integer response[] = responseRedear(apiBalanceCheckXmlResponse);
//            d = response[0];
//            System.out.println("api bal = " + response[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return d;
    }

    private Double balChk() {
        Double d = 0.0;
        try {

            String apiUrl = "http://anypay.my/software/apiintegration/commands.php?";
            String apiBalanceCheckXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Data><Command>" + apiBalanceCheckCommand + "</Command><AccountId>" + apiAccountID + "</AccountId><ApiPassword>" + apiAccountIDPassword + "</ApiPassword></Data>";
            String apiBalanceCheckXmlResponse = communicationWithServer(apiUrl, apiBalanceCheckXml);
            System.out.println("apiBalanceCheckXmlResponse = " + apiBalanceCheckXmlResponse);

            Double response[] = responseRedear(apiBalanceCheckXmlResponse);
            if (response != null) {
                d = response[0];
                System.out.println("api bal = " + response[1]);
            } else {
                System.out.println("Invalid/blank IP address");
            }

        } catch (Exception e) {
        }
        return d;
    }

    public String communicationWithServer(String url, String requestXml) {

        String totalStringInfo = "";

        StringBuilder stringBuffer = new StringBuilder();

        try {

            URL oracle = new URL(url);

            URLConnection con = oracle.openConnection();

            // specify that we will send output and accept input
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setConnectTimeout(20000);  // long timeout, but not infinite
            con.setReadTimeout(20000);
            con.setUseCaches(false);
            con.setDefaultUseCaches(false);
            // tell the web server what we are sending
            con.setRequestProperty("Content-Type", "text/xml");

            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(requestXml);
            writer.flush();
            writer.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                //System.out.println(inputLine);
                stringBuffer.append(inputLine);
            }

            totalStringInfo = stringBuffer.toString();

            in.close();

        } catch (IOException ex) {
            ex.printStackTrace();
            totalStringInfo = "Error78";
        }

        return totalStringInfo;
    }

    public Double[] responseRedear(String apiBalanceCheckXmlResponse) {

        Double balanceCheckResponse[] = new Double[2];

        try {

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(apiBalanceCheckXmlResponse));
            Document doc = builder.parse(src);

            String respStatus = doc.getElementsByTagName("RespStatus").item(0).getTextContent();
            if ((Integer.parseInt(respStatus)) == 0) {
                balanceCheckResponse[0] = Double.parseDouble(respStatus);
                balanceCheckResponse[1] = Double.parseDouble(doc.getElementsByTagName("Balance").item(0).getTextContent());
            } else {
                balanceCheckResponse = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return balanceCheckResponse;
    }

    public String childResellerByParent(String pID) {

        String tempID = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectChldID = " SELECT user_id FROM user_info "
                        + " WHERE parent_id = '" + pID + "' ";

                ps = connection.prepareStatement(selectChldID);
                rs = ps.executeQuery();

                while (rs.next()) {
                    tempID = rs.getString("user_id");
                }

            } catch (Exception e) {
                System.out.println(e);
            }

//            System.out.println(allChildReseller);
            if (tempID.isEmpty() || tempID == null) {
                allChildReseller += tempID;
                System.out.println(allChildReseller);
            } else {
                allChildReseller += childResellerByParent(pID);
                System.out.println(allChildReseller);
            }
        }

        return allChildReseller;
    }

    /**
     * *************************************************************************
     * ********************** MR List Methods
     *
     * @return
     */
    public List<MobileRechargeDebit> currentMRBalance(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setCurrentMrBalInfoList(commonList.mrCurBal(connection, uId));
        }

        return currentMrBalInfoList;
    }

    public List<MobileRechargeHistory> lastMRechargeHInfo() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setLastMRechargeHInfoList(commonList.getLastMobileRechargeHistoryInfo(connection, userID));
        }

        return lastMRechargeHInfoList;
    }

    public List<MobileRechargeHistory> allMRechargeHInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMRechargeHInfoList(commonList.getAllMobileRechargeHistoryInfo(connection, uId));
        }

        return allMRechargeHInfoList;
    }

    public List<UserInfo> getResellerByParent(String rID) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setResellerByParentList(commonList.getResellerInfoByParent(connection, rID));
        }

        return resellerByParentList;
    }

    public List<MobileRechargeHistory> getSingleMRHInfo(Integer MrID) {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMRHInfoList(commonList.getSingleMobileRechargeHistoryInfo(connection, MrID));
        }

        return singleMRHInfoList;
    }

    public List<MRGroupName> getMRGroupName(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrGroupInfoList(commonList.getAllMRGroupName(connection, uId));
        }

        return mrGroupInfoList;
    }

    public List<MRGroupName> getSingleMRGroupInfo(String uId, Integer mrGNId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMRGroupInfoList(commonList.getSingleMRGroupInfo(connection, uId, mrGNId));
        }

        return singleMRGroupInfoList;
    }

    public List<Contact> getContactList() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setContactInfoList(commonList.getAllContact(connection, userID));
        }

        return contactInfoList;
    }

    public List<MobileRechargeHistory> getMRHistoryByDate(String uId, String cMrDate) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrHByDateList(commonList.getAllMRHistoryByDate(connection, uId, cMrDate));
        }

        return mrHByDateList;
    }

    public List<MobileMoneyHistory> getMMHistoryByDate(String uId, String cMmDate) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMmHByDateList(commonList.getAllMMHistoryByDate(connection, uId, cMmDate));
        }

        return mmHByDateList;
    }

    public List<MobileRechargeTransactionHistory> getMRPaymentReceiveHistory(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrPaymentReceiveInfoList(commonList.getMRPaymentReceiveHistory(connection, uId));
        }

        return mrPaymentReceiveInfoList;
    }

    public List<MobileRechargeTransactionHistory> getMRPaymentMadeHistory(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrPaymentMadeInfoList(commonList.getMRPaymentMadeHistory(connection, uId));
        }

        return mrPaymentMadeInfoList;
    }

    public List<MobileRechargeTransactionHistory> getSingleMRPReceiveHInfo(String uId, Integer mrthId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMrPaymentReceiveInfoList(commonList.getSingleMRPaymentReceiveHistory(connection, uId, mrthId));
        }

        return singleMrPaymentReceiveInfoList;
    }

    public List<MobileRechargeTransactionHistory> getSingleMRPMadeHInfo(String uId, Integer mrthId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMrPaymentMadeInfoList(commonList.getSingleMRPaymentMadeHistory(connection, uId, mrthId));
        }

        return singleMrPaymentMadeInfoList;
    }

    /**
     * *************************************************************************
     ****************** Action Come from home page
     *
     * @return
     */
    public String showLastMRHInHome() {

        lastMRechargeHInfo();

        return SUCCESS;
    }

    public String sendMRFromHome() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

            if (cPin.size() != 0) {

                String trid = "";
                if (orderReqIdGenerator() != null) {
                    trid = orderReqIdGenerator();
                } else {
                    trid = "100000000001";
                }

                int checked = mobileRechargeDao.saveMobileRecharge(connection, userID, phone, type, amount, operator, trid, userID);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Tk. " + amount + " has been transferred to mobile number " + phone + ".";
                } else {
                    messageColor = "danger";
                    messageString = "Tk. " + amount + " transfer failed to mobile number " + phone + ".";
                }
            } else {
                messageColor = "danger";
                messageString = "Invalid Pin !";
            }
        }

        currentMRBalance(userID);

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ***************************** Mobile Recharge Methods
     *
     * @return
     */
    public String showLastMRHistory() {

        lastMRechargeHInfo();

        return SUCCESS;
    }

    public String sendMobileRecharge() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

            if (cPin.size() != 0) {

                List<MobileRechargeDebit> mrCurBal = currentMRBalance(userID);

                for (MobileRechargeDebit rechargeDebit : mrCurBal) {

                    if (rechargeDebit.getMrCurrentBalance() > amount) {

                        String trid = "";
                        if (orderReqIdGenerator() != null) {
                            trid = orderReqIdGenerator();
                        } else {
                            trid = "100000000001";
                        }

                        int checked = mobileRechargeDao.saveMobileRecharge(connection, userID, phone, type, amount, operator, trid, userID);

                        if (checked > 0) {
                            messageColor = "success";
                            messageString = "Tk. " + amount + " has been transferred to mobile number " + phone + ".";
                        } else {
                            messageColor = "danger";
                            messageString = "Tk. " + amount + " transfer failed to mobile number " + phone + ".";
                        }
                    } else {
                        messageColor = "danger";
                        messageString = "Oops! There are not enough balance in the mobile recharge";
                    }
                }
            } else {
                messageColor = "danger";
                messageString = "Invalid Pin !";
            }
        }

        currentMRBalance(userID);

        return SUCCESS;
    }

    public String sendMalaysianMobileRecharge() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

            if (cPin.size() != 0) {

                List<MobileRechargeDebit> mrCurBal = currentMRBalance(userID);

                for (MobileRechargeDebit rechargeDebit : mrCurBal) {

                    if (rechargeDebit.getMrCurrentBalance() > amount) {

                        String trid = "";
                        if (orderReqIdGenerator() != null) {
                            trid = orderReqIdGenerator();
                        } else {
                            trid = "100000000001";
                        }

                        int checked = mobileRechargeDao.saveMobileRecharge(connection, userID, phone, type, amount, operator, trid, userID);

                        if (checked > 0) {
                            messageColor = "success";
                            messageString = "Tk. " + amount + " has been transferred to mobile number " + phone + ".";
                        } else {
                            messageColor = "danger";
                            messageString = "Tk. " + amount + " transfer failed to mobile number " + phone + ".";
                        }
                    } else {
                        messageColor = "danger";
                        messageString = "Oops! There are not enough balance in the mobile recharge";
                    }
                }
            } else {
                messageColor = "danger";
                messageString = "Invalid Pin !";
            }
        }

        currentMRBalance(userID);

        return SUCCESS;
    }

    private String orderReqIdGenerator() {

        String orderReqId = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT "
                        + " LPAD((MAX(A.OR_ID)+1),12,'0') "
                        + " AS ORDER_ID "
                        + " FROM "
                        + " (SELECT CONVERT(SUBSTRING(trid, 1,12),UNSIGNED INTEGER) "
                        + " AS OR_ID FROM mobile_recharge_history)A ";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();
                while (rs.next()) {
                    orderReqId = rs.getString("ORDER_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return orderReqId;
    }

    /**
     * *************************************************************************
     * ***************************** Mobile Recharge By Group Save Contacts
     *
     * @return
     */
    public String showGroup() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        getMRGroupName(userID);

        return SUCCESS;
    }

    public String mrGroupView() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        List l = getSingleMRGroupInfo(userID, mrGNameId);

        if (l.size() > 0) {
            getMRGroupName(userID);
            messageString = "1";
            return SUCCESS;
        } else {
            getMRGroupName(userID);
            messageColor = "danger";
            messageString = "Please relogin.";
            return ERROR;
        }
    }

    public String viewEditMRGroup() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        List l = getSingleMRGroupInfo(userID, mrGNameId);

        if (l.size() > 0) {
            getMRGroupName(userID);
            return SUCCESS;
        } else {
            getMRGroupName(userID);
            return ERROR;
        }
    }

    public String saveGroup() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        String[] formStringArray = null;
        String[] splitStringArray = null;

        String phoneArray = "";
        Integer typeArray = 0;
        Double amountArray = 0d;
        Integer modemArray = 0;

        PreparedStatement psGName = null;
        PreparedStatement psFk = null;
        ResultSet rs = null;

        if (connection != null) {

            if ((groupFormString != null) && (groupName != null)) {
                if ((!groupFormString.isEmpty()) && (!groupName.isEmpty())) {

                    formStringArray = groupFormString.split("/rd/");

                    if (formStringArray != null) {

                        int status = 0;

                        String mrGroupNameInsertStatement = " INSERT INTO "
                                + " mr_group_name "
                                + " ( "
                                + " mr_group_name, "
                                + " user_id, "
                                + " insert_by, "
                                + " insert_date "
                                + " ) VALUES ( "
                                + " ?,?,?, CURRENT_TIMESTAMP "
                                + " ) ";

                        try {

                            psGName = connection.prepareStatement(mrGroupNameInsertStatement);

                            psGName.setString(1, groupName);
                            psGName.setString(2, userID);
                            psGName.setString(3, userID);

                            status = psGName.executeUpdate();

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (psGName != null) {
                                try {
                                    psGName.close();
                                } catch (Exception ex) {
                                    System.out.println(ex);
                                }
                            }
                        }

                        int fk = 0;
                        int l = 0;
                        if (status != 0) {
                            for (l = 0; l < formStringArray.length; l++) {
                                if (l == 0) {
                                    String id = " SELECT MAX(mr_gname_id) FROM mr_group_name ";
                                    try {
                                        psFk = connection.prepareStatement(id);
                                        rs = psFk.executeQuery();

                                        while (rs.next()) {
                                            fk = rs.getInt(1);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        if (psFk != null) {
                                            try {
                                                psFk.close();
                                            } catch (Exception ex) {
                                                System.out.println(ex);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        for (int i = 0; i < formStringArray.length; i++) {
                            if (formStringArray[i] != null) {
                                if (!formStringArray[i].isEmpty()) {

                                    phoneArray = "";
                                    typeArray = 0;
                                    amountArray = 0d;
                                    modemArray = 0;

                                    splitStringArray = formStringArray[i].split("/fd/");

                                    if (splitStringArray != null) {

                                        phoneArray = splitStringArray[0];
                                        typeArray = Integer.parseInt(splitStringArray[1]);
                                        amountArray = Double.parseDouble(splitStringArray[2]);
                                        modemArray = Integer.parseInt(splitStringArray[3]);

                                        if ((phoneArray != null) && (amountArray != 0)) {

                                            int checked = 0;

                                            if ((!phoneArray.isEmpty()) && (amountArray != 0)) {

                                                if (status != 0) {
                                                    checked = mobileRechargeDao.saveMRMGroup(connection, phoneArray, typeArray, amountArray, modemArray, userID, fk);
                                                }
                                                if (checked > 0) {
                                                    messageColor = "success";
                                                    messageString = "Mobile Recharge group created successfully.";
                                                } else {
                                                    messageColor = "danger";
                                                    messageString = "Mobile Recharge group create failed !";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return SUCCESS;
    }

    public String editMRGroups() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            String ph[] = null;
            String p = "";
            String mOperator = "";
            ph = egPhone.split(",");

            String typ[] = null;
            String ty = "";
            typ = egType.split(",");

            String amt[] = null;
            String am = "";
            amt = egAmount.split(",");

            String mod[] = null;
            String mo = "";
            mod = egModem.split(",");

            int mrUpdateStatus = 0;
            int mrDeleteStatus = 0;

            PreparedStatement mrUpdatePs = null;
            PreparedStatement mrDeletePs = null;

            String mrGroupUpdateStatement = " UPDATE "
                    + " mr_group_name MRGN "
                    + " SET "
                    + " MRGN.mr_group_name = '" + groupName + "', "
                    + " MRGN.update_by = '" + userID + "', "
                    + " MRGN.update_date = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " MRGN.user_id = '" + userID + "' "
                    + " AND "
                    + " MRGN.mr_gname_id = '" + mrGNameId + "' ";

            String mrGroupDeleteStatement = " DELETE FROM "
                    + " mr_group_info "
                    + " WHERE "
                    + " user_id = '" + userID + "' "
                    + " AND "
                    + " mr_gname_id = '" + mrGNameId + "' ";

            if (ph != null) {

                try {

                    mrUpdatePs = connection.prepareStatement(mrGroupUpdateStatement);
                    mrUpdateStatus = mrUpdatePs.executeUpdate();

                    if (mrUpdateStatus != 0) {
                        mrDeletePs = connection.prepareStatement(mrGroupDeleteStatement);
                        mrDeleteStatus = mrDeletePs.executeUpdate();
                    }

                } catch (Exception e) {
                    System.out.println(e);
                    mrUpdateStatus = 0;
                    mrDeleteStatus = 0;
                } finally {
                    if (mrUpdatePs != null) {
                        try {
                            mrUpdatePs.close();
                        } catch (Exception ex) {
                            System.out.println(ex);
                        }
                    }

                    if (mrDeletePs != null) {
                        try {
                            mrDeletePs.close();
                        } catch (Exception ex) {
                            System.out.println(ex);
                        }
                    }
                }

                for (int i = 0; i < ph.length; i++) {
                    p = ph[i];
                    for (int ii = 0; ii <= p.length(); ii++) {
                        String phoneNo = p;
                        if (phoneNo.length() > 11) {
                            phoneNo = phoneNo.substring(phoneNo.length() - 11);
                        }
                        mOperator = phoneNo.substring(0, 3);
                        if (mOperator.equals("019")) {
                            mOperator = "Banglalink";
                        } else if (mOperator.equals("018")) {
                            mOperator = "Robi";
                        } else if (mOperator.equals("017")) {
                            mOperator = "Grameen Phone";
                        } else if (mOperator.equals("016")) {
                            mOperator = "Airtel";
                        } else if (mOperator.equals("015")) {
                            mOperator = "Teletalk";
                        } else {
                            return ERROR;
                        }

                        for (int tp = 0; tp < typ.length; tp++) {
                            ty = typ[i];
                        }

                        for (int a = 0; a < amt.length; a++) {
                            am = amt[i];
                        }

                        for (int m = 0; m < mod.length; m++) {
                            mo = mod[i];
                        }
                    }

                    int checked = 0;

                    if (mrUpdateStatus != 0 && mrDeleteStatus != 0) {
                        checked = mobileRechargeDao.updateMRGroupInfo(connection, p.trim(), ty.trim(), am.trim(), mo.trim(), userID, mrGNameId);
                    }

                    if (checked > 0) {
                        messageColor = "success";
                        messageString = "Group updated.";
                    } else {
                        messageColor = "danger";
                        messageString = "Group update failed.";
                    }
                }
            } else {
                return ERROR;
            }
        } else {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String deleteMRGroup() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            int checked = mobileRechargeDao.deleteMRGroupInfo(connection, mrGNameId, userID);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Group Deleted Successfully.";
            } else {
                messageColor = "danger";
                messageString = "Group delete failed";
            }
        }

        return SUCCESS;
    }

    public String sendMRByGroupPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        List l = getSingleMRGroupInfo(userID, mrGNameId);

        if (l.size() > 0) {
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    public String sendMobileRechargeByGroup() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            String ph[] = null;
            String p = "";
            String mOperator = "";
            ph = egPhone.split(",");

            String typ[] = null;
            String ty = "";
            typ = egType.split(",");

            String amt[] = null;
            String am = "";
            amt = egAmount.split(",");

            String mod[] = null;
            String mo = "";
            mod = egModem.split(",");

            if (ph != null) {
                for (int i = 0; i < ph.length; i++) {
                    p = ph[i];
                    for (int ii = 0; ii <= p.length(); ii++) {
                        String phoneNo = p;
                        if (phoneNo.length() > 11) {
                            phoneNo = phoneNo.substring(phoneNo.length() - 11);
                        }
                        mOperator = phoneNo.substring(0, 3);
                        if (mOperator.equals("019")) {
                            mOperator = "Banglalink";
                        } else if (mOperator.equals("018")) {
                            mOperator = "Robi";
                        } else if (mOperator.equals("017")) {
                            mOperator = "Grameen Phone";
                        } else if (mOperator.equals("016")) {
                            mOperator = "Airtel";
                        } else if (mOperator.equals("015")) {
                            mOperator = "Teletalk";
                        } else {
                            return ERROR;
                        }

                        for (int tp = 0; tp < typ.length; tp++) {
                            ty = typ[i];
                        }

                        for (int a = 0; a < amt.length; a++) {
                            am = amt[i];
                        }

                        for (int m = 0; m < mod.length; m++) {
                            mo = mod[i];
                        }
                    }

                    List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

                    if (cPin.size() != 0) {

                        int checked = mobileRechargeDao.sendMRByGroup(connection, userID, p.trim(), mOperator.trim(), ty.trim(), am.trim(), mo.trim(), userID);

                        if (checked > 0) {
                            messageString = "1";
                        } else {
                            messageString = "failed";
                        }
                    } else {
                        messageString = "2";
                    }
                }
            } else {
                return ERROR;
            }
        } else {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String confirmSendMobileRechargeByGroup() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (mobileMoneyActionInfo == null) {
            mobileMoneyActionInfo = new MobileMoneyAction();
        }

        if ((messageString != "") && (messageString.equals("1"))) {
            messageColor = "success";
            messageString = "Mobile Recharge has been transferred by group";
            mes = "show";
        } else if ((messageString != "") && (messageString.equals("2"))) {
            messageColor = "danger";
            messageString = "Invalid Pin !";
            mes = "show";
        } else {
            messageColor = "danger";
            messageString = "Mobile Recharge transfer failed by group";
            mes = "show";
        }

        lastMRechargeHInfo();

        lastMMoneyHInfoList = mobileMoneyActionInfo.lastMMoneyHInfo();

        currentMRBalance(userID);

        return SUCCESS;
    }

    public String showContactsList() {

        getContactList();

        return SUCCESS;
    }

    public String contactsAdd() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            int checked = mobileRechargeDao.saveContacts(connection, contactName, contactNumber, userID, userID);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Contact Added Successfully.";
            } else {
                messageColor = "danger";
                messageString = "Contact Add Failed !";
            }
        }

        return SUCCESS;
    }

    public String contactsGet() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setCnInfoList(commonList.getContactNumber(connection, contactId, userID));
        }

        return SUCCESS;
    }

    public String contactsGetHome() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setCnInfoList(commonList.getContactNumber(connection, contactId, userID));
        }

        return SUCCESS;
    }

    public String contactsDelete() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (connection != null) {

            int checked = mobileRechargeDao.deleteContacts(connection, contactId, userID);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Contact Deleted Successfully.";
            } else {
                messageColor = "danger";
                messageString = "Contact Delete Failed !";
            }
        }

        return SUCCESS;
    }

    public String getMRHistoryByParent() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        allMRechargeHInfo(resellerId);

        return SUCCESS;
    }

    public String viewMRechargeHDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

//        allMRechargeHInfo(userID);
        getSingleMRHInfo(mobileRechargeId);

        return SUCCESS;
    }

    public String searchMobileRechargeHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMRechargeHInfoList(commonList.searchMobileRechargeHistoryInfo(connection, userID, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String viewMRPReceiveHDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getSingleMRPReceiveHInfo(userID, mrTransactionHistoryId);

        return SUCCESS;
    }

    public String searchMRPaymentsReceiveHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrPaymentReceiveInfoList(commonList.searchMRPaymentReceiveHistory(connection, userID, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String viewMRPMadeHDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getSingleMRPMadeHInfo(userID, mrTransactionHistoryId);

        return SUCCESS;
    }

    public String searchMRPaymentsMadeHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrPaymentMadeInfoList(commonList.searchMRPaymentMadeHistory(connection, userID, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ***************************** Mobile Recharge By CSV File
     *
     * @return
     */
    public String uploadCsvFile() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        try {

            SimpleDateFormat formatter = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ss");

            uploadFilePath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/uploadedFile");

            File file = new File(uploadFilePath + uploadFileFileName);

            if (file.exists()) {
                file.renameTo(new File(uploadFilePath + formatter.format(new Date()) + "_" + file.getName()));
            }

            File fileToCreate = new File(uploadFilePath, uploadFileFileName);
            FileUtils.copyFile(uploadFile, fileToCreate);

            System.out.println("Src File name: " + uploadFile);
            System.out.println("Dst File name: " + uploadFileFileName);
            System.out.println("Path: " + uploadFilePath);

            csvFileReader(uploadFilePath + "/" + uploadFileFileName);

        } catch (IOException e) {
            System.out.println(e);
            return ERROR;
        }

        return SUCCESS;
    }

    public void csvFileReader(String filePath) {

        CsvFileInfo csvFileInfo = null;
        CSVReader csvReader = null;

        try {

            csvFileInfoList = new ArrayList<CsvFileInfo>();
            csvReader = new CSVReader(new FileReader(filePath));

            List<?> content = csvReader.readAll();

            for (Object object : content) {

                csvInfoArray = (String[]) object;

                csvFileInfo = new CsvFileInfo();

                csvFileInfo.setMobileNumber(csvInfoArray[0]);
                csvFileInfo.setType(Integer.parseInt(csvInfoArray[1]));
                csvFileInfo.setAmount(Integer.parseInt(csvInfoArray[2]));
                csvFileInfo.setModemNumber(Integer.parseInt(csvInfoArray[3]));

                csvFileInfoList.add(csvFileInfo);
            }

            csvReader.close();
        } catch (IOException e) {
            System.out.println(e);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }

    public String sendMRByCsvFile() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (mobileRechargeDao == null) {
            mobileRechargeDao = new MobileRechargeDao();
        }

        if (mobileMoneyActionInfo == null) {
            mobileMoneyActionInfo = new MobileMoneyAction();
        }

        if (connection != null) {

            int checked = 0;

            for (CsvFileInfo obj : csvFileInfoList) {

                String phoneNo = obj.getMobileNumber();

                if (phoneNo.length() > 11) {
                    phoneNo = phoneNo.substring(phoneNo.length() - 11);
                }

                String mOperator = phoneNo.substring(0, 3);

                if (mOperator.equals("019")) {
                    mOperator = "Banglalink";
                } else if (mOperator.equals("018")) {
                    mOperator = "Robi";
                } else if (mOperator.equals("017")) {
                    mOperator = "Grameen Phone";
                } else if (mOperator.equals("016")) {
                    mOperator = "Airtel";
                } else if (mOperator.equals("015")) {
                    mOperator = "Teletalk";
                } else {
                    messageColor = "danger";
                    messageString = "Invalid mobile operator";
                    return ERROR;
                }

                List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

                if (cPin.size() != 0) {

                    checked = mobileRechargeDao.saveCsvFileMR(connection, userID, obj.getMobileNumber(), mOperator, obj.getType(), obj.getAmount(), obj.getModemNumber(), userID);

                    if (checked > 0) {
                        messageColor = "success";
                        messageString = "Mobile Recharge has been transferred by CSV file";
                        mes = "show";
                    } else {
                        messageColor = "danger";
                        messageString = "Mobile Recharge transfer failed by CSV file";
                        mes = "show";
                    }
                } else {
                    messageColor = "danger";
                    messageString = "Invalid Pin !";
                    mes = "show";
                }
            }
        }

        lastMRechargeHInfo();

        currentMRBalance(userID);

        lastMMoneyHInfoList = mobileMoneyActionInfo.lastMMoneyHInfo();

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ************** Action Come From Report
     *
     * @return
     */
    public String mrSummaryByDate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMRHistoryByDate(userID, clickedDate);

        return SUCCESS;
    }

    public String mmSummaryByDate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMMHistoryByDate(userID, clickedDate);

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * *********************** MR Page Methods
     *
     * @return
     */
    public String goSendMobileRechargePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (resellerAction == null) {
            resellerAction = new ResellerAction();
        }

        lastMRechargeHInfo();

        currentMRBalance(userID);

        setPinStatus(resellerAction.checkPinStatus(userID));

        return SUCCESS;
    }

    public String goSendMalaysiaMobileRecharge() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (resellerAction == null) {
            resellerAction = new ResellerAction();
        }

        lastMRechargeHInfo();

        currentMRBalance(userID);

        setPinStatus(resellerAction.checkPinStatus(userID));

        return SUCCESS;
    }

    public String goSendManageGroupPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMRGroupName(userID);

        return SUCCESS;
    }

    public String goSendFromCsvFilePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goMobileRechargeHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        allMRechargeHInfo(userID);

//        getResellerByParent(userID);
        return SUCCESS;
    }

    public String goMRPaymentsReceivedPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMRPaymentReceiveHistory(userID);

        return SUCCESS;
    }

    public String goMRPaymentsMadePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMRPaymentMadeHistory(userID);

        return SUCCESS;
    }

    public String goInternatePackagePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goAllMobileRechargeHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMRHInfoByAdmin(commonList.getAllMRHistoryInfoByAdmin(connection));
        }

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ********************** MR Setter and Getter Methods
     *
     * @return
     */
    /**
     * @return the lastMRechargeHInfoList
     */
    public List<MobileRechargeHistory> getLastMRechargeHInfoList() {
        return lastMRechargeHInfoList;
    }

    /**
     * @param lastMRechargeHInfoList the lastMRechargeHInfoList to set
     */
    public void setLastMRechargeHInfoList(List<MobileRechargeHistory> lastMRechargeHInfoList) {
        this.lastMRechargeHInfoList = lastMRechargeHInfoList;
    }

    /**
     * @return the allMRechargeHInfoList
     */
    public List<MobileRechargeHistory> getAllMRechargeHInfoList() {
        return allMRechargeHInfoList;
    }

    /**
     * @param allMRechargeHInfoList the allMRechargeHInfoList to set
     */
    public void setAllMRechargeHInfoList(List<MobileRechargeHistory> allMRechargeHInfoList) {
        this.allMRechargeHInfoList = allMRechargeHInfoList;
    }

    /**
     * @return the uploadFileFileName
     */
    public String getUploadFileFileName() {
        return uploadFileFileName;
    }

    /**
     * @param uploadFileFileName the uploadFileFileName to set
     */
    public void setUploadFileFileName(String uploadFileFileName) {
        this.uploadFileFileName = uploadFileFileName;
    }

    /**
     * @return the uploadFile
     */
    public File getUploadFile() {
        return uploadFile;
    }

    /**
     * @param uploadFile the uploadFile to set
     */
    public void setUploadFile(File uploadFile) {
        this.uploadFile = uploadFile;
    }

    /**
     * @return the uploadFilePath
     */
    public String getUploadFilePath() {
        return uploadFilePath;
    }

    /**
     * @param uploadFilePath the uploadFilePath to set
     */
    public void setUploadFilePath(String uploadFilePath) {
        this.uploadFilePath = uploadFilePath;
    }

    /**
     * @return the csvInfoArray
     */
    public String[] getCsvInfoArray() {
        return csvInfoArray;
    }

    /**
     * @param csvInfoArray the csvInfoArray to set
     */
    public void setCsvInfoArray(String[] csvInfoArray) {
        this.csvInfoArray = csvInfoArray;
    }

    /**
     * @return the csvFileInfoList
     */
    public List<CsvFileInfo> getCsvFileInfoList() {
        return csvFileInfoList;
    }

    /**
     * @param csvFileInfoList the csvFileInfoList to set
     */
    public void setCsvFileInfoList(List<CsvFileInfo> csvFileInfoList) {
        this.csvFileInfoList = csvFileInfoList;
    }

    /**
     * @return the lastMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getLastMMoneyHInfoList() {
        return lastMMoneyHInfoList;
    }

    /**
     * @param lastMMoneyHInfoList the lastMMoneyHInfoList to set
     */
    public void setLastMMoneyHInfoList(List<MobileMoneyHistory> lastMMoneyHInfoList) {
        this.lastMMoneyHInfoList = lastMMoneyHInfoList;
    }

    /**
     * @return the phoneArray
     */
    public String getPhoneArray() {
        return phoneArray;
    }

    /**
     * @param phoneArray the phoneArray to set
     */
    public void setPhoneArray(String phoneArray) {
        this.phoneArray = phoneArray;
    }

    /**
     * @return the typeArray
     */
    public Integer getTypeArray() {
        return typeArray;
    }

    /**
     * @param typeArray the typeArray to set
     */
    public void setTypeArray(Integer typeArray) {
        this.typeArray = typeArray;
    }

    /**
     * @return the modemArray
     */
    public Integer getModemArray() {
        return modemArray;
    }

    /**
     * @param modemArray the modemArray to set
     */
    public void setModemArray(Integer modemArray) {
        this.modemArray = modemArray;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the groupFormString
     */
    public String getGroupFormString() {
        return groupFormString;
    }

    /**
     * @param groupFormString the groupFormString to set
     */
    public void setGroupFormString(String groupFormString) {
        this.groupFormString = groupFormString;
    }

    /**
     * @return the clickedDate
     */
    public String getClickedDate() {
        return clickedDate;
    }

    /**
     * @param clickedDate the clickedDate to set
     */
    public void setClickedDate(String clickedDate) {
        this.clickedDate = clickedDate;
    }

    /**
     * @return the mrHByDateList
     */
    public List<MobileRechargeHistory> getMrHByDateList() {
        return mrHByDateList;
    }

    /**
     * @param mrHByDateList the mrHByDateList to set
     */
    public void setMrHByDateList(List<MobileRechargeHistory> mrHByDateList) {
        this.mrHByDateList = mrHByDateList;
    }

    /**
     * @return the mmHByDateList
     */
    public List<MobileMoneyHistory> getMmHByDateList() {
        return mmHByDateList;
    }

    /**
     * @param mmHByDateList the mmHByDateList to set
     */
    public void setMmHByDateList(List<MobileMoneyHistory> mmHByDateList) {
        this.mmHByDateList = mmHByDateList;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin the pin to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return the amountArray
     */
    public Integer getAmountArray() {
        return amountArray;
    }

    /**
     * @param amountArray the amountArray to set
     */
    public void setAmountArray(Integer amountArray) {
        this.amountArray = amountArray;
    }

    /**
     * @return the mrGroupInfoList
     */
    public List<MRGroupName> getMrGroupInfoList() {
        return mrGroupInfoList;
    }

    /**
     * @param mrGroupInfoList the mrGroupInfoList to set
     */
    public void setMrGroupInfoList(List<MRGroupName> mrGroupInfoList) {
        this.mrGroupInfoList = mrGroupInfoList;
    }

    /**
     * @return the mrGNameId
     */
    public Integer getMrGNameId() {
        return mrGNameId;
    }

    /**
     * @param mrGNameId the mrGNameId to set
     */
    public void setMrGNameId(Integer mrGNameId) {
        this.mrGNameId = mrGNameId;
    }

    /**
     * @return the singleMRGroupInfoList
     */
    public List<MRGroupName> getSingleMRGroupInfoList() {
        return singleMRGroupInfoList;
    }

    /**
     * @param singleMRGroupInfoList the singleMRGroupInfoList to set
     */
    public void setSingleMRGroupInfoList(List<MRGroupName> singleMRGroupInfoList) {
        this.singleMRGroupInfoList = singleMRGroupInfoList;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName the contactName to set
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the contactNumber
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * @param contactNumber the contactNumber to set
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     * @return the contactInfoList
     */
    public List<Contact> getContactInfoList() {
        return contactInfoList;
    }

    /**
     * @param contactInfoList the contactInfoList to set
     */
    public void setContactInfoList(List<Contact> contactInfoList) {
        this.contactInfoList = contactInfoList;
    }

    /**
     * @return the contactId
     */
    public Integer getContactId() {
        return contactId;
    }

    /**
     * @param contactId the contactId to set
     */
    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    /**
     * @return the cnInfoList
     */
    public List<Contact> getCnInfoList() {
        return cnInfoList;
    }

    /**
     * @param cnInfoList the cnInfoList to set
     */
    public void setCnInfoList(List<Contact> cnInfoList) {
        this.cnInfoList = cnInfoList;
    }

    /**
     * @return the mobileRechargeId
     */
    public Integer getMobileRechargeId() {
        return mobileRechargeId;
    }

    /**
     * @param mobileRechargeId the mobileRechargeId to set
     */
    public void setMobileRechargeId(Integer mobileRechargeId) {
        this.mobileRechargeId = mobileRechargeId;
    }

    /**
     * @return the singleMRHInfoList
     */
    public List<MobileRechargeHistory> getSingleMRHInfoList() {
        return singleMRHInfoList;
    }

    /**
     * @param singleMRHInfoList the singleMRHInfoList to set
     */
    public void setSingleMRHInfoList(List<MobileRechargeHistory> singleMRHInfoList) {
        this.singleMRHInfoList = singleMRHInfoList;
    }

    /**
     * @return the mobileRechargeDebitId
     */
    public Integer getMobileRechargeDebitId() {
        return mobileRechargeDebitId;
    }

    /**
     * @param mobileRechargeDebitId the mobileRechargeDebitId to set
     */
    public void setMobileRechargeDebitId(Integer mobileRechargeDebitId) {
        this.mobileRechargeDebitId = mobileRechargeDebitId;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the showMrType
     */
    public String getShowMrType() {
        return showMrType;
    }

    /**
     * @param showMrType the showMrType to set
     */
    public void setShowMrType(String showMrType) {
        this.showMrType = showMrType;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the egPhone
     */
    public String getEgPhone() {
        return egPhone;
    }

    /**
     * @param egPhone the egPhone to set
     */
    public void setEgPhone(String egPhone) {
        this.egPhone = egPhone;
    }

    /**
     * @return the egType
     */
    public String getEgType() {
        return egType;
    }

    /**
     * @param egType the egType to set
     */
    public void setEgType(String egType) {
        this.egType = egType;
    }

    /**
     * @return the egAmount
     */
    public String getEgAmount() {
        return egAmount;
    }

    /**
     * @param egAmount the egAmount to set
     */
    public void setEgAmount(String egAmount) {
        this.egAmount = egAmount;
    }

    /**
     * @return the egModem
     */
    public String getEgModem() {
        return egModem;
    }

    /**
     * @param egModem the egModem to set
     */
    public void setEgModem(String egModem) {
        this.egModem = egModem;
    }

    /**
     * @return the currentMrBalInfoList
     */
    public List<MobileRechargeDebit> getCurrentMrBalInfoList() {
        return currentMrBalInfoList;
    }

    /**
     * @param currentMrBalInfoList the currentMrBalInfoList to set
     */
    public void setCurrentMrBalInfoList(List<MobileRechargeDebit> currentMrBalInfoList) {
        this.currentMrBalInfoList = currentMrBalInfoList;
    }

    /**
     * @return the mrPaymentReceiveInfoList
     */
    public List<MobileRechargeTransactionHistory> getMrPaymentReceiveInfoList() {
        return mrPaymentReceiveInfoList;
    }

    /**
     * @param mrPaymentReceiveInfoList the mrPaymentReceiveInfoList to set
     */
    public void setMrPaymentReceiveInfoList(List<MobileRechargeTransactionHistory> mrPaymentReceiveInfoList) {
        this.mrPaymentReceiveInfoList = mrPaymentReceiveInfoList;
    }

    /**
     * @return the mrTransactionHistoryId
     */
    public Integer getMrTransactionHistoryId() {
        return mrTransactionHistoryId;
    }

    /**
     * @param mrTransactionHistoryId the mrTransactionHistoryId to set
     */
    public void setMrTransactionHistoryId(Integer mrTransactionHistoryId) {
        this.mrTransactionHistoryId = mrTransactionHistoryId;
    }

    /**
     * @return the singleMrPaymentReceiveInfoList
     */
    public List<MobileRechargeTransactionHistory> getSingleMrPaymentReceiveInfoList() {
        return singleMrPaymentReceiveInfoList;
    }

    /**
     * @param singleMrPaymentReceiveInfoList the singleMrPaymentReceiveInfoList
     * to set
     */
    public void setSingleMrPaymentReceiveInfoList(List<MobileRechargeTransactionHistory> singleMrPaymentReceiveInfoList) {
        this.singleMrPaymentReceiveInfoList = singleMrPaymentReceiveInfoList;
    }

    /**
     * @return the mrPaymentMadeInfoList
     */
    public List<MobileRechargeTransactionHistory> getMrPaymentMadeInfoList() {
        return mrPaymentMadeInfoList;
    }

    /**
     * @param mrPaymentMadeInfoList the mrPaymentMadeInfoList to set
     */
    public void setMrPaymentMadeInfoList(List<MobileRechargeTransactionHistory> mrPaymentMadeInfoList) {
        this.mrPaymentMadeInfoList = mrPaymentMadeInfoList;
    }

    /**
     * @return the singleMrPaymentMadeInfoList
     */
    public List<MobileRechargeTransactionHistory> getSingleMrPaymentMadeInfoList() {
        return singleMrPaymentMadeInfoList;
    }

    /**
     * @param singleMrPaymentMadeInfoList the singleMrPaymentMadeInfoList to set
     */
    public void setSingleMrPaymentMadeInfoList(List<MobileRechargeTransactionHistory> singleMrPaymentMadeInfoList) {
        this.singleMrPaymentMadeInfoList = singleMrPaymentMadeInfoList;
    }

    public List<String> getMobileRechargeWatingStatus() {

        List<String> watingIDList = new ArrayList<String>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mrStatusQuery = " SELECT mobile_recharge_id FROM mobile_recharge_history WHERE active_status = 'W' ";

                ps = connection.prepareStatement(mrStatusQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    String id = rs.getString("mobile_recharge_id");
                    watingIDList.add(id);
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return watingIDList;
    }

    /**
     * @return the apiAccountID
     */
    public String getApiAccountID() {
        return apiAccountID;
    }

    /**
     * @param apiAccountID the apiAccountID to set
     */
    public void setApiAccountID(String apiAccountID) {
        this.apiAccountID = apiAccountID;
    }

    /**
     * @return the apiAccountIDPassword
     */
    public String getApiAccountIDPassword() {
        return apiAccountIDPassword;
    }

    /**
     * @param apiAccountIDPassword the apiAccountIDPassword to set
     */
    public void setApiAccountIDPassword(String apiAccountIDPassword) {
        this.apiAccountIDPassword = apiAccountIDPassword;
    }

    /**
     * @return the apiBalanceCheckCommand
     */
    public String getApiBalanceCheckCommand() {
        return apiBalanceCheckCommand;
    }

    /**
     * @param apiBalanceCheckCommand the apiBalanceCheckCommand to set
     */
    public void setApiBalanceCheckCommand(String apiBalanceCheckCommand) {
        this.apiBalanceCheckCommand = apiBalanceCheckCommand;
    }

    /**
     * @return the userBalanceRechargeCommand
     */
    public String getUserBalanceRechargeCommand() {
        return userBalanceRechargeCommand;
    }

    /**
     * @param userBalanceRechargeCommand the userBalanceRechargeCommand to set
     */
    public void setUserBalanceRechargeCommand(String userBalanceRechargeCommand) {
        this.userBalanceRechargeCommand = userBalanceRechargeCommand;
    }

    /**
     * @return the userBalanceRechargeStatusCheckCommand
     */
    public String getUserBalanceRechargeStatusCheckCommand() {
        return userBalanceRechargeStatusCheckCommand;
    }

    /**
     * @param userBalanceRechargeStatusCheckCommand the
     * userBalanceRechargeStatusCheckCommand to set
     */
    public void setUserBalanceRechargeStatusCheckCommand(String userBalanceRechargeStatusCheckCommand) {
        this.userBalanceRechargeStatusCheckCommand = userBalanceRechargeStatusCheckCommand;
    }

    /**
     * @return the pinStatus
     */
    public String getPinStatus() {
        return pinStatus;
    }

    /**
     * @param pinStatus the pinStatus to set
     */
    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }

    /**
     * @return the resellerId
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * @param resellerId the resellerId to set
     */
    public void setResellerId(String resellerId) {
        this.resellerId = resellerId;
    }

    /**
     * @return the uID
     */
    public String getuID() {
        return uID;
    }

    /**
     * @param uID the uID to set
     */
    public void setuID(String uID) {
        this.uID = uID;
    }

    /**
     * @return the resellerByParentList
     */
    public List<UserInfo> getResellerByParentList() {
        return resellerByParentList;
    }

    /**
     * @param resellerByParentList the resellerByParentList to set
     */
    public void setResellerByParentList(List<UserInfo> resellerByParentList) {
        this.resellerByParentList = resellerByParentList;
    }

    /**
     * @return the allChildReseller
     */
    public String getAllChildReseller() {
        return allChildReseller;
    }

    /**
     * @param allChildReseller the allChildReseller to set
     */
    public void setAllChildReseller(String allChildReseller) {
        this.allChildReseller = allChildReseller;
    }

    /**
     * @return the isMRThreadRunning
     */
    public boolean isIsMRThreadRunning() {
        return isMRThreadRunning;
    }

    /**
     * @param isMRThreadRunning the isMRThreadRunning to set
     */
    public void setIsMRThreadRunning(boolean isMRThreadRunning) {
        this.isMRThreadRunning = isMRThreadRunning;
    }

    /**
     * @return the allMRHInfoByAdmin
     */
    public List<MobileRechargeHistory> getAllMRHInfoByAdmin() {
        return allMRHInfoByAdmin;
    }

    /**
     * @param allMRHInfoByAdmin the allMRHInfoByAdmin to set
     */
    public void setAllMRHInfoByAdmin(List<MobileRechargeHistory> allMRHInfoByAdmin) {
        this.allMRHInfoByAdmin = allMRHInfoByAdmin;
    }

    /**
     * @return the trid
     */
    public String getTrid() {
        return trid;
    }

    /**
     * @param trid the trid to set
     */
    public void setTrid(String trid) {
        this.trid = trid;
    }
}
