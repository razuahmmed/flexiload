package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.PaymentsDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.MobileMoneyDebit;
import com.persistance.MobileRechargeDebit;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class PaymentsAction extends ActionSupport {

    private String resellerId;
    private String loadType;
    private Double amount;
    private String amountType;
    private String pin;
    private String description;
    private String pinStatus;
    private String balReqID;
    //    
    private String messageString;
    private String messageColor;
    //    
    private CommonList commonList = null;
    private Connection connection = null;
    //    
    private List<UserInfo> paymentInfoList = null;
    private List<UserInfo> singleResallerInfoList = null;
    private List<MobileRechargeDebit> mrBalanceInfoList = null;
    private List<MobileMoneyDebit> mmBalanceInfoList = null;
    //    
    private PaymentsDao paymentsDao = null;
    private ResellerAction resellerAction = null;
    private MobileRechargeAction mobileRechargeAction = null;
    private MobileMoneyAction mobileMoneyAction = null;
    //    

    public List<UserInfo> getPaymentInfo(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setPaymentInfoList(commonList.getPaymentInfo(connection, pId));
        }

        return paymentInfoList;
    }

    public List<UserInfo> getSingleReseller(String pId, String rId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleResallerInfoList(commonList.getSingleResellerInfo(connection, pId, rId));
        }

        return singleResallerInfoList;
    }

    public List<MobileRechargeDebit> getMRBalance() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrBalanceInfoList(commonList.getSingMRBalance(connection, resellerId));
        }

        return mrBalanceInfoList;
    }

    public List<MobileMoneyDebit> getMMBalance() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMmBalanceInfoList(commonList.getSingMMBalance(connection, resellerId));
        }

        return mmBalanceInfoList;
    }

    public String getResellerTypeAndParent() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getSingleReseller(userID, resellerId);

        return SUCCESS;
    }

    public String getCurrentBalance() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (loadType.matches("MR")) {
            getMRBalance();
        } else if (loadType.matches("MM")) {
            getMMBalance();
        }

        return SUCCESS;
    }

    public String addReturnResellerBalance() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (paymentsDao == null) {
            paymentsDao = new PaymentsDao();
        }

        if (mobileRechargeAction == null) {
            mobileRechargeAction = new MobileRechargeAction();
        }

        if (mobileMoneyAction == null) {
            mobileMoneyAction = new MobileMoneyAction();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

            if (cPin.size() != 0) {

                List<MobileRechargeDebit> mrCurBal = mobileRechargeAction.currentMRBalance(userID);

                List<MobileMoneyDebit> mmCurBal = mobileMoneyAction.currentMMBalance(userID);

                if ((loadType.matches("MR")) && (amountType.matches("ADD"))) {

                    for (MobileRechargeDebit rechargeDebit : mrCurBal) {

                        if (rechargeDebit.getMrCurrentBalance() > amount) {

                            int checked = paymentsDao.addMRBalance(connection, resellerId, amount, amount, userID, description);

                            if (checked > 0) {
                                paymentsDao.updateMRBalParent(connection, userID, amount);
                                messageColor = "success";
                                messageString = "Tk. " + amount + " mobile recharge amount has been added to reseller " + resellerId + " from reseller " + userID + ". ";
                            } else {
                                messageColor = "danger";
                                messageString = "Tk. " + amount + " mobile recharge amount added failed to reseller " + resellerId + " from reseller " + userID + ". ";
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Oops! There are not enough balance in the mobile recharge";
                        }
                    }
                } else if ((loadType.matches("MR")) && (amountType.matches("RETURN"))) {

                    for (MobileRechargeDebit rechargeDebit : mrCurBal) {

                        if (rechargeDebit.getMrCurrentBalance() > amount) {

                            int checked = paymentsDao.returnMRBalance(connection, resellerId, amount, amount, userID, description);

                            if (checked > 0) {
                                paymentsDao.returnMRBalParent(connection, userID, amount);
                                messageColor = "success";
                                messageString = "Tk. " + amount + " mobile recharge amount has been returned to reseller " + userID + " from reseller " + resellerId + ". ";
                            } else {
                                messageColor = "danger";
                                messageString = "Tk. " + amount + " mobile recharge amount return failed to reseller " + userID + " from reseller " + resellerId + ". ";
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Oops! There are not enough balance in the mobile recharge";
                        }
                    }
                }

                if ((loadType.matches("MM")) && (amountType.matches("ADD"))) {

                    for (MobileMoneyDebit moneyDebit : mmCurBal) {

                        if (moneyDebit.getMmCurrentBalance() > amount) {

                            int checked = paymentsDao.addMMBalance(connection, resellerId, amount, amount, userID, description);

                            if (checked > 0) {
                                paymentsDao.addMMBalParent(connection, userID, amount);
                                messageColor = "success";
                                messageString = "Tk. " + amount + " mobile money amount has been added to reseller " + resellerId + " from reseller " + userID + ". ";
                            } else {
                                messageColor = "danger";
                                messageString = "Tk. " + amount + " mobile money amount added failed to reseller " + resellerId + " from reseller " + userID + ". ";
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Oops! There are not enough balance in the mobile money";
                        }
                    }
                } else if ((loadType.matches("MM")) && (amountType.matches("RETURN"))) {

                    for (MobileMoneyDebit moneyDebit : mmCurBal) {

                        if (moneyDebit.getMmCurrentBalance() > amount) {

                            int checked = paymentsDao.returnMMBalance(connection, resellerId, amount, amount, userID, description);

                            if (checked > 0) {
                                paymentsDao.returnMMBalParent(connection, userID, amount);
                                messageColor = "success";
                                messageString = "Tk. " + amount + " mobile money amount has been returned to reseller " + userID + " from reseller " + resellerId + ". ";
                            } else {
                                messageColor = "danger";
                                messageString = "Tk. " + amount + " mobile money amount return failed to reseller " + userID + " from reseller " + resellerId + ". ";
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Oops! There are not enough balance in the mobile money";
                        }
                    }
                }
            } else {
                messageColor = "danger";
                messageString = "Invalid Pin !";
            }
        }

        return SUCCESS;
    }

    public String balanceRequestResponse() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (paymentsDao == null) {
            paymentsDao = new PaymentsDao();
        }

        if (mobileRechargeAction == null) {
            mobileRechargeAction = new MobileRechargeAction();
        }

        if (mobileMoneyAction == null) {
            mobileMoneyAction = new MobileMoneyAction();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

            if (cPin.size() != 0) {

                List<MobileRechargeDebit> mrCurBal = mobileRechargeAction.currentMRBalance(userID);

                List<MobileMoneyDebit> mmCurBal = mobileMoneyAction.currentMMBalance(userID);

                if ((loadType.matches("MR")) && (amountType.matches("ADD"))) {

                    for (MobileRechargeDebit rechargeDebit : mrCurBal) {

                        if (rechargeDebit.getMrCurrentBalance() > amount) {

                            int checked = paymentsDao.addMRBalance(connection, resellerId, amount, amount, userID, description);

                            if (checked > 0) {

                                paymentsDao.updateMRBalParent(connection, userID, amount);

                                paymentsDao.requestBalanceStatusUpdate(connection, balReqID, userID);

                                messageColor = "success";
                                messageString = "Tk. " + amount + " mobile recharge amount has been added to reseller " + resellerId + " from reseller " + userID + ". ";
                            } else {
                                messageColor = "danger";
                                messageString = "Tk. " + amount + " mobile recharge amount added failed to reseller " + resellerId + " from reseller " + userID + ". ";
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Oops! There are not enough balance in the mobile recharge";
                        }
                    }
                }

                if ((loadType.matches("MM")) && (amountType.matches("ADD"))) {

                    for (MobileMoneyDebit moneyDebit : mmCurBal) {

                        if (moneyDebit.getMmCurrentBalance() > amount) {

                            int checked = paymentsDao.addMMBalance(connection, resellerId, amount, amount, userID, description);

                            if (checked > 0) {

                                paymentsDao.addMMBalParent(connection, userID, amount);

                                paymentsDao.requestBalanceStatusUpdate(connection, balReqID, userID);

                                messageColor = "success";
                                messageString = "Tk. " + amount + " mobile money amount has been added to reseller " + resellerId + " from reseller " + userID + ". ";
                            } else {
                                messageColor = "danger";
                                messageString = "Tk. " + amount + " mobile money amount added failed to reseller " + resellerId + " from reseller " + userID + ". ";
                            }
                        } else {
                            messageColor = "danger";
                            messageString = "Oops! There are not enough balance in the mobile money";
                        }
                    }
                }
            } else {
                messageColor = "danger";
                messageString = "Invalid Pin !";
            }
        }

        return SUCCESS;
    }

    public String showPaymentsInfo() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getPaymentInfo(userID);

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ******************Action From Admin
     *
     * @return
     */
    public String getUserSummary() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (loadType.matches("MR")) {
            getMRBalance();
        } else if (loadType.matches("MM")) {
            getMMBalance();
        }

        return SUCCESS;
    }

    public String goPaymentsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (resellerAction == null) {
            resellerAction = new ResellerAction();
        }

        setPinStatus(resellerAction.checkPinStatus(userID));

        List l = getPaymentInfo(userID);

        if (!l.isEmpty()) {
            messageString = getPaymentInfo(userID).get(0).getParentId();
            resellerId = resellerId;
            loadType = loadType;
        } else {
            messageString = "no reseller found";
        }

        return SUCCESS;
    }

    public String requestForPaymentPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (resellerAction == null) {
            resellerAction = new ResellerAction();
        }

        setPinStatus(resellerAction.checkPinStatus(userID));

        List l = getPaymentInfo(userID);

        if (!l.isEmpty()) {
            messageString = getPaymentInfo(userID).get(0).getParentId();
            resellerId = resellerId;
            loadType = loadType;
            balReqID = balReqID;
            amount = amount;
        } else {
            messageString = "no reseller found";
        }

        return SUCCESS;
    }

    /**
     * @return the singleResallerInfoList
     */
    public List<UserInfo> getSingleResallerInfoList() {
        return singleResallerInfoList;
    }

    /**
     * @param singleResallerInfoList the singleResallerInfoList to set
     */
    public void setSingleResallerInfoList(List<UserInfo> singleResallerInfoList) {
        this.singleResallerInfoList = singleResallerInfoList;
    }

    /**
     * @return the resellerId
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * @param resellerId the resellerId to set
     */
    public void setResellerId(String resellerId) {
        this.resellerId = resellerId;
    }

    /**
     * @return the loadType
     */
    public String getLoadType() {
        return loadType;
    }

    /**
     * @param loadType the loadType to set
     */
    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    /**
     * @return the mrBalanceInfoList
     */
    public List<MobileRechargeDebit> getMrBalanceInfoList() {
        return mrBalanceInfoList;
    }

    /**
     * @param mrBalanceInfoList the mrBalanceInfoList to set
     */
    public void setMrBalanceInfoList(List<MobileRechargeDebit> mrBalanceInfoList) {
        this.mrBalanceInfoList = mrBalanceInfoList;
    }

    /**
     * @return the mmBalanceInfoList
     */
    public List<MobileMoneyDebit> getMmBalanceInfoList() {
        return mmBalanceInfoList;
    }

    /**
     * @param mmBalanceInfoList the mmBalanceInfoList to set
     */
    public void setMmBalanceInfoList(List<MobileMoneyDebit> mmBalanceInfoList) {
        this.mmBalanceInfoList = mmBalanceInfoList;
    }

    /**
     * @return the amountType
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * @param amountType the amountType to set
     */
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    /**
     * @return the pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin the pin to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the paymentInfoList
     */
    public List<UserInfo> getPaymentInfoList() {
        return paymentInfoList;
    }

    /**
     * @param paymentInfoList the paymentInfoList to set
     */
    public void setPaymentInfoList(List<UserInfo> paymentInfoList) {
        this.paymentInfoList = paymentInfoList;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the pinStatus
     */
    public String getPinStatus() {
        return pinStatus;
    }

    /**
     * @param pinStatus the pinStatus to set
     */
    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }

    /**
     * @return the balReqID
     */
    public String getBalReqID() {
        return balReqID;
    }

    /**
     * @param balReqID the balReqID to set
     */
    public void setBalReqID(String balReqID) {
        this.balReqID = balReqID;
    }
}
