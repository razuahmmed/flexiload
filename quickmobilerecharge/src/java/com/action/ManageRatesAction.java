package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.ManageRateDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.ManageRateMaster;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class ManageRatesAction extends ActionSupport {

    private Integer manageRateId;
    private String rateName;
    private String operatorName;
    private String operatorCode;
    private String rateCommission;
    //    
    private String messageString;
    private String messageColor;
    //
    private CommonList commonList = null;
    private Connection connection = null;
    //    
    private List<ManageRateMaster> manageRateInfoList = null;
    private List<ManageRateMaster> oneManageRateInfoList = null;
    //
    private ManageRateDao manageRateDao = null;

    public List<ManageRateMaster> getManageRate(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setManageRateInfoList(commonList.getAllManageRate(connection, pId));
        }

        return manageRateInfoList;
    }

    public List<ManageRateMaster> getOneManageRate(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setOneManageRateInfoList(commonList.getOneManageRateInfo(connection, uId, manageRateId));
        }

        return oneManageRateInfoList;
    }

    public String showAllManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getManageRate(userID);

        return SUCCESS;
    }

    public String newManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (manageRateDao == null) {
            manageRateDao = new ManageRateDao();
        }

        if (connection != null) {

            int chkParent = manageRateDao.saveManageRateMaster(connection, rateName, userID, userID);

            if (chkParent > 0) {

                String opNameArray[] = null;
                String opCodeArray[] = null;
                String commArray[] = null;

                if (operatorName != null && operatorCode != null && rateCommission != null) {
                    opNameArray = operatorName.split(",");
                    opCodeArray = operatorCode.split(",");
                    commArray = rateCommission.split(",");
                }

                int chkChild = 0;
                for (int i = 0; i < opNameArray.length; i++) {
                    chkChild = manageRateDao.saveManageRateChild(connection, opNameArray[i].trim(), opCodeArray[i].trim(), commArray[i].trim());
                }

                if (chkChild > 0) {
                    messageColor = "success";
                    messageString = "Rate Plan created successfully.";
                } else {
                    messageColor = "danger";
                    messageString = "Rate Plan create failed !";
                }
            } else {
                messageColor = "danger";
                messageString = "Rate Plan create failed !";
            }
        }

        return SUCCESS;
    }

    public String manageRateDetailsView() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getOneManageRate(userID);

        getManageRate(userID);

        return SUCCESS;
    }

    public String updateManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (manageRateDao == null) {
            manageRateDao = new ManageRateDao();
        }

        if (connection != null) {

            int upMaster = manageRateDao.updateManageRateMaster(connection, rateName, userID, userID, manageRateId);

            if (upMaster > 0) {

                int childDlt = manageRateDao.deleteManageRateChild(connection, manageRateId);

                if (childDlt > 0) {

                    String opNameArray[] = null;
                    String opCodeArray[] = null;
                    String commArray[] = null;

                    if (operatorName != null && operatorCode != null && rateCommission != null) {
                        opNameArray = operatorName.split(",");
                        opCodeArray = operatorCode.split(",");
                        commArray = rateCommission.split(",");
                    }

                    int chkChild = 0;
                    for (int i = 0; i < opNameArray.length; i++) {
                        chkChild = manageRateDao.updateManageRateChild(connection, opNameArray[i].trim(), opCodeArray[i].trim(), commArray[i].trim(), manageRateId);
                    }
                    if (chkChild > 0) {
                        messageColor = "success";
                        messageString = "Rate Plan updated successfully.";
                    } else {
                        messageColor = "danger";
                        messageString = "Rate Plan update failed !";
                    }
                } else {
                    messageColor = "danger";
                    messageString = "Rate Plan update failed !";
                }
            } else {
                messageColor = "danger";
                messageString = "Rate Plan update failed !";
            }
        }

        return SUCCESS;
    }

    public String deleteManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (manageRateDao == null) {
            manageRateDao = new ManageRateDao();
        }

        if (connection != null) {

            int checked = manageRateDao.deleteManageRate(connection, manageRateId, userID);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Rate Plan deleted successfully.";
            } else {
                messageColor = "danger";
                messageString = "Rate Plan delete failed !";
            }
        }

        return SUCCESS;
    }

    public String goManageRatesPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getManageRate(userID);

        return SUCCESS;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the manageRateId
     */
    public Integer getManageRateId() {
        return manageRateId;
    }

    /**
     * @param manageRateId the manageRateId to set
     */
    public void setManageRateId(Integer manageRateId) {
        this.manageRateId = manageRateId;
    }

    /**
     * @return the rateName
     */
    public String getRateName() {
        return rateName;
    }

    /**
     * @param rateName the rateName to set
     */
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the operatorName
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * @param operatorName the operatorName to set
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * @return the operatorCode
     */
    public String getOperatorCode() {
        return operatorCode;
    }

    /**
     * @param operatorCode the operatorCode to set
     */
    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    /**
     * @return the rateCommission
     */
    public String getRateCommission() {
        return rateCommission;
    }

    /**
     * @param rateCommission the rateCommission to set
     */
    public void setRateCommission(String rateCommission) {
        this.rateCommission = rateCommission;
    }

    /**
     * @return the manageRateInfoList
     */
    public List<ManageRateMaster> getManageRateInfoList() {
        return manageRateInfoList;
    }

    /**
     * @param manageRateInfoList the manageRateInfoList to set
     */
    public void setManageRateInfoList(List<ManageRateMaster> manageRateInfoList) {
        this.manageRateInfoList = manageRateInfoList;
    }

    /**
     * @return the oneManageRateInfoList
     */
    public List<ManageRateMaster> getOneManageRateInfoList() {
        return oneManageRateInfoList;
    }

    /**
     * @param oneManageRateInfoList the oneManageRateInfoList to set
     */
    public void setOneManageRateInfoList(List<ManageRateMaster> oneManageRateInfoList) {
        this.oneManageRateInfoList = oneManageRateInfoList;
    }
}
