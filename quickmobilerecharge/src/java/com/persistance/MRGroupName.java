package com.persistance;

public class MRGroupName {

    private Integer mrGNameId;
    private String mrGroupName;
    private MRGroupInfo mrGroupInfo;
    private String insertBy;

    /**
     * @return the mrGNameId
     */
    public Integer getMrGNameId() {
        return mrGNameId;
    }

    /**
     * @param mrGNameId the mrGNameId to set
     */
    public void setMrGNameId(Integer mrGNameId) {
        this.mrGNameId = mrGNameId;
    }

    /**
     * @return the mrGroupName
     */
    public String getMrGroupName() {
        return mrGroupName;
    }

    /**
     * @param mrGroupName the mrGroupName to set
     */
    public void setMrGroupName(String mrGroupName) {
        this.mrGroupName = mrGroupName;
    }

    /**
     * @return the mrGroupInfo
     */
    public MRGroupInfo getMrGroupInfo() {
        return mrGroupInfo;
    }

    /**
     * @param mrGroupInfo the mrGroupInfo to set
     */
    public void setMrGroupInfo(MRGroupInfo mrGroupInfo) {
        this.mrGroupInfo = mrGroupInfo;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }
}
