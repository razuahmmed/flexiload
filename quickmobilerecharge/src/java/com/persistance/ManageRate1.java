package com.persistance;

import java.util.Date;

public class ManageRate1 {

    private Integer manageRateId;
    private String rateName;
    private Float grameenPhone;
    private Float robi;
    private Float banglalink;
    private Float airtel;
    private Float teletalk;
    private Float bkash;
    private Float dbbl;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;

    /**
     * @return the manageRateId
     */
    public Integer getManageRateId() {
        return manageRateId;
    }

    /**
     * @param manageRateId the manageRateId to set
     */
    public void setManageRateId(Integer manageRateId) {
        this.manageRateId = manageRateId;
    }

    /**
     * @return the rateName
     */
    public String getRateName() {
        return rateName;
    }

    /**
     * @param rateName the rateName to set
     */
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    /**
     * @return the grameenPhone
     */
    public Float getGrameenPhone() {
        return grameenPhone;
    }

    /**
     * @param grameenPhone the grameenPhone to set
     */
    public void setGrameenPhone(Float grameenPhone) {
        this.grameenPhone = grameenPhone;
    }

    /**
     * @return the robi
     */
    public Float getRobi() {
        return robi;
    }

    /**
     * @param robi the robi to set
     */
    public void setRobi(Float robi) {
        this.robi = robi;
    }

    /**
     * @return the banglalink
     */
    public Float getBanglalink() {
        return banglalink;
    }

    /**
     * @param banglalink the banglalink to set
     */
    public void setBanglalink(Float banglalink) {
        this.banglalink = banglalink;
    }

    /**
     * @return the airtel
     */
    public Float getAirtel() {
        return airtel;
    }

    /**
     * @param airtel the airtel to set
     */
    public void setAirtel(Float airtel) {
        this.airtel = airtel;
    }

    /**
     * @return the teletalk
     */
    public Float getTeletalk() {
        return teletalk;
    }

    /**
     * @param teletalk the teletalk to set
     */
    public void setTeletalk(Float teletalk) {
        this.teletalk = teletalk;
    }

    /**
     * @return the bkash
     */
    public Float getBkash() {
        return bkash;
    }

    /**
     * @param bkash the bkash to set
     */
    public void setBkash(Float bkash) {
        this.bkash = bkash;
    }

    /**
     * @return the dbbl
     */
    public Float getDbbl() {
        return dbbl;
    }

    /**
     * @param dbbl the dbbl to set
     */
    public void setDbbl(Float dbbl) {
        this.dbbl = dbbl;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
