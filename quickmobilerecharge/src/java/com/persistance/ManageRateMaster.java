package com.persistance;

public class ManageRateMaster {

    private Integer manageRateId;
    private String rateName;
    private Character status;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private UserInfo userInfo;
    private ManageRateChild rateChildInfo;

    /**
     * @return the manageRateId
     */
    public Integer getManageRateId() {
        return manageRateId;
    }

    /**
     * @param manageRateId the manageRateId to set
     */
    public void setManageRateId(Integer manageRateId) {
        this.manageRateId = manageRateId;
    }

    /**
     * @return the rateName
     */
    public String getRateName() {
        return rateName;
    }

    /**
     * @param rateName the rateName to set
     */
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    /**
     * @return the status
     */
    public Character getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Character status) {
        this.status = status;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the rateChildInfo
     */
    public ManageRateChild getRateChildInfo() {
        return rateChildInfo;
    }

    /**
     * @param rateChildInfo the rateChildInfo to set
     */
    public void setRateChildInfo(ManageRateChild rateChildInfo) {
        this.rateChildInfo = rateChildInfo;
    }
}
