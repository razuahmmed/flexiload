package com.persistance;

public class ManageRateChild {

    private Integer manageChildRateId;
    private String operatorName;
    private String operatorCode;
    private String commission;
    private ManageRateMaster rateMasterInfo;

    /**
     * @return the manageChildRateId
     */
    public Integer getManageChildRateId() {
        return manageChildRateId;
    }

    /**
     * @param manageChildRateId the manageChildRateId to set
     */
    public void setManageChildRateId(Integer manageChildRateId) {
        this.manageChildRateId = manageChildRateId;
    }

    /**
     * @return the operatorName
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * @param operatorName the operatorName to set
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * @return the commission
     */
    public String getCommission() {
        return commission;
    }

    /**
     * @param commission the commission to set
     */
    public void setCommission(String commission) {
        this.commission = commission;
    }

    /**
     * @return the rateMasterInfo
     */
    public ManageRateMaster getRateMasterInfo() {
        return rateMasterInfo;
    }

    /**
     * @param rateMasterInfo the rateMasterInfo to set
     */
    public void setRateMasterInfo(ManageRateMaster rateMasterInfo) {
        this.rateMasterInfo = rateMasterInfo;
    }

    /**
     * @return the operatorCode
     */
    public String getOperatorCode() {
        return operatorCode;
    }

    /**
     * @param operatorCode the operatorCode to set
     */
    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }
}
