package com.persistance;

import java.util.Date;

public class MRGroupInfo {

    private Integer mRGroupId;
    private String mRGroupName;
    private String mobileNumber;
    private Integer type;
    private Double amount;
    private Integer modemNumber;
    private String userId;
    private String insertBy;
    private Date insertDate;
    private Character activeStatus;
    private String updateBy;
    private Date updateDate;

    /**
     * @return the mRGroupId
     */
    public Integer getmRGroupId() {
        return mRGroupId;
    }

    /**
     * @param mRGroupId the mRGroupId to set
     */
    public void setmRGroupId(Integer mRGroupId) {
        this.mRGroupId = mRGroupId;
    }

    /**
     * @return the mRGroupName
     */
    public String getmRGroupName() {
        return mRGroupName;
    }

    /**
     * @param mRGroupName the mRGroupName to set
     */
    public void setmRGroupName(String mRGroupName) {
        this.mRGroupName = mRGroupName;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the modemNumber
     */
    public Integer getModemNumber() {
        return modemNumber;
    }

    /**
     * @param modemNumber the modemNumber to set
     */
    public void setModemNumber(Integer modemNumber) {
        this.modemNumber = modemNumber;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the activeStatus
     */
    public Character getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(Character activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
