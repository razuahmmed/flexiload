package com.model;

import com.common.QueryStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ManageRateDao {

    public int saveManageRateMaster(
            Connection connection,
            String rateName,
            String uID,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        try {

            String manageRateInsertStatement = " INSERT INTO "
                    + " manage_rate_master ( "
                    + " rate_name, "
                    + " user_id, "
                    + " insert_by, "
                    + " insert_date "
                    + " ) VALUES ( "
                    + "'" + rateName + "', "
                    + "'" + uID + "', "
                    + "'" + insertBy + "',"
                    + " CURRENT_TIMESTAMP)";

            ps = connection.prepareStatement(manageRateInsertStatement);

            status = ps.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
            status = 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return status;
    }

    public int saveManageRateChild(
            Connection connection,
            String operatorName,
            String operatorCode,
            String rateCommission) {

        int status = 0;

        PreparedStatement ps = null;

        try {

            String manageRateInsertStatement = " INSERT INTO "
                    + " manage_rate_child ( "
                    + " operator_name, "
                    + " operator_code, "
                    + " rate_commission, "
                    + " manage_rate_id "
                    + " ) VALUES ( "
                    + "'" + operatorName + "', "
                    + "'" + operatorCode + "', "
                    + "'" + rateCommission + "', "
                    + " (SELECT MAX(manage_rate_id) FROM manage_rate_master)) ";

            ps = connection.prepareStatement(manageRateInsertStatement);

            status = ps.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
            status = 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return status;
    }

    public int updateManageRateMaster(
            Connection connection,
            String rateName,
            String updateBy,
            String uID,
            Integer rateId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateManageRateStatement(rateName, updateBy, uID, rateId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateManageRateChild(
            Connection connection,
            String operatorName,
            String operatorCode,
            String rateCommission,
            Integer manageRateID) {

        int status = 0;

        PreparedStatement ps = null;

        try {

            String manageRateInsertStatement = " INSERT INTO "
                    + " manage_rate_child ( "
                    + " operator_name, "
                    + " operator_code, "
                    + " rate_commission, "
                    + " manage_rate_id "
                    + " ) VALUES ( "
                    + "'" + operatorName + "', "
                    + "'" + operatorCode + "', "
                    + "'" + rateCommission + "', "
                    + "'" + manageRateID + "')";

            ps = connection.prepareStatement(manageRateInsertStatement);

            status = ps.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
            status = 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return status;
    }

    public int deleteManageRateChild(
            Connection connection,
            Integer rateId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.deleteManageRateStatement(rateId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteManageRate(
            Connection connection,
            Integer gId,
            String userId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String mrGroupDeleteStatement = " DELETE "
                    + " manage_rate_master MRM, "
                    + " manage_rate_child MRCH "
                    + " FROM "
                    + " manage_rate_master MRM "
                    + " INNER JOIN "
                    + " manage_rate_child MRCH "
                    + " WHERE "
                    + " MRM.manage_rate_id = MRCH.manage_rate_id "
                    + " AND "
                    + " MRM.user_id = '" + userId + "' "
                    + " AND "
                    + " MRM.manage_rate_id = '" + gId + "' ";

            try {

                ps = connection.prepareStatement(mrGroupDeleteStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
