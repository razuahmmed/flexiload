package com.model;

import com.common.QueryStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MyAccountDao {

    public int updateProfile(
            Connection connection,
            String userFullName,
            String phone,
            String email,
            String otpStatus,
            String pinEnabledStatus,
            String apiStatus,
            String pinConfirmStatus,
            String updateBy,
            String resellerId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateResellerProgile(resellerId));

                ps.setString(1, userFullName);
                ps.setString(2, phone);
                ps.setString(3, email);
                ps.setString(4, otpStatus);
                ps.setString(5, pinEnabledStatus);
                ps.setString(6, apiStatus);
                ps.setString(7, pinConfirmStatus);
                ps.setString(8, updateBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateResellerPassword(
            Connection connection,
            String password,
            String updateBy,
            String resellerId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateResellerPassword(resellerId));

                ps.setString(1, password);
                ps.setString(2, updateBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int resetResellersPassword(
            Connection connection,
            String password,
            String updateBy,
            String resellerId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String resetPass = " UPDATE "
                        + " user_info "
                        + " SET "
                        + " user_password = '" + password + "', "
                        + " update_by = '" + updateBy + "', "
                        + " update_date = CURRENT_TIMESTAMP "
                        + " WHERE "
                        + " user_id = '" + resellerId + "' ";

                ps = connection.prepareStatement(resetPass);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int requestBalance(
            Connection connection,
            Double amount,
            String balType,
            String resellerID,
            String requestTo,
            String trid) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String balRequestHisInsertStatement = " INSERT INTO "
                        + " bal_request_history ( "
                        + " amount, "
                        + " bal_type, "
                        + " user_id, "
                        + " request_to, "
                        + " trid, "
                        + " request_time "
                        + " ) VALUES ( "
                        + "?,?,?,?,?, CURRENT_TIMESTAMP )";

                ps = connection.prepareStatement(balRequestHisInsertStatement);

                ps.setDouble(1, amount);
                ps.setString(2, balType);
                ps.setString(3, resellerID);
                ps.setString(4, requestTo);
                ps.setString(5, trid);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int setTransactionPin(
            Connection connection,
            String transactionPin,
            String updateBy,
            String resellerId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.setResellerTPin(resellerId));

                ps.setString(1, transactionPin);
                ps.setString(2, updateBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
