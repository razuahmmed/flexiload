package com.model;

import com.common.QueryStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LoginDao {

    public int updateLoginStatus(
            Connection connection,
            String uId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateLoginStatus(uId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateLogoutStatus(
            Connection connection,
            String uId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateLogoutStatus(uId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
