<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleMmTransactionHInfoList !=null">
    <s:if test="singleMmTransactionHInfoList.size() !=0">
        <s:iterator value="singleMmTransactionHInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Transaction History ID # <s:property value="mmTransactionHistoryId"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN ALERTS PORTLET-->
                        <div class="portlet green box">
                            <div class="portlet-title">
                                <div class="caption">
                                    Order Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <!--                                <div class="alert alert-success">
                                                                                                        <strong>Success!</strong>
                                                                    Transaction ID is - 
                                                                </div>                    -->
                                <!--                                <form method="post" class="form-inline" role="form" action="#">
                                                                    <div class="form-group">
                                                                        <label class="sr-only" for="trid">TrID</label>
                                                                        <input class="form-control" autocomplete="off" id="trid" name="trid" value="Success" type="text">
                                                                    </div>
                                                                    <button type="submit" class="btn btn-success">Manual Confirm</button>
                                                                </form>
                                                                <div class="clearfix">
                                                                    <br>
                                                                </div>
                                                                <form method="post" class="form-inline" role="form" action="#">
                                                                    <div class="form-group">
                                                                        <label class="sr-only" for="trid">TrID</label>
                                                                        <input autocomplete="off" placeholder="Failed Reason..." class="form-control" id="trid" name="trid" value="Success" type="text">
                                                                    </div>
                                                                    <button type="submit" class="btn btn-danger">Refund</button>
                                                                </form>
                                                                <div class="clearfix">
                                                                    <br>
                                                                                                    <a href="" class="btn red">Refund</a>
                                                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>
                                                                </div>-->
                                <h4>Details</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width: 30%">History ID</th>
                                                <th><s:property value="mmTransactionHistoryId"/></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>From</td>
                                                <td><s:property value="balanceGivenBy"/></td>
                                            </tr>
                                            <tr>
                                                <td>Balance Type</td>
                                                <td>Mobile Money</td>
                                            </tr>
                                            <tr>
                                                <td>To</td>
                                                <td><s:property value="userInfo.userId"/></td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <td><s:property value="addBalance"/></td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td>
                                                    <s:if test="type=='Y'">
                                                        Transfer
                                                    </s:if>
                                                    <s:else>
                                                        <span style="color: #ff0033;">
                                                            Waiting
                                                        </span>
                                                    </s:else>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Request Time</td>
                                                <td><s:property value="balanceGivenDate"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h4>Orders For this number</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><s:property value="mmTransactionHistoryId"/></td>
                                                <td><s:property value="balanceGivenBy"/></td>
                                                <td><s:property value="userInfo.userId"/></td>
                                                <td><s:property value="balanceGivenDate"/></td>
                                                <td>
                                                    <s:if test="type=='Y'">
                                                        Transfer
                                                    </s:if>
                                                    <s:else>
                                                        <span style="color: #ff0033;">
                                                            Waiting
                                                        </span>
                                                    </s:else>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END ALERTS PORTLET-->
                    </div>
                </div>
            </div>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>