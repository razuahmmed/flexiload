<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="allMRNumber.size() !=0">
    <h4>Mobile Recharge History</h4>
    <div class="table-responsive">
        <table id="example" class="table table-striped table-bordered table-advance table-hover mr-table">
            <thead>
                <tr>
                    <th><i class="fa fa-briefcase"></i>ID</th>
                    <th><i class="fa fa-briefcase"></i>Sender</th>
                    <th><i class="fa fa-question"></i>Phone</th>
                    <th><i class="fa fa-bookmark"></i>Amount</th>
                    <th><i class="fa fa-bookmark"></i>Type</th>
                    <th><i class="fa fa-bookmark"></i>Status</th>
                    <th><i class="fa fa-bookmark"></i>TrID</th>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="allMRNumber">
                    <tr>
                        <td><s:property value="mobileRechargeId"/></td>
                        <td><s:property value="sender"/></td>
                        <td><s:property value="receiver"/></td>
                        <td class="mr-amount"><s:property value="givenBalance"/></td>
                        <td>
                            <s:if test="type==0">
                                Prepaid
                            </s:if>
                            <s:else>
                                <span style="color: #009966;">
                                    Postpaid
                                </span>
                            </s:else>
                        </td>
                        <td>
                            <s:if test="activeStatus=='Y'">
                                <span>
                                    Success
                                </span>
                            </s:if>
                            <s:elseif test="activeStatus=='N'">
                                <span>
                                    Pending
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='P'">
                                <span>
                                    Processing
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='W'">
                                <span>
                                    Waiting
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='F'">
                                <span>
                                    Failed
                                </span>
                            </s:elseif>
                        </td>
                        <td><s:property value="trid"/></td>
                    </tr>
                </s:iterator>
                <tr style="font-size: 20px;font-weight: bold;">
                    <th colspan="7" style="text-align: center;">Total Amount of Success Request to this Number Taka : <span class="mr-total"></span></th>
                </tr>
            </tbody>
        </table>
    </div>
</s:if>

<s:if test="allMMNumber.size() !=0">
    <h4>Mobile Money History</h4>
    <div class="table-responsive">
        <table id="example" class="table table-striped table-bordered table-advance table-hover mm-table">
            <thead>
                <tr>
                    <th><i class="fa fa-briefcase"></i>ID</th>
                    <th><i class="fa fa-briefcase"></i>Sender</th>
                    <th><i class="fa fa-question"></i>Phone</th>
                    <th><i class="fa fa-bookmark"></i>Amount</th>
                    <th><i class="fa fa-bookmark"></i>Type</th>
                    <th><i class="fa fa-bookmark"></i>Status</th>
                    <th><i class="fa fa-bookmark"></i>TrID</th>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="allMMNumber">
                    <tr>
                        <td><s:property value="mobileMoneyId"/></td>
                        <td><s:property value="sender"/></td>
                        <td><s:property value="receiver"/></td>
                        <td class="mm-amount"><s:property value="givenBalance"/></td>
                        <td><s:property value="type"/></td>
                        <td>
                            <s:if test="activeStatus=='Y'">
                                <span>
                                    Success
                                </span>
                            </s:if>
                            <s:elseif test="activeStatus=='N'">
                                <span>
                                    Pending
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='P'">
                                <span>
                                    Processing
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='W'">
                                <span>
                                    Waiting
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='F'">
                                <span>
                                    Failed
                                </span>
                            </s:elseif>
                        </td>
                        <td><s:property value="trid"/></td>
                    </tr>
                </s:iterator>
                <tr style="font-size: 20px;font-weight: bold;">
                    <th colspan="7" style="text-align: center;">Total Amount of Success Request to this Number Taka : <span class="mm-total"></span></th>
                </tr>
            </tbody>
        </table>
    </div>
</s:if>
<s:if test="allMRNumber.size() == 0">
    <s:if test="allMMNumber.size() == 0">
        <table id="example" class="table table-striped table-bordered table-advance table-hover mr-table">
            <thead>
                <tr>
                    <th><i class="fa fa-briefcase"></i>ID</th>
                    <th><i class="fa fa-briefcase"></i>Sender</th>
                    <th><i class="fa fa-question"></i>Phone</th>
                    <th><i class="fa fa-bookmark"></i>Amount</th>
                    <th><i class="fa fa-bookmark"></i>Type</th>
                    <th><i class="fa fa-bookmark"></i>Status</th>
                    <th><i class="fa fa-bookmark"></i>TrID</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="7" style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;">No history found</td>
                </tr>
            </tbody>
        </table>
    </s:if>
</s:if>