<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Admin&nbsp;<small>Online&nbsp;Users</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light bordered">
                                <table id="datatable_orders" class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>User</th>
                                            <th>Type</th>
                                            <th>Parent</th>
                                            <th>Path</th>
                                            <th>MR Bal</th>
                                            <th>MM Bal</th>
                                            <th>IP</th>
                                            <th>Login Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% int sId = 0;%>
                                        <s:if test="onLineUserList !=null">
                                            <s:if test="onLineUserList.size() !=0">
                                                <s:iterator value="onLineUserList">
                                                    <% sId++;%>
                                                    <tr>
                                                        <td><%= sId%></td>
                                                        <td><s:property value="userId"/></td>
                                                        <td><s:property value="userGroupInfo.groupName"/></td>
                                                        <td><s:property value="parentId"/></td>
                                                        <td><s:property value="parentId"/></td>
                                                        <td><s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/></td>
                                                        <td><s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/></td>
                                                        <td> <a href="http://www.ip-adress.com" target="_blank">Who am I ?</a></td>
                                                        <td><s:property value="lastLogin"/></td>
                                                    </tr>
                                                </s:iterator>
                                            </s:if>
                                        </s:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable_orders').DataTable({
                    "orderCellsTop": true,
                    "pagingType": "full_numbers"
                });
            });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>

    </body>
</html>