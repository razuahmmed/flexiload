<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<title>Quick Mobile Recharge</title>
<link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

<link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

<link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>

<link href="<%= request.getContextPath()%>/my-css/plugins.css" rel="stylesheet" type="text/css"/>

<!-- BOOTSTRAP.CSS BEFORE CUSTOM.CSS -->
<link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

<!-- FOR COMPANY ICON -->
<link href="<%= request.getContextPath()%>/my-css/company.css" rel="stylesheet" type="text/css"/>

<!-- FOR DATA TABLE -->
<link href="<%= request.getContextPath()%>/my-css/dataTables.css" rel="stylesheet" type="text/css"/>

<link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/my-css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/my-css/select2.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/my-css/datepicker.css" rel="stylesheet" type="text/css"/>

<link href="<%= request.getContextPath()%>/my-css/mobileRechargeImage.css" rel="stylesheet" type="text/css"/>

<!-- FOR RESPONSIVE PAGE THIS CSS ADD END OF ALL CSS-->
<link href="<%= request.getContextPath()%>/my-css/responsive.css" rel="stylesheet" type="text/css"/>