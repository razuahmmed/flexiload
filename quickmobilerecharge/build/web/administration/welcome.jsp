<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quick Mobile Recharge</title>
        <link href="<%= request.getContextPath()%>/images/login_title.png" rel="shortcut icon" type="image/x-icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/normalize.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/owl.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/owl_002.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/owl_003.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/jquery.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/main.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/login-form.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/responsive.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <main class="app">
            <!-- STAR PAGE HEADER -->
            <header class="main-header">
                <div class="header-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="clearfix header-inner">
                                    <!--                                    <div class="pull-left">
                                                                            <div class="logo-section">
                                                                                <h1><a href="javascript:;" class="show"><img src="<%= request.getContextPath()%>/images/logo_login.png" alt=""></a></h1>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pull-right">
                                                                            <ul class="clearfix social-icons">
                                                                                <li><a href="https://www.facebook.com/Brand-Bangla-IT-572802589586723/?fref=ts" target="_blank" class="round-icon"><i class="fa fa-facebook"></i></a></li>
                                                                                <li><a href="#" class="round-icon"><i class="fa fa-twitter"></i></a></li>
                                                                                <li><a href="#" class="round-icon"><i class="fa fa-google-plus"></i></a></li>
                                                                            </ul>
                                                                        </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- START MARQUEE -->
                <div class="text-center header-bottom">
                    <ul id="webTicker" class="newsticker">
                        <li>You can easily send money from here.</li>
                    </ul>
                </div>
                <!-- END MARQUEE -->
            </header>
            <!-- END PAGE HEADER -->
            <section class="page-body">
                <!-- START SLIDER -->
                <section class="bg-slider">
                    <!--  SLIDER IMAGES-->
                    <div class="bg-slider-inner">
                        <div id="bgSliderWrap" class="bg-slider-img-wrap">
                            <img src="<%= request.getContextPath()%>/images/bg-slider1.jpg" alt="">
                            <img src="<%= request.getContextPath()%>/images/bg-slider2.jpg" alt="">
                            <img src="<%= request.getContextPath()%>/images/bg-slider3.jpg" alt="">
                        </div>
                    </div>
                    <!-- START SLIDER CONTENT -->
                    <div class="bg-slider-content-wrap">
                        <div class="bg-slider-content-wrap-inner">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-8">
                                        <div class="slogan-section">
                                            <h1 class="sl-title title">Welcome to <br/><span>Quick Mobile Recharge</span></h1>
                                            <h3 class="sl-title subtitle">You can easily send money any operator from your device.</h3>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="login-form up-slider-form">
                                            <div class="login-form-inner">
                                                <div class="form-wrap">
                                                    <form id="resellerLogin" method="post">
                                                        <h2 class="regular-title title">Login to your Account</h2>
                                                        <div class="form-group">
                                                            <div id="errorMess">

                                                            </div>
                                                            <input type="text" class="form-control" name="userId" id="username" placeholder="User ID">
                                                            <input type="password" class="form-control" name="userPassword" id="password" placeholder="Password">
                                                            <p class="checkbox-wrap">
                                                                <!--<label style="float: left; margin-top: 15px;" class="checkbox-inline"><input type="checkbox" id="inlineCheckbox1" value="option1">Remember me</label>-->
                                                                <img id="loginLoadingImage" src="" alt="" />
                                                                <button type="submit" id="btnLogin" class="btn bg-orng regular-title custom-btn" style="float: right;">
                                                                    Login <i class="m-icon-swapright m-icon-white"></i>
                                                                </button>
                                                            </p>
                                                            <!--                                                            <div class="forget-password">
                                                                                                                            <h4>Forgot your password ?</h4>
                                                                                                                            <p>
                                                                                                                                <a href="javascript:;" id="forget-password">
                                                                                                                                    Click here
                                                                                                                                </a>
                                                                                                                                &nbsp;
                                                                                                                                to reset your password.
                                                                                                                            </p>
                                                                                                                        </div>-->
                                                        </div>
                                                    </form>

                                                    <form class="forget-form" method="post">
                                                        <h3>Forgotten your password ?</h3>
                                                        <p>
                                                            Enter your e-mail address below to reset your password.
                                                        </p>
                                                        <div class="form-group">
                                                            <div id="errorMess2">

                                                            </div>
                                                            <div class="input-icon">
                                                                <i class="fa fa-envelope"></i>
                                                                <input type="text" id="email" name="email" autocomplete="off" placeholder="Email" class="form-control placeholder-no-fix"/>
                                                            </div>
                                                        </div>
                                                        <div style="height: 35px;" class="form-actions">
                                                            <button type="button" style="float: left" id="back-btn"  class="btn">
                                                                <i class="m-icon-swapleft"></i>
                                                                Back
                                                            </button>
                                                            <img id="forgotPassLoadingImage" style="margin-top: 3px; margin-left: 60px;" src="" alt="" />
                                                            <button type="submit" id="forgetPassButton" style="background-color: #ff7f3f; color: #FFFFFF;" class="btn blue pull-right">
                                                                Submit <i class="m-icon-swapright m-icon-white"></i>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="note-sec">
                                                    <h2 class="title">Note</h2>
                                                    <ul>
                                                        <li>Don't provide your password to any other</li>
                                                        <li>Use strong password for your safety</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END SLIDER -->

                <!-- START BRAND SLIDER -->
                <section class="brand-slider">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 ">
                                <div class="brand-slider-inner">
                                    <div id="brandSlider" class="clearfix brand-sl-wrap">
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider9.jpg" alt=""></div>
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider1.jpg" alt=""></div>
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/teletalk.png" alt=""></div>
                                        <!--<div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider3.jpg" alt=""></div>-->
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider4.jpg" alt=""></div>
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider5.jpg" alt=""></div>
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider6.jpg" alt=""></div>
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider7.jpg" alt=""></div>
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider8.jpg" alt=""></div>
                                        <div class="bsl-item-wrap"><img src="<%= request.getContextPath()%>/images/brandSlider9.jpg" alt=""></div>
                                    </div>
                                    <div class="slider-cntrl">
                                        <div class="scntrl-inner">
                                            <a href="#" class="round-icon bs-ctrl bs-left"><i class="fa fa-chevron-left"></i></a>
                                            <a href="#" class="round-icon bs-ctrl bs-right"><i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END BRAND SLIDER -->

                <!-- START FEATURE SLIDER -->
                <!--                <section class="feature-sidebar">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-7 col-md-8 col-lg-8">
                                                <h1 class="sl-title title">Welcome to <span>MOBILE RECHARGE</span></h1>
                                                <div class="feature-wrap">
                                                    <div class="row">
                                                        <article class="col-sm-6 col-md-6 col-lg-6 featwrap">
                                                            <div class="single-feature">
                                                                <div class="feat-head">
                                                                    <span><i class="fa fa-cogs"></i></span>
                                                                </div>
                                                                <div class="feat-body">
                                                                    <h2 class="feat-title">Be Automized</h2>
                                                                    <ul>
                                                                        <li>Instant Automatic Flexiload</li>
                                                                        <li>Ultrafast Transactions</li>
                                                                        <li>Supports Multiple Modems for Single Operator</li>
                                                                        <li>Real Time Transaction ID. Tracks Success/Failure messages </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <article class="col-sm-6 col-md-6 col-lg-6 featwrap">
                                                            <div class="single-feature">
                                                                <div class="feat-head">
                                                                    <span><i class="fa fa-send-o"></i></span>
                                                                </div>
                                                                <div class="feat-body">
                                                                    <h2 class="feat-title">Beyondness</h2>
                                                                    <ul>
                                                                        <li>Up to 4 label user creation system (R4, R3, R2, R1)</li>
                                                                        <li>Approx 100 RPM (Request Per Minutes) </li>
                                                                        <li>Real-time Transaction ID matched exactly with End user recharge confirmation message</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <article class="col-sm-6 col-md-6 col-lg-6 featwrap">
                                                            <div class="single-feature">
                                                                <div class="feat-head">
                                                                    <span><i class="fa fa-file"></i></span>
                                                                </div>
                                                                <div class="feat-body">
                                                                    <h2 class="feat-title">Reporting</h2>
                                                                    <ul>
                                                                        <li>User friendly reports</li>
                                                                        <li>Operator wise recharge &amp; payment report</li>
                                                                        <li>Account statement &amp; Daily Report</li>
                                                                        <li>Status wise recharge &amp; balance report</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <article class="col-sm-6 col-md-6 col-lg-6 featwrap">
                                                            <div class="single-feature">
                                                                <div class="feat-head">
                                                                    <span><i class="fa fa-paperclip"></i></span>
                                                                </div>
                                                                <div class="feat-body">
                                                                    <h2 class="feat-title">Extars</h2>
                                                                    <ul>
                                                                        <li>API for integrate with other system</li>
                                                                        <li>Available addons to offer other services</li>
                                                                        <li>Android apps</li>
                                                                        <li>many more . .</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                             START SIDE BAR 
                                            <div class="col-sm-5 col-md-4 col-lg-4 ">
                                                <div class="sidebar-wrap">
                                                    <div class="news-widget single-widget">
                
                                                        <div class="clearfix widget-header">
                                                            <h3 class="pull-left title">News Board</h3>
                                                            <div class="pull-right nbs-slider-ctrl">
                                                                <a href="#" id="neswNext" class="round-icon  nbs-arrow left-arrow"><i class="fa fa-chevron-left"></i></a>
                                                                <a href="#" id="neswPrev" class="round-icon  nbs-arrow right-arrow"><i class="fa fa-chevron-right"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="widget-content">
                                                            <ul id="newsSlider">
                                                                <li>Monotonectally leverage other's diverse technologies and interdependent quality vectors. Efficiently provide access to process-centric.</li>
                                                                <li>Efficiently foster enterprise communities after open-source expertise. Professionally iterate go forward bandwidth.</li>
                                                                <li>Monotonectally leverage other's diverse technologies and interdependent quality vectors. Efficiently provide access to process-centric.</li>
                                                                <li>Quickly provide access to visionary human capital through extensible channels. Holisticly transition diverse strategic theme areas whereas distributed.</li>
                                                                <li>Monotonectally leverage other's diverse technologies and interdependent quality vectors. Efficiently provide access to process-centric.</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             END SIDE BAR 
                                        </div>
                                    </div>
                                </section>-->
                <!-- END FEATURE SIDE BAR -->
            </section>

            <!-- START FOOTER -->
            <footer class="main-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="copy-right text-center">
                                <p style="color: #000;">Copyright &copy; 2017 quick mobile recharge. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- END FOOTER -->
        </main>

        <script src="<%= request.getContextPath()%>/my-js/modernizr-2.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/plugins.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/owl.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/jqurycycle.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/jquery.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/main.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                var btn_login = $("#btnLogin");

                var forgetPass = $("#forgetPassButton");

                btn_login.on('click', function (e) {

                    e.preventDefault();

                    document.getElementById('loginLoadingImage').src = 'images/loading.gif';

                    $.post("Login", $("#resellerLogin").serialize()).done(function (data) {

                        if (data.trim() == 1) {
                            document.getElementById('loginLoadingImage').src = '';
                            window.location = 'Home';
                        } else {
                            $('#errorMess').html(data);
                            document.getElementById('loginLoadingImage').src = '';
                        }
                    });
                });

                forgetPass.on('click', function (e) {

                    e.preventDefault();

                    document.getElementById('forgotPassLoadingImage').src = 'images/loading.gif';

                    $.post("ForgetPassword", $(".forget-form").serialize()).done(function (data) {

                        if (data.trim() == 1) {
//                            document.getElementById('forgotPassLoadingImage').src = '';
                            $('#errorMess2').html(data);
//                            window.location = 'Home';
                        } else {
                            $('#errorMess2').html(data);
                            document.getElementById('forgotPassLoadingImage').src = '';
                        }
                    });
                });

                jQuery('#forget-password').click(function () {
                    jQuery('#resellerLogin').hide();
                    jQuery('.forget-form').show();
                });

                jQuery('#back-btn').click(function () {
                    jQuery('#resellerLogin').show();
                    jQuery('.forget-form').hide();
                });

            });
        </script>

    </body>
</html>