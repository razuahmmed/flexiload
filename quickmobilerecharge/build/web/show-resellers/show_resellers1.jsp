<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <script type="text/javascript">

            function viewResellerButtonClicked(resellerId) {

                var dataString = resellerId;

                window.location = "ViewResellerButtonClicked?resellerId=" + dataString;
            }

            function editResellerButtonClicked(resellerId) {

                var dataString = resellerId;

                window.location = "EditResellerButtonClicked?resellerId=" + dataString;
            }

        </script>

        <style>
            .btn.ad-reseller {
                margin-right: 0px;
            }
        </style>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Resellers <small>show resellers</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light bordered">

                                <div class="portlet-title">
                                    <div class="caption font-green-sharp">
                                        <span class="caption-subject bold uppercase"> Resellers </span>
                                        <span class="caption-helper">Show Resellers - Reseller 1 ...</span>
                                    </div>

                                    <div class="actions">
                                        <a href="AddReseller1" class="btn btn-circle blue btn-sm"><i class="icon-plus"></i> Add Reseller </a>
                                        <a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="11%">Reseller ID</th>
                                                    <th width="14%">Full Name</th>
                                                    <th width="8%">Phone</th>
                                                    <th width="17%">MR&nbsp;Balance</th>
                                                    <th width="17%">MM&nbsp;Balance</th>
                                                    <th width="9%">Type</th>
                                                    <th width="7%">Status</th>
                                                    <th width="5%">OTP</th>
                                                    <th width="12%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <s:if test="resallersOneInfoList !=null">
                                                    <s:if test="resallersOneInfoList.size() !=0">
                                                        <s:iterator value="resallersOneInfoList">
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="userId"/>');">
                                                                        <s:property value="userId"/>
                                                                    </a>
                                                                </td>
                                                                <td><s:property value="userName"/></td>
                                                                <td>
                                                                    <s:if test="contactNumber1 !=null">
                                                                        <s:property value="contactNumber1"/>
                                                                    </s:if>
                                                                    <s:else>
                                                                        No Phone Number
                                                                    </s:else>
                                                                </td>
                                                                <td align="center">
                                                                    <strong>
                                                                        <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/>
                                                                    </strong>
                                                                    <br/>
                                                                    <% if (PM03CM01 && PERMISSION_MR) {%>
                                                                    <a href="Payments?resellerId=<s:property value="userId"/>" class="btn btn-xs green-meadow ad-reseller">
                                                                        <i class="icon-plus"></i>
                                                                        Add Fund
                                                                    </a>
                                                                    <a href="MRPaymentsMade" class="btn btn-xs green-meadow ad-reseller">
                                                                        <i class="icon-paper-plane"></i>
                                                                        History
                                                                    </a>
                                                                    <% }%>
                                                                </td>
                                                                <td align="center">
                                                                    <strong>
                                                                        <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                                                                    </strong>
                                                                    <br/>
                                                                    <% if (PM04CM01 && PERMISSION_MM) {%>
                                                                    <a href="Payments?resellerId=<s:property value="userId"/>&loadType=<s:property value="serviceMMoney"/>" class="btn btn-xs purple-medium ad-reseller">
                                                                        <i class="icon-plus"></i>
                                                                        Add Fund
                                                                    </a>
                                                                    <a href="MMPaymentsMade" class="btn btn-xs purple-medium ad-reseller">
                                                                        <i class="icon-wallet"></i>
                                                                        History
                                                                    </a>
                                                                    <% }%>
                                                                </td>
                                                                <td><s:property value="userGroupInfo.groupName"/></td>
                                                                <td align="center">
                                                                    <s:if test="suspendActivity=='Y'">Active</s:if>
                                                                    <s:else>
                                                                        <p style="color: red;">Inactive</p>
                                                                    </s:else>
                                                                </td>
                                                                <td align="center">
                                                                    <s:if test="otpStatus=='Y'">Enable</s:if>
                                                                    <s:else>
                                                                        <p style="color: red;">Disable</p>
                                                                    </s:else>
                                                                </td>
                                                                <td align="center">
                                                                    <a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="userId"/>');" class="btn blue btn-xs ad-reseller">
                                                                        <i class="icon-list"></i>
                                                                        View
                                                                    </a>
                                                                    <a href="javascript:void(0);" onclick="editResellerButtonClicked('<s:property value="userId"/>');" class="btn btn-xs red btn-editable ad-reseller">
                                                                        <i class="icon-note"></i>
                                                                        Edit
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable_orders').DataTable({
                    "orderCellsTop": true,
                    "pagingType": "full_numbers"
                });

                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>
    </body>
</html>