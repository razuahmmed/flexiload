<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <style type="text/css">
            .modal-scrollable {
                overflow: auto !important;
            }
        </style>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile&nbsp;Recharge&nbsp;<small class="fv">Bangladesh &amp; Malaysia</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-green-sharp">
                                        <span class="caption-subject bold uppercase">Mobile&nbsp;Recharge</span>
                                        <span class="caption-helper">All Mobile Recharge History in your system ...</span>
                                    </div>

                                    <!--                                    <div style="margin-left: 14%;" class="caption font-green-sharp">
                                                                            <select id="cid" name="userList" data-placeholder="Select Reseller" class="form-control input-large">
                                                                                <option value=""></option>
                                    <s:if test="resellerByParentList !=null">
                                        <s:if test="resellerByParentList.size() !=0">
                                            <s:iterator value="resellerByParentList">
                                                <option value="<s:property value="userId"/>"><s:property value="userId"/></option>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </select>
                                <img id="createLoadingImage" src="" alt="" />
                            </div>-->

                                    <div class="actions">
                                        <div class="tools">
                                            <a href="SendMobileRecharge" class="btn btn-circle blue btn-sm">
                                                <i class="icon-plus"></i>&nbsp;Send&nbsp;Recharge
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span></span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="resend">Resend</option>
                                                <option value="refund">Refund</option>
                                                <option value="success">Success</option>
                                            </select>
                                            <button class="btn btn-sm yellow table-group-action-submit"><i class="icon-check"></i> Submit</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"><input type="checkbox" class="group-checkable form-filter"></th>
                                                    <th width="5%">Order Reg ID</th>
                                                    <th width="12%">Purchased&nbsp;On</th>
                                                    <th width="14%">Sender</th>
                                                    <th width="10%">Receiver</th>
                                                    <th width="8%">Operator</th>
                                                    <th width="8%">Type</th>
                                                    <th width="6%">Amount</th>
                                                    <th width="7%">Status</th>
                                                    <th width="10%">TnxID</th>
                                                    <th width="6%">Originator</th>
                                                    <th width="6%">OP Bal</th>
                                                    <th width="6%">Actions</th>
                                                </tr>
                                                <tr class="filter">
                                                    <td></td>
                                                    <td><input type="text" id="order_id" name="order_id" class="form-control form-filter input-sm"></td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                            <input type="text" id="sdate" name="sdate" style="width: 90px;" value="" class="form-control form-filter input-sm" readonly placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="icon-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                            <input type="text" id="tdate" name="tdate" style="width: 90px;" value="" class="form-control form-filter input-sm" readonly placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="icon-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" id="sender" name="sender" class="form-control form-filter input-sm"></td>
                                                    <td><input type="text" id="receiver" name="receiver" class="form-control form-filter input-sm"></td>
                                                    <td>
                                                        <select id="op" name="op" style="width: 90px;" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="Grameen Phone">GP</option>
                                                            <option value="Robi">Robi</option>
                                                            <option value="Banglalink">BL</option>
                                                            <option value="Airtel">Airtel</option>
                                                            <option value="Teletalk">Teletalk</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="type" name="type" style="width: 95px;" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="0">Prepaid</option>
                                                            <option value="1">Postpaid</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <input type="text" id="famount" name="famount" class="form-control form-filter input-sm margin-bottom-5 clearfix" placeholder="From"/>
                                                        </div>
                                                        <input type="text" id="tamount" name="tamount" class="form-control form-filter input-sm" placeholder="To"/>
                                                    </td>
                                                    <td>
                                                        <select id="status" name="status" style="width: 107px;" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="W">Waiting</option>
                                                            <option value="S">Sent</option>
                                                            <option value="Y">Success</option>
                                                            <option value="P">Processing</option>
                                                            <option value="R">Refunded</option>
                                                            <option value="F">Failed</option>
                                                        </select>
                                                    </td>
                                                    <td><input type="text" id="tnxid" name="tnxid" class="form-control form-filter input-sm"></td>
                                                    <td><input type="text" id="o" name="o" class="form-control form-filter input-sm"></td>
                                                    <td></td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm yellow filter-submit1 margin-bottom"><i class="icon-magnifier"></i> Search</button>
                                                        </div>
                                                        <button class="btn btn-sm red filter-cancel"><i class="icon-close"></i> Reset</button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_mr_history">
                                                <s:if test="allMRHInfoByAdmin !=null">
                                                    <s:if test="allMRHInfoByAdmin.size() !=0">
                                                        <s:iterator value="allMRHInfoByAdmin" var="allMRHInfo">
                                                            <tr>
                                                                <td><input type="checkbox" id="chkb" name="chkb" value="${allMRHInfo.mobileRechargeId}" class="form-filter"></td>
                                                                <td align="center" class="sorting_1">
                                                                    ${allMRHInfo.trid}
                                                                </td>
                                                                <td align="center"><s:property value="purchasedOn"/></td>
                                                                <td>${allMRHInfo.sender}</td>
                                                                <td>${allMRHInfo.receiver}</td>
                                                                <td>${allMRHInfo.operator}</td>
                                                                <td>
                                                                    <s:if test="type==0">
                                                                        Prepaid
                                                                    </s:if>
                                                                    <s:else>
                                                                        <span style="color: #009966;">
                                                                            Postpaid
                                                                        </span>
                                                                    </s:else>
                                                                </td>
                                                                <td class="amount">${allMRHInfo.givenBalance}</td>
                                                                <td align="center">
                                                                    <s:if test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='S'">
                                                                        <span>
                                                                            Sent
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='R'">
                                                                        <span>
                                                                            Refunded
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>
                                                                    ${allMRHInfo.tnxid}
                                                                </td>
                                                                <td>
                                                                    <s:if test='originator=="1"'>
                                                                        Modem
                                                                    </s:if>
                                                                    <s:else>
                                                                        Web
                                                                    </s:else>
                                                                </td>
                                                                <td>${allMRHInfo.operatorBalance}</td>
                                                                <td align="center">
                                                                    <input type="hidden" id="rID" value="<s:property value="sender"/>"/>
                                                                    <a href="javascript:void(0);" id="<s:property value="mobileRechargeId"/>" class="fview btn btn-xs blue btn-editable tooltips" data-container="body" data-placement="top" data-original-title="Show Details">
                                                                        <i class="icon-list"></i>
                                                                        View
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="7" style="text-align:right">Total:</th>
                                                    <th colspan="6" style="text-align: left"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/mrHModalView.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('#cid').select2().on("change", function (e) {

                    var cid = $('#cid').val();

                    if ((cid != '')) {

                        document.getElementById('createLoadingImage').src = 'images/loading.gif';

                        $.post('GetMRHistoryByParent', {
                            resellerId: $('#cid').val()
                        }).done(function (data) {
                            document.getElementById('createLoadingImage').src = '';
                            $("#tbody_mr_history").html(data);
                        });
                    }
                });

                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;
                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(7)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(7, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(7).footer()).html(
                                'Page Total ' + pageTotal + ' [ All Total ' + total + ' ]'
                                );
                    }
                });

                Metronic.init();
                Layout.init();
                QuickSidebar.init();
                EcommerceOrders.init();
            });
        </script>
    </body>
</html>