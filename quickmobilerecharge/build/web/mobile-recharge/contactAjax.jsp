<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="contactInfoList !=null">
    <s:if test="contactInfoList.size() !=0">
        <% int sId = 0;%>
        <s:iterator value="contactInfoList">
            <%  sId++;%>
            <tr>
                <td><%=  sId%></td>
                <td><s:property value="contactName"/></td>
                <td><s:property value="contactNumber"/></td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="contactId"/>" class="btn btn-xs green contact_recharge">
                        Recharge
                    </a>
                    <a href="javascript:void(0);" id="<s:property value="contactId"/>" class="btn btn-xs red contact_delete">
                        Delete
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>