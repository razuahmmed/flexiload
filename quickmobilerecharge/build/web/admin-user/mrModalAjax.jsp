<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleMRHInfoList !=null">
    <s:if test="singleMRHInfoList.size() !=0">
        <s:iterator value="singleMRHInfoList">
            <s:if test="activeStatus=='Y'">
                <div class="alert alert-success m-a">
                    <strong>Success ! </strong> Transaction ID is - <s:property value="trid"/>
                </div>
            </s:if>
            <s:elseif test="activeStatus=='W'">
                <div class="alert alert-danger">
                    <strong>Waiting</strong> Transaction ID is - <s:property value="trid"/>
                </div>
            </s:elseif>
            <s:elseif test="activeStatus=='S'">
                <div class="alert alert-info">
                    <strong>Sent</strong> Transaction ID is - <s:property value="trid"/>
                </div>
            </s:elseif>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 30%">Order ID</th>
                        <th><s:property value="mobileRechargeId"/></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Sender</td>
                        <td><s:property value="sender"/></td>
                    </tr>
                    <tr>
                        <td>Your Balance</td>
                        <td><s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/></td>
                    </tr>
                    <tr>
                        <td>Receiver</td>
                        <td><s:property value="receiver"/></td>
                    </tr>
                    <tr>
                        <td>Operator</td>
                        <td><s:property value="operator"/></td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><s:property value="givenBalance"/></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>
                            <s:if test="type==0">
                                Prepaid
                            </s:if>
                            <s:else>
                                <span style="color: #009966;">
                                    Postpaid
                                </span>
                            </s:else>
                        </td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>
                            <s:if test="activeStatus=='W'">
                                <span>
                                    Waiting
                                </span>
                            </s:if>
                            <s:elseif test="activeStatus=='S'">
                                <span>
                                    Sent
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='Y'">
                                <span>
                                    Success
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='R'">
                                <span>
                                    Refunded
                                </span>
                            </s:elseif>
                        </td>
                    </tr>
                    <tr>
                        <td>Request Time</td>
                        <td><s:property value="purchasedOn"/></td>
                    </tr>
                    <tr>
                        <td>Transaction ID</td>
                        <td><s:property value="trid"/></td>
                    </tr>
                </tbody>
            </table>
            <h6>Orders For this number</h6>
            <div class="table-scrollable">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Sender</th>
                            <th>Phone</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int sId = 0;%>
                        <s:if test="allMRNumber !=null">
                            <s:if test="allMRNumber.size() !=0">
                                <s:iterator value="allMRNumber">
                                    <% sId++;%>
                                    <tr>
                                        <td><%= sId%></td>
                                        <td><s:property value="sender"/></td>
                                        <td><s:property value="receiver"/></td>
                                        <td><s:property value="purchasedOn"/></td>
                                        <td>
                                            <s:if test="activeStatus=='W'">
                                                <span>
                                                    Waiting
                                                </span>
                                            </s:if>
                                            <s:elseif test="activeStatus=='S'">
                                                <span>
                                                    Sent
                                                </span>
                                            </s:elseif>
                                            <s:elseif test="activeStatus=='Y'">
                                                <span>
                                                    Success
                                                </span>
                                            </s:elseif>
                                            <s:elseif test="activeStatus=='R'">
                                                <span>
                                                    Refunded
                                                </span>
                                            </s:elseif>
                                        </td>
                                    </tr>
                                </s:iterator>
                            </s:if>
                        </s:if>
                    </tbody>
                </table>
            </div>
        </s:iterator>
    </s:if>
</s:if>
<s:if test="singleMRHInfoList == null">
    no record found
</s:if>