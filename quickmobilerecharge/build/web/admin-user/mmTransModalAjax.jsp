<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleMmTransactionHInfoList !=null">
    <s:if test="singleMmTransactionHInfoList.size() !=0">
        <s:iterator value="singleMmTransactionHInfoList">
            <s:if test="type=='Y'">
                <div class="alert alert-success m-a">
                    <strong>Success ! </strong> Order ID is - <s:property value="mmTransactionHistoryId"/>
                </div>
            </s:if>
            <s:else>
                <div class="alert alert-info">
                    <strong>Waiting</strong>
                </div>
            </s:else>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 30%">Order ID</th>
                        <th><s:property value="mmTransactionHistoryId"/></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>From</td>
                        <td><s:property value="balanceGivenBy"/></td>
                    </tr>
                    <tr>
                        <td>Balance Type</td>
                        <td>Mobile Money</td>
                    </tr>
                    <tr>
                        <td>To</td>
                        <td><s:property value="userInfo.userId"/></td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><s:property value="addBalance"/></td>
                    </tr>
                    <tr>
                        <td>Request Time</td>
                        <td><s:property value="balanceGivenDate"/></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>
                            <s:if test="type=='Y'">
                                Transfer
                            </s:if>
                            <s:else>
                                <span style="color: #ff0033;">
                                    Waiting
                                </span>
                            </s:else>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h6>Orders For this Reseller</h6>
            <div class="table-scrollable">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Date</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><s:property value="mmTransactionHistoryId"/></td>
                            <td><s:property value="balanceGivenBy"/></td>
                            <td><s:property value="userInfo.userId"/></td>
                            <td><s:property value="balanceGivenDate"/></td>
                            <td>
                                <s:if test="type=='Y'">
                                    Transfer
                                </s:if>
                                <s:else>
                                    <span style="color: #ff0033;">
                                        Waiting
                                    </span>
                                </s:else>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </s:iterator>
    </s:if>
</s:if>
<s:if test="singleMmTransactionHInfoList == null">
    no record found
</s:if>