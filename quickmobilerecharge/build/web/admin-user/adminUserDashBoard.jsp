<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ADMIN USER PANEL</title>
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/admin-user/logo.png">

        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/animate.css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/glyphicons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/material-design-icons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/app.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font.css" type="text/css" />

    </head>
    <body class="container pace-done">
        <div class="app" id="app">

            <div id="aside" class="app-aside modal fade folded md nav-expand">
                <div class="left navside indigo-900 dk" layout="column">
                    <div class="navbar navbar-md no-radius">
                        <a class="navbar-brand">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24" height="24">
                            <path d="M 4 4 L 44 4 L 44 44 Z" fill="#03A9F4"></path>
                            <path d="M 4 4 L 34 4 L 24 24 Z" fill="rgba(0,0,0,0.15)"></path>
                            <path d="M 4 4 L 24 4 L 4  44 Z" fill="#6887ff"></path>
                            </svg>
                            <img src="<%= request.getContextPath()%>/admin-user/logo.png" alt="." class="hide">
                            <span class="hidden-folded inline">BB IT</span>
                        </a>
                    </div>
                    <div flex="" class="hide-scroll">
                        <nav class="scroll nav-active-primary">
                            <ul class="nav" ui-nav="">
                                <li class="nav-header hidden-folded">
                                    <small class="text-muted">Main</small>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserDashBoard">
                                        <span class="nav-icon">
                                            <i class="material-icons">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                                                <path d="M24,20c-7.72,0-14,6.28-14,14h4c0-5.51,4.49-10,10-10s10,4.49,10,10h4C38,26.28,31.721,20,24,20z" fill="#6887ff"></path>
                                                </svg>
                                            </i>
                                        </span>
                                        <span class="nav-text">Dashboard</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMobileRecharge">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Mobile Recharge</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMobileMoney">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Mobile Money</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMRTransactions">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">MR Transactions</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMMTransactions">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">MM Transactions</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="Home">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Back to Home</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div flex-no-shrink="">
                        <nav ui-nav="">
                            <ul class="nav">
                                <li>
                                    <div class="b-b b m-t-sm"></div>
                                </li>
                                <li class="no-bg">
                                    <a href="AdminUserLogout">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Logout</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            <div id="content" class="app-content box-shadow-z0" role="main">
                <div ui-view class="app-body" id="view">
                    <div class="padding">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="box">
                                    <div class="box-header">
                                        <h3>Recent Mobile Recharge Feed</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="streamline b-l m-l">
                                            <s:if test="allMRHInfoForAdminPanel !=null">
                                                <s:if test="allMRHInfoForAdminPanel.size() !=0">
                                                    <s:iterator value="allMRHInfoForAdminPanel">
                                                        <s:if test="activeStatus=='Y'">
                                                            <div class="sl-item b-success">
                                                                <div class="sl-icon">
                                                                    <i class="fa fa-check"></i>
                                                                </div>
                                                                <div class="sl-content">
                                                                    <div class="sl-date text-muted"><s:property value="purchasedOn"/></div>
                                                                    <div>
                                                                        <s:property value="receiver"/> Tk. <s:property value="givenBalance"/> by <s:property value="sender"/> [Success]
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </s:if>
                                                        <s:elseif test="activeStatus=='W'">
                                                            <div class="sl-item b-warning">
                                                                <div class="sl-content">
                                                                    <div class="sl-date text-muted"><s:property value="purchasedOn"/></div>
                                                                    <div>
                                                                        <s:property value="receiver"/> Tk. <s:property value="givenBalance"/> by <s:property value="sender"/> [Waiting]
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </s:elseif>
                                                        <s:elseif test="activeStatus=='S'">
                                                            <div class="sl-item b-warning">
                                                                <div class="sl-content">
                                                                    <div class="sl-date text-muted"><s:property value="purchasedOn"/></div>
                                                                    <div>
                                                                        <s:property value="receiver"/> Tk. <s:property value="givenBalance"/> by <s:property value="sender"/> [Sent]
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </s:elseif>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                            <s:if test="allMRHInfoForAdminPanel == null">
                                                no record found
                                            </s:if>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-6">
                                <div class="box">
                                    <div class="box-header">
                                        <h3>Recent Mobile Money Feed</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="streamline b-l m-l">
                                            <s:if test="allMMHInfoForAdminPanel !=null">
                                                <s:if test="allMMHInfoForAdminPanel.size() !=0">
                                                    <s:iterator value="allMMHInfoForAdminPanel">
                                                        <s:if test="activeStatus=='Y'">
                                                            <div class="sl-item b-success">
                                                                <div class="sl-icon">
                                                                    <i class="fa fa-check"></i>
                                                                </div>
                                                                <div class="sl-content">
                                                                    <div class="sl-date text-muted"><s:property value="purchasedOn"/></div>
                                                                    <div>
                                                                        <s:property value="receiver"/> Tk. <s:property value="givenBalance"/> by <s:property value="sender"/> [Success]
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </s:if>
                                                        <s:elseif test="activeStatus=='W'">
                                                            <div class="sl-item b-warning">
                                                                <div class="sl-content">
                                                                    <div class="sl-date text-muted"><s:property value="purchasedOn"/></div>
                                                                    <div>
                                                                        <s:property value="receiver"/> Tk. <s:property value="givenBalance"/> by <s:property value="sender"/> [Waiting]
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </s:elseif>
                                                        <s:elseif test="activeStatus=='S'">
                                                            <div class="sl-item b-warning">
                                                                <div class="sl-content">
                                                                    <div class="sl-date text-muted"><s:property value="purchasedOn"/></div>
                                                                    <div>
                                                                        <s:property value="receiver"/> Tk. <s:property value="givenBalance"/> by <s:property value="sender"/> [Sent]
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </s:elseif>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                            <s:if test="allMMHInfoForAdminPanel == null">
                                                no record found
                                            </s:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/admin-user/ui-nav.js" type="text/javascript"></script>

        <script type="text/javascript">
            function ReloadPage() {
                location.reload();
            }
        </script>


        <script>
            jQuery(document).ready(function () {
                setTimeout("ReloadPage()", 180000);
            });
        </script>

    </body>
</html>
