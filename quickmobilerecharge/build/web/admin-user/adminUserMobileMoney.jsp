<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ADMIN USER PANEL</title>
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/admin-user/logo.png">

        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/animate.css"/>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/glyphicons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/material-design-icons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/app.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font.css" type="text/css" />

    </head>
    <body class="container pace-done">
        <div class="app" id="app">

            <div id="aside" class="app-aside modal fade folded md nav-expand">
                <div class="left navside indigo-900 dk" layout="column">
                    <div class="navbar navbar-md no-radius">
                        <a class="navbar-brand">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24" height="24">
                            <path d="M 4 4 L 44 4 L 44 44 Z" fill="#03A9F4"></path>
                            <path d="M 4 4 L 34 4 L 24 24 Z" fill="rgba(0,0,0,0.15)"></path>
                            <path d="M 4 4 L 24 4 L 4  44 Z" fill="#6887ff"></path>
                            </svg>
                            <img src="<%= request.getContextPath()%>/admin-user/logo.png" alt="." class="hide">
                            <span class="hidden-folded inline">BB IT</span>
                        </a>
                    </div>
                    <div flex="" class="hide-scroll">
                        <nav class="scroll nav-active-primary">
                            <ul class="nav" ui-nav="">
                                <li class="nav-header hidden-folded">
                                    <small class="text-muted">Main</small>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserDashBoard">
                                        <span class="nav-icon">
                                            <i class="material-icons">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                                                <path d="M24,20c-7.72,0-14,6.28-14,14h4c0-5.51,4.49-10,10-10s10,4.49,10,10h4C38,26.28,31.721,20,24,20z" fill="#6887ff"></path>
                                                </svg>
                                            </i>
                                        </span>
                                        <span class="nav-text">Dashboard</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMobileRecharge">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Mobile Recharge</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMobileMoney">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Mobile Money</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMRTransactions">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">MR Transactions</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMMTransactions">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">MM Transactions</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="Home">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Back to Home</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div flex-no-shrink="">
                        <nav ui-nav="">
                            <ul class="nav">
                                <li>
                                    <div class="b-b b m-t-sm"></div>
                                </li>
                                <li class="no-bg">
                                    <a href="AdminUserLogout">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Logout</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            <div id="content" class="app-content box-shadow-z0" role="main">
                <div ui-view="" class="app-body" id="view">
                    <div class="padding">
                        <div class="box">
                            <div class="box-header">
                                <h2>Recent Mobile Money Requests</h2>
                            </div>
                            <div>
                                <table class="table m-b-none table-responsive no-paging footable-loaded footable" ui-jp="footable" data-filter="#filter" data-page-size="100">
                                    <thead>
                                        <tr>
                                            <th data-toggle="true" class="footable-sortable">
                                                ID
                                                <span class="footable-sort-indicator"></span>
                                            </th>
                                            <th class="footable-sortable">Sender<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Receiver<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Amount<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Type<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Status<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Transaction ID<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Operator<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Order Time<span class="footable-sort-indicator"></span></th>
                                            <th class="footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_render">
                                        <s:if test="allMMHInfoForAdminPanel !=null">
                                            <s:if test="allMMHInfoForAdminPanel.size() !=0">
                                                <s:iterator value="allMMHInfoForAdminPanel">
                                                    <tr style="display: table-row;" class="footable-even">
                                                        <td><span class="footable-toggle"></span><s:property value="mobileMoneyId"/></td>
                                                        <td><s:property value="sender"/></td>
                                                        <td>
                                                            <s:property value="receiver"/>
                                                        </td>
                                                        <td><s:property value="givenBalance"/></td>
                                                        <td><s:property value="type"/></td>
                                                        <td>
                                                            <s:if test="activeStatus=='Y'">
                                                                <span class="label success" title="Active">
                                                                    Success
                                                                </span>
                                                            </s:if>
                                                            <s:elseif test="activeStatus=='W'">
                                                                <span class="label accent" title="Active">
                                                                    Waiting
                                                                </span>
                                                            </s:elseif>
                                                            <s:elseif test="activeStatus=='S'">
                                                                <span class="label green" title="Active">
                                                                    Sent
                                                                </span>
                                                            </s:elseif>
                                                        </td>
                                                        <td><s:property value="trid"/></td>
                                                        <td><s:property value="operator"/></td>
                                                        <td><s:property value="purchasedOn"/></td>
                                                        <td>
                                                            <button id="<s:property value="mobileMoneyId"/>" class="md-btn md-raised m-t-sm w-xs indigo mm_btn_view" data-toggle="modal" data-target="#view_order" ui-toggle-class="modal-open-aside" ui-target="body">View</button>
                                                        </td>
                                                    </tr>
                                                </s:iterator>
                                            </s:if>
                                        </s:if>
                                        <s:if test="allMMHInfoForAdminPanel == null">
                                            <tr>
                                                <td style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;" colspan="20">
                                                    no record found
                                                </td>
                                            </tr>
                                        </s:if>
                                    </tbody>
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                <ul class="pagination">
                                                    <li class="footable-page-arrow disabled">
                                                        <a data-page="first" href="#first">«</a>
                                                    </li>
                                                    <li class="footable-page-arrow disabled">
                                                        <a data-page="prev" href="#prev">‹</a>
                                                    </li>
                                                    <li class="footable-page active">
                                                        <a data-page="0" href="#">1</a>
                                                    </li>
                                                    <li class="footable-page-arrow disabled">
                                                        <a data-page="next" href="#next">›</a>
                                                    </li>
                                                    <li class="footable-page-arrow disabled">
                                                        <a data-page="last" href="#last">»</a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="modal fade inactive" id="view_order" data-backdrop="false" ui-jp="flmcs_mm">
                            <div class="right w-xl white b-l">
                                <div class="row-col">
                                    <a data-dismiss="modal" class="pull-right text-muted text-lg p-a-sm m-r-sm">×</a>
                                    <div class="p-a b-b">
                                        <span class="h5" id="display_order_id">...</span>
                                    </div>
                                    <div class="row-row">
                                        <div class="row-body">
                                            <div class="row-inner" id="order_details">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/admin-user/tether.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/admin-user/ui-nav.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/admin-user/admin_mmoney.js" type="text/javascript"></script>

        <script type="text/javascript">
            function ReloadPage() {
                location.reload();
            }
        </script>

        <script>
            jQuery(document).ready(function () {
                setTimeout("ReloadPage()", 180000);
            });
        </script>

    </body>
</html>
