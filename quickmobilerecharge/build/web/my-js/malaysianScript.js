
var operators = $('.operators');

operators.change(function () {

    var keyword = $(this).val();

    var data = '';

    if (keyword == "Digi" || keyword == "Celcom Xpax" || keyword == "Umobile"
            || keyword == "Tunetalk" || keyword == "XOX"
            || keyword == "Merchantrade") {
        data = [5, 10, 15, 20, 30, 50, 60, 100];
    } else if (keyword == "Hotlink Speed") {
        data = [5, 10, 20, 30, 50, 60, 100];
    } else if (keyword == "RM15") {
        data = [15];
    } else if (keyword == "Speakout") {
        data = [5, 10, 18, 30, 40, 50];
    } else if (keyword == "Tron") {
        data = [10];
    } else if (keyword == "Altel") {
        data = [5, 10, 20, 30];
    }

    var rateList = $('.rate-list');

    rateList.empty();

    $.each(data, function (index, value) {

        var checked = "";

        if (index === 0) {
            checked = "checked";
        }
        rateList.append("<li> <div class=\"selectarea radiogroup\"> <input " + checked + " type=\"radio\" value=\"" + value + "\" required name=\"amount\"> </div> RM " + value + " </li>");
    });
});

function phoneTest(a) {
    var b = /^[0-9]+$/;
    return b.test(a)
}

function showError(a, b) {
    alert(b);
    a.focus();
    return false;
}

function sendMRvalidate(a, b, c) {

    var d = document.getElementById("phone").value;

    var reTypePhone = document.getElementById("reTypePhone").value;

    if (d == "") {
        return showError(a, "Mobile No cannot be empty");
    }

    var h = phoneTest(d);

    if (h == true) {
        if (d != reTypePhone) {
            alert("Mobile number do not match !");
            return false;
        }

        var rechargeType = $('input[name=recharge-type]:radio:checked').val();
        var amount = '';
        if (rechargeType != 0) {
            amount = $('#postAmount').val();
            if (amount != '' && amount > 0) {
                if (amount < 200) {
                    alert("Postpaid mobile recharge amount cannot be less than 200");
                    return false;
                }
                if (amount > 10000) {
                    alert("Postpaid mobile recharge amount cannot be greater than 10000");
                    return false;
                }
            } else {
                alert("Invalid postpaid mobile recharge amount");
                return false;
            }
        } else {
            amount = $('input[name=amount]:radio:checked').val();
        }
        return actionConfirmation("You are sending Tk." + amount + " to mobile number " + d + ". Please Make sure- Phone Number, Amount and Type is Correct. Click OK to send or Cancel to Cancel");
    } else {
        return showError(a, "Invalid Mobile No")
    }
}

function actionConfirmation(a) {
    var b = confirm(a);
    if (b == true) {
        sendMobileRecharge();
        return true;
    } else {
        return false;
    }
}

function showLastMRHistory() {
    $.ajax({
        type: "POST",
        url: 'ShowLastMRHistory',
        success: function (data) {
            $("#lastMRHTable").html(data);
        }
    });
}

function sendMobileRecharge() {

    var rechargeType = $('input[name=recharge-type]:radio:checked').val();

    var amount = '';
    if (rechargeType == 0) {
        amount = $('input[name=amount]:radio:checked').val();
    } else {
        amount = $('#postAmount').val();
    }

    var selectedOperator = $('input[name=operator]:radio:checked').val();

    var phone = document.getElementById("phone").value;

    var pin = document.getElementById("pin").value;

    var dataString = 'phone=' + phone;
    dataString += '&amount=' + amount;
    dataString += '&pin=' + pin;
    dataString += '&type=' + rechargeType;
    dataString += '&operator=' + selectedOperator;

    document.getElementById('createLoadingImage').src = 'images/loading.gif';

    $.ajax({
        type: "POST",
        url: 'SendMalaysianMobileRecharge',
        data: dataString,
        success: function (data) {
            document.getElementById('createLoadingImage').src = '';
            $("#message").html(data);
            showLastMRHistory();
        }
    });
}