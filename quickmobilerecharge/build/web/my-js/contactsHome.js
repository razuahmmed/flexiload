

$(document).ready(function () {

    var ajax_modal = $('#ajax-modal');

    function load_contacts() {
        $.get('ShowContactsList', function (data) {
            $("#tb").html(data);
        });
    }

    $("#my_contacts").on('click', function (e) {

        e.preventDefault();

//        $('body').modalmanager('loading');

        ajax_modal.load('.my_contacts', '', function () {

            load_contacts();

            $('#ajax-modal').modal();
        });
    });

    ajax_modal.on('click', '.contact_save', function (e) {

        e.preventDefault();

        var contact_save = $(".contact_save");

        contact_save.prop('disabled', true);

        contact_save.html("Saving...");

        var cName = $('#contact_name').val();

        var cNumber = $('#contact_number').val();

        if (cName != '') {

            var message = confirm("Do you want to add contact ?");

            if (message == true) {

                var dataString = 'contactName=' + cName;
                dataString += '&contactNumber=' + cNumber;

                $.post('ContactsAdd', dataString)
                        .done(function (data) {

                            contact_save.prop('disabled', false);

                            contact_save.html("Save");

                            $("#contact_resp").html(data);

                            load_contacts();
                        });
            } else {
                contact_save.prop('disabled', false);
                contact_save.html("Save");
            }
        } else {

            alert('Contact name can not be empty');

            contact_save.prop('disabled', false);

            contact_save.html("Save");
        }
    });

    ajax_modal.on('click', '.contact_recharge', function (e) {

        e.preventDefault();

        var cid = this.id;

        $.get('ContactsGetHome', {contactId: cid}, function (data) {
//            $("#spanPhone").html(data);
//            $("#spanrePhone").html(data);
//            $('#ajax-modal').modal('hide');
//            $('#mrf')[0].reset();
        });
    });

    ajax_modal.on('click', '.contact_delete', function (e) {

        e.preventDefault();

        var cid = this.id;

        var message = confirm("Do you want to delete this contact ?");

        if (message == true) {

            $.get('ContactsDelete', {contactId: cid}, function (data) {
                $('#ajax-modal').modal('hide');
                $("#message").html(data);
            });
        }
    });
});