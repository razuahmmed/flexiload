function mmPhoneTest(a) {
    var b = /^(\+88|88|)(\d{11})$/;
    return b.test(a)
}

function phoneTestDBBL(a) {
    var b = /^(\+88|88|)(\d{12})$/;
    return b.test(a)
}

function mmAmountTest(a) {
    var b = /^[1-9][0-9]+$/;
    return b.test(a)
}

function mmShowError(a, b) {
    alert(b);
    a.focus();
    return false;
}

function validateMMInHome(a, b, c) {

    var operator = document.getElementById("mmOperatorList").value;

    var d = document.getElementById("mmPhone").value;

    var mmReTypePhone = document.getElementById("mmReTypePhone").value;

    if (d == "") {
        return mmShowError(a, "Mobile No cannot be empty");
    }

    if (operator == "DBBL") {
        var h = phoneTestDBBL(d);
    } else {
        var h = mmPhoneTest(d);
    }

    if (h == true) {

        var phone = d;

        if (operator == "DBBL") {
            if (d.length > 12) {
                d = d.substring(d.length - 12);
            }
        } else {
            if (d.length > 11) {
                d = d.substring(d.length - 11);
            }
        }

        var i = d.substring(0, 3);

        if (i == "019" || i == "018" || i == "017" || i == "016" || i == "015") {

            if (phone != mmReTypePhone) {
                alert("Mobile number do not match !");
                return false;
            }

            var g = document.getElementById("mmAmount").value;

            if (g == "") {
                return mmShowError(b, "Mobile Money amount cannot be empty");
            }

            if (g < 500) {
                return mmShowError(b, "Amount cannot be less than 500.");
            }

            if (g > 50000) {
                return mmShowError(b, "Amount cannot be greater than 50000.");
            }

            var am = mmAmountTest(g);

            if (am == true) {
                mmConfirmPage();
            } else {
                return mmShowError(b, "Invalid amount");
            }
        } else {
            return mmShowError(a, "Invalid Mobile No");
        }
    } else {
        return mmShowError(a, "Invalid Mobile No")
    }
}

function mmConfirmPage() {

    var phoneNo = document.getElementById("mmPhone").value;

    var operatorTest = document.getElementById("mmOperatorList").value;

    if (operatorTest == "DBBL") {
        if (phoneNo.length > 12) {
            phoneNo = phoneNo.substring(phoneNo.length - 12);
        }
    } else {
        if (phoneNo.length > 11) {
            phoneNo = phoneNo.substring(phoneNo.length - 11);
        }
    }

    var i = phoneNo.substring(0, 3);

    if (i == "019") {
        i = "Banglalink";
    } else if (i == "018") {
        i = "Robi";
    } else if (i == "017") {
        i = "Grameen Phone";
    } else if (i == "016") {
        i = "Airtel";
    } else if (i == "015") {
        i = "Teletalk";
    } else {
        return mmShowError(a, "Invalid mobile operator");
    }

    var operator = document.getElementById("mmOperatorList").value;

    var type = document.getElementById('mmType').value;

    var phone = document.getElementById("mmPhone").value;

    var amount = document.getElementById("mmAmount").value;

    var dataString = 'phone=' + phone;
    dataString += '&amount=' + amount;
    dataString += '&type=' + type;
    dataString += '&mmOperator=' + operator;
    dataString += '&operator=' + i;

    window.location = 'MMConfirmPage?' + dataString;
}