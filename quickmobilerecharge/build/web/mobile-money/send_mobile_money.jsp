<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile&nbsp;Money&nbsp;<small>Send&nbsp;bKash,&nbsp;DBBL,&nbsp;mCash,&nbsp;uCash,&nbsp;MobiCash&nbsp;etc.</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div id="<s:property value="mess"/>ms" class="alert alert-<s:property value="messageColor"/> fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-times"></i>
                        <s:property value="messageString"/>
                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div class="row">
                        <div class=" col-md-12" style="margin-bottom: 5px;">
                            <% if (PM03CM01 || PM04CM01) {%>
                            <% if (PERMISSION_MR || PERMISSION_MM) {%>
                            <h4>Quick&nbsp;Access</h4>
                            <% if (PM03CM01) {%>
                            <% if (PERMISSION_MR) {%>
                            <a href="MalaysiaMobileRecharge" class="icon-btn">
                                <i class="icon-screen-smartphone"></i>
                                <div>Malaysia MR</div>
                            </a>
                            <a href="SendMobileRecharge" class="icon-btn">
                                <i class="icon-paper-plane"></i>
                                <div>BD MR</div>
                            </a>
                            <a href="MobileRechargeHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MR&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>

                            <% if (PM04CM01) {%>
                            <% if (PERMISSION_MM) {%>
                            <a href="SendMobileMoney" class="icon-btn">
                                <i class="icon-wallet"></i>
                                <div>Send&nbsp;MM</div>
                            </a>
                            <a href="MobileMoneyHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MM&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>
                            <% }%>
                            <% }%>

                            <% if (groupId < 5) {%>
                            <a href="Payments?loadType=MM" class="icon-btn">
                                <i class="icon-credit-card"></i>
                                <div>Payments</div>
                            </a>
                            <a href="ShowAllResellers" class="icon-btn">
                                <i class="icon-users"></i>
                                <div>Resellers</div>
                            </a>

                            <% if (groupId == 0) {%>
                            <a href="AddResellerSubAdmin" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 1) {%>
                            <a href="AddReseller4" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 2) {%>
                            <a href="AddReseller3" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 3) {%>
                            <a href="AddReseller2" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 4) {%>
                            <a href="AddReseller1" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% }%>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-4">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption text-info">
                                        <strong>Your&nbsp;MM&nbsp;Balance&nbsp;Tk.
                                            <s:if test="currentMmBalInfoList !=null">
                                                <s:if test="currentMmBalInfoList.size() !=0">
                                                    <s:iterator value="currentMmBalInfoList">
                                                        <s:property value="mmCurrentBalance"/>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </strong>
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <form role="form">
                                        <div class="form-body">
                                            <div class="form-group has-success">
                                                <label><strong>Mobile&nbsp;Money&nbsp;Operator</strong></label>
                                                <select id="operatorList" name="operatorList" class="form-control input-small">
                                                    <option value="bKash">bKash</option>
                                                    <option value="DBBL">DBBL</option>
                                                    <option value="mCash">mCash</option>
                                                </select>
                                            </div>

                                            <div class="form-group has-success">
                                                <label>
                                                    <strong>Type</strong>
                                                </label>
                                                <select id="type" name="type" class="form-control input-small">
                                                    <option value="Personal" selected>Personal</option>
                                                    <option value="Agent">Agent</option>
                                                </select>
                                            </div>
                                            <input type="hidden" name="type" value="Personal">

                                            <div class="form-group has-success">
                                                <label><strong><span style="color: #0033cc;" id="mmOperator">bKash</span>&nbsp;Phone &nbsp;Number</strong></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-arrow-right"></i></span>
                                                    <input type="text" id="phone" name="phone" style="font-size:20px; color:#C00" class="form-control input-lg">
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label><strong><span>Re-type</span>&nbsp;Phone &nbsp;Number</strong></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-arrow-right"></i></span>
                                                    <input type="text" id="reTypePhone" name="reTypePhone" style="font-size:20px; color:#C00" class="form-control input-lg">
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label><strong>Amount</strong></label>
                                                <div class="input-group input-medium">
                                                    <span class="input-group-addon"><i class="icon-credit-card"></i></span>
                                                    <input type="text" id="amount" name="amount" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <s:if test="pinStatus !=null">
                                                <s:if test='pinStatus=="N"'>
                                                    <button type="button" onclick="sendMMValidate();" disabled class="btn blue">
                                                        <i class="icon-check"></i>
                                                        Send
                                                    </button>
                                                </s:if>
                                                <s:else>
                                                    <button type="button" onclick="sendMMValidate();"class="btn blue">
                                                        <i class="icon-check"></i>
                                                        Send
                                                    </button>
                                                </s:else>
                                            </s:if>
                                            <button type="reset" class="btn default">Cancel</button>
                                            <img id="createLoadingImage" src="" alt="" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class=" col-md-8   ">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">Last&nbsp;20&nbsp;Mobile&nbsp;Money&nbsp;Requests</div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>TrID</th>
                                                    <th>Sent&nbsp;By</th>
                                                    <th>Number</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>Operator</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lastMMHTable">
                                                <s:if test="lastMMoneyHInfoList !=null">
                                                    <s:if test="lastMMoneyHInfoList.size() !=0">
                                                        <s:iterator value="lastMMoneyHInfoList" var="lastMMHInfo">
                                                            <tr>
                                                                <td>${lastMMHInfo.trid}</td>
                                                                <td>${lastMMHInfo.sender}</td>
                                                                <td>${lastMMHInfo.receiver}</td>
                                                                <td>${lastMMHInfo.type}</td>
                                                                <td>${lastMMHInfo.givenBalance}</td>
                                                                <td>
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>${lastMMHInfo.operator}</td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/sendMMValidation.js" type="text/javascript"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {

                $('#operatorList').on('change', function (e) {
                    var mmop = $("option:selected", this);
                    var valueSelected = this.value;
                    $('#mmOperator').text(valueSelected);
                });

                $('#ms').hide();

                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>
    </body>
</html>