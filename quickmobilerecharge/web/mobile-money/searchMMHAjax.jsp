<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="allMMoneyHInfoList != null">
    <s:if test="allMMoneyHInfoList.size()!=0">
        <s:iterator value="allMMoneyHInfoList">
            <tr>
                <td><input type="checkbox" id="chkb" name="chkb" value="<s:property value="mobileMoneyId"/>"></td>
                <td align="center" class="sorting_1">
                    <s:property value="mobileMoneyId"/>
                </td>
                <td align="center">
                    <s:property value="purchasedOn"/>
                </td>
                <td align="center"><s:property value="sender"/></td>
                <td align="center"><s:property value="receiver"/></td>
                <td align="center"><s:property value="operator"/></td>
                <td align="center"><s:property value="type"/></td>
                <td align="center"><s:property value="givenBalance"/></td>
                <td align="center"><s:property value="totalAmount"/></td>
                <td align="center">
                    <s:if test="activeStatus=='Y'">
                        <span>
                            Success
                        </span>
                    </s:if>
                    <s:elseif test="activeStatus=='N'">
                        <span>
                            Pending
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='P'">
                        <span>
                            Processing
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='W'">
                        <span>
                            Waiting
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='F'">
                        <span>
                            Failed
                        </span>
                    </s:elseif>
                </td>
                <td align="center"><s:property value="trid"/></td>
                <td align="center"><s:property value="originator"/></td>
                <td align="center"><s:property value="operatorBalance"/></td>
                <td align="center">
                    <a href="javascript:void(0);"  id="<s:property value="mobileMoneyId"/>" class="fview btn btn-xs purple-medium btn-editable tooltips" data-container="body" data-placement="top" data-original-title="Show Details">
                        <i class="icon-list"></i>
                        View
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>