<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <script type="text/javascript">

            function showManageRate() {

                $.ajax({
                    type: "POST",
                    url: 'ShowAllManageRate',
                    success: function (data) {
                        $("#manageRateTable").html(data);
                    }
                });
            }
            //            
            function createManageRate() {

                var rateName = document.getElementById("rateName").value;

                var grameenPhone = document.getElementById("grameenPhone").value;

                var robi = document.getElementById("robi").value;

                var banglalink = document.getElementById("banglalink").value;

                var airtel = document.getElementById("airtel").value;

                var teletalk = document.getElementById("teletalk").value;

                var bkash = document.getElementById("bkash").value;

                var dbbl = document.getElementById("dbbl").value;
                //
                if ((notEmpty(rateName, "Rate Plan Name can\'t empty")) && (forRateName(rateName, "Invalid Rate Plan Name")) && (lengthRestriction(rateName, 3, 15, "rate plan name"))) {
                    flag = false;
                } else {
                    flag = true;
                }
                //
                var message = confirm("Do you want to create New Rate Plan ?");

                if (message == true) {

                    var flag = true;

                    if (flag) {

                        var dataString = 'rateName=' + rateName;
                        dataString += '&grameenPhone=' + grameenPhone;
                        dataString += '&robi=' + robi;
                        dataString += '&banglalink=' + banglalink;
                        dataString += '&airtel=' + airtel;
                        dataString += '&teletalk=' + teletalk;
                        dataString += '&bkash=' + bkash;
                        dataString += '&dbbl=' + dbbl;
                        //
                        document.getElementById('createLoadingImage').src = 'images/loading.gif';
                        //
                        $.ajax({
                            type: "POST",
                            url: 'AddNewManageRate',
                            data: dataString,
                            success: function (data) {
                                document.getElementById('createLoadingImage').src = '';
                                $("#message").html(data);
                                showManageRate();
                            }
                        });
                    }
                }
            }
            //
            function forRateName(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z_ ]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }
            //
            function isNumeric(id, helperMsg) {
                var numericExpression = /^[0-9]+$/;
                if (id.match(numericExpression)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Input Rates <small>BD Manage Rate Plans</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        BD&nbsp;Rate&nbsp;Plan
                                    </div>
                                </div>

                                <div class="portlet-body form">

                                    <form class="form-horizontal">
                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">BD Plan&nbsp;Name</label>
                                                <div class=" col-md-4">
                                                    <input type="text" id="rateName" name="rateName" class="form-control">
                                                </div>
                                            </div>

                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Operator&nbsp;Name</th>
                                                        <th>Operator&nbsp;Code</th>
                                                        <th>Commission&nbsp;(%)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Grameen&nbsp;Phone</th>
                                                        <td><input type="text" class="form-control" disabled value="017"></td>
                                                        <td><input type="text" id="grameenPhone" name="grameenPhone" value="0.00" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Robi</th>
                                                        <td><input type="text" class="form-control" disabled value="018"></td>
                                                        <td><input type="text" id="robi" name="robi" value="0.00" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Banglalink</th>
                                                        <td><input type="text" class="form-control" disabled value="019"></td>
                                                        <td><input type="text" id="banglalink" name="banglalink" value="0.00" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Airtel</th>
                                                        <td><input type="text" class="form-control" disabled value="016"></td>
                                                        <td><input type="text" id="airtel" name="airtel" value="0.00" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Teletalk</th>
                                                        <td><input type="text" class="form-control" disabled value="015"></td>
                                                        <td><input type="text" id="teletalk" name="teletalk" value="0.00" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">bKash</th>
                                                        <td><input type="text" class="form-control" disabled value=""></td>
                                                        <td><input type="text" id="bkash" name="bkash" value="0.00" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">DBBL</th>
                                                        <td><input type="text" class="form-control" disabled value=""></td>
                                                        <td><input type="text" id="dbbl" name="dbbl" value="0.00" class="form-control"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="form-actions fluid">
                                            <div class=" col-md-offset-3 col-md-9">
                                                <button type="button" onclick="createManageRate();" class="btn blue">
                                                    <i class="icon-check"></i>
                                                    Add
                                                </button>
                                                <button type="reset" class="btn default">Cancel</button>
                                                <img id="createLoadingImage" src="" alt="" />
                                            </div>
                                        </div>
                                    </form>

                                    <hr>

                                    <!-- BEGIN FORM-->
                                    <div style="padding-top: 16px;" class="table-scrollable">
                                        <table id="datatable_orders" class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="padding-top: 12px;"><i style="margin-top: -2px;" class="icon-list">&nbsp;</i>Plan&nbsp;Name</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody id="manageRateTable">
                                                <s:if test="allManageRateInfoList !=null">
                                                    <s:if test="allManageRateInfoList.size() !=0">
                                                        <s:iterator value="allManageRateInfoList">
                                                            <tr>
                                                                <td>
                                                                    <a href="ManageRateDetailsView?manageRateId=<s:property value="manageRateId"/>">
                                                                        <s:property value="rateName"/>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a href="ManageRateDetailsView?manageRateId=<s:property value="manageRateId"/>" class="btn btn-primary btn-xs purple">
                                                                        <i class="icon-pencil"></i>
                                                                        View&nbsp;/&nbsp;Edit
                                                                    </a>

                                                                    <a href="javascript:void(0);" id="<s:property value="manageRateId"/>" class="btn btn-danger btn-xs purple btn_delete">
                                                                        <i class="icon-trash"></i>
                                                                        Delete
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script type="text/javascript">

            $(document).on("click", '.btn_delete', function (e) {

                e.preventDefault();

                var id = this.id;

                var warning = confirm("Do you want to delete Rate Plan ?");

                if (warning == true) {

                    var dataString = 'manageRateId=' + id;
                    //
                    document.getElementById('createLoadingImage').src = 'images/loading.gif';
                    //
                    $.post('DeleteManageRate', dataString).done(function (data) {
                        document.getElementById('createLoadingImage').src = '';
                        $("#message").html(data);
                        showManageRate();
                    });
                }
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable_orders').DataTable({
                    "orderCellsTop": true,
                    "pagingType": "full_numbers"
                });
            });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
            });
        </script>

    </body>
</html>