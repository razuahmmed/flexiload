<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <style type="text/css">
            .table {
                counter-reset:section;
            }

            .count:before {
                counter-increment:section;
                content:counter(section);
            }
        </style>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Input Rates <small>Manage Rate Plans</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Rate&nbsp;Plan
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <form id="rate-form" class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Plan&nbsp;Name</label>
                                            <div class="col-md-4">
                                                <input type="text" id="rateName" name="rateName" class="form-control">
                                            </div>
                                        </div>

                                        <div class="table-scrollable">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Serial</th>
                                                        <th>Operator&nbsp;Name</th>
                                                        <th>Operator&nbsp;Code</th>
                                                        <th>Commission&nbsp;(%)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center;" class="count"></td>
                                                        <th scope="row"><input type="text" id="operatorName" name="operatorName" placeholder="Teletalk" class="form-control"></th>
                                                        <td><input type="text" id="operatorCode" name="operatorCode" placeholder="015" class="form-control"></td>
                                                        <td><input type="text" id="rateCommission" name="rateCommission" placeholder="0.00" class="form-control"></td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td style="text-align: center;">
                                                            <button type="button" id="add-row" class="btn green">
                                                                <i class="icon-check"></i>
                                                                Add
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" id="remove-row" class="btn btn-danger">
                                                                <i class="icon-trash"></i>
                                                                Remove
                                                            </button>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="form-actions fluid">
                                            <div class=" col-md-offset-3 col-md-9">
                                                <button type="button" id="btn-insert" class="btn blue">
                                                    <i class="icon-check"></i>
                                                    Submit
                                                </button>
                                                <button type="reset" class="btn default">Cancel</button>
                                                <img id="createLoadingImage" src="" alt="" />
                                            </div>
                                        </div>
                                    </form>
                                    <hr>

                                    <div class="portlet light bordered">
                                        <table id="datatable_orders" class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="padding-top: 12px;"><i style="margin-top: -2px;" class="icon-list">&nbsp;</i>Plan&nbsp;Name</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody id="manageRateTable">
                                                <s:if test="manageRateInfoList !=null">
                                                    <s:if test="manageRateInfoList.size() !=0">
                                                        <s:iterator value="manageRateInfoList">
                                                            <tr>
                                                                <td>
                                                                    <a href="ManageRateDetailsView?manageRateId=<s:property value="manageRateId"/>">
                                                                        <s:property value="rateName"/>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a href="ManageRateDetailsView?manageRateId=<s:property value="manageRateId"/>" class="btn btn-primary btn-xs purple">
                                                                        <i class="icon-pencil"></i>
                                                                        View&nbsp;/&nbsp;Edit
                                                                    </a>

                                                                    <a href="javascript:void(0);" id="<s:property value="manageRateId"/>" class="btn btn-danger btn-xs purple btn_delete">
                                                                        <i class="icon-trash"></i>
                                                                        Delete
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script type="text/javascript">
            $(document).ready(function () {

                var item_remove = $('#remove-row');
                item_remove.hide();

                $('#datatable_orders').DataTable({
                    "orderCellsTop": true,
                    "pagingType": "full_numbers"
                });

                $("#btn-insert").click(function () {

                    var rtName = $("#rateName").val();
                    var opName = $("#operatorName").val();
                    var opCode = $("#operatorCode").val();
                    var opCom = $("#rateCommission").val();

                    if (notEmpty(rtName, "Rate Name blank not allow")
                            & notEmpty(opName, "Operator Name blank not allow")
                            & notEmpty(opCode, "Operator Code blank not allow")
                            & notEmpty(opCom, "Operator Commission blank not allow")) {

                        if (forRateName(rtName, "Rate Name format don't match")
                                & forRateName(opName, "Operator Name format don't match")
                                & isNumeric(opCode, "Operator Code format don't match")
                                & isNumeric(opCom, "Operator Commission format don't match")) {

                            if (lengthRestriction(rtName, 3, 25, "Rate Name")
                                    & lengthRestriction(opName, 3, 15, "Operator Name")
                                    & lengthRestriction(opCode, 2, 5, "Operator Code")
                                    & lengthRestriction(opCom, 1, 5, "Operator Commission")) {

                                var message = confirm("Do you want to create New Rate Plan ?");
                                if (message == true) {

                                    document.getElementById('createLoadingImage').src = 'images/loading.gif';
                                    $.post("NewManageRate", $("#rate-form").serialize(), function (data) {
                                        document.getElementById('createLoadingImage').src = '';
                                        $("#message").html(data);
                                        showManageRate();
                                    });
                                }
                            }
                        }
                    }
                });

                item_remove.on('click', function () {
                    $("#rate-form tr.info").fadeOut(300, function () {
                        $(this).remove();
                    });
                });

                $('#add-row').on('click', function () {
                    $("#rate-form").find('tbody')
                            .append('<tr>\n\
                                        <td style="text-align: center;" class="count"></td>\n\
                                        <th scope="row"><input type="text" id="operatorName" name="operatorName" placeholder="Teletalk" class="form-control remove_item"></th>\n\
                                        <td><input type="text" id="operatorCode" name="operatorCode" placeholder="015" class="form-control remove_item"></td>\n\
                                        <td><input type="text" id="rateCommission" name="rateCommission" placeholder="0.00" class="form-control remove_item"></td>\n\
                                    </tr>');
                });

                $('#rate-form').on('click', '.remove_item', function () {
                    $("tr").removeClass("info");
                    $(this).closest('tr').addClass("info");
                    item_remove.show();
                });

                $('.btn_delete').on('click', function () {

                    var id = this.id;

                    var warning = confirm("Do you want to delete Rate Plan ?");

                    if (warning == true) {

                        var dataString = 'manageRateId=' + id;
                        //
                        document.getElementById('createLoadingImage').src = 'images/loading.gif';
                        //
                        $.post('DeleteManageRate', dataString).done(function (data) {
                            document.getElementById('createLoadingImage').src = '';
                            $("#message").html(data);
                            showManageRate();
                        });
                    }
                });
            });
            //
            function showManageRate() {
                $.ajax({
                    type: "POST",
                    url: 'ShowAllManageRate',
                    success: function (data) {
                        $("#manageRateTable").html(data);
                    }
                });
            }
            //
            function forRateName(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z_ ]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }
            //
            function isNumeric(id, helperMsg) {
                var numericExpression = /^[0-9-.]+$/;
                if (id.match(numericExpression)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>
    </body>
</html>