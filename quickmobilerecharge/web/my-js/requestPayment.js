$(document).ready(function () {

    function getTypeAndParent() {

        var cid = $('#cid').val();

        var load_type = $("input[name=loadType]:checked").val();

        if ((cid != '') & (load_type != "")) {
            document.getElementById('createLoadingImage').src = 'images/loading.gif';
            $.post('GetResellerTypeAndParent', {
                resellerId: $('#cid').val()
            }).done(function (data) {
                $("#typeParentDiv").html(data);
            });
        }
    }

    function updateCurrentBalance() {

        var cid = $('#cid').val();

        var load_type = $("input[name=loadType]:checked").val();

        if ((cid != '') & (load_type != "")) {
            $.post('GetCurrentBalance', {
                resellerId: $('#cid').val(),
                loadType: $("input[name=loadType]:checked").val()
            }).done(function (data) {
                getTypeAndParent();
                document.getElementById('createLoadingImage').src = '';
                var cbal = $("#cBal");
                cbal.val(data);
            });
        }
    }

    updateCurrentBalance();

    $('input[type=radio][name=loadType]').change(function () {
        updateCurrentBalance();
    });

    $('#cid').select2().on("change", function (e) {
        updateCurrentBalance();
    });

    $("#btnSubmit").click(function () {

        var cid = $('#cid').val();

        var am_type = $("input[name=amountType]:checked").val();

        var load_type = $("input[name=loadType]:checked").val();

        var amount = $('#amount').val();

        var rq_id = $('#rq_id').val();

        if (cid != '') {

            if ((load_type != "") && (am_type != "") && (amount != 0) && (rq_id != "")) {

                var message = confirm("Do you want to " + am_type + " " + load_type + " balance to reseller " + cid + " ?");

                if (message == true) {

                    document.getElementById('btnCreateLoadingImage').src = 'images/loading.gif';

                    $.post('BalanceRequestResponse', {
                        resellerId: $('#cid').val(),
                        loadType: $("input[name=loadType]:checked").val(),
                        amount: $("#amount").val(),
                        amountType: $("input[name=amountType]:checked").val(),
                        pin: $("#pin").val(),
                        balReqID: $("#rq_id").val(),
                        description: $("#description").val()
                    }).done(function (data) {
                        document.getElementById('btnCreateLoadingImage').src = '';
                        $("#message").html(data);
                        updateCurrentBalance();
                        showPaymentsInfo();
                    });
                }
            } else {
                alert("Empty field not allow");
            }
        } else {
            alert("Please select user id");
        }
    });

    function showPaymentsInfo() {

        $.ajax({
            type: "POST",
            url: 'ShowPaymentsInfo',
            success: function (data) {
                $("#payments_tbl").html(data);
            }
        });
    }

    $(document).on("input", "#amount", function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });
});