$(document).ready(function () {

    var item_remove = $('#item-remove');
    item_remove.hide();

    item_remove.on('click', function () {
        $("#invoice_items tr.info").fadeOut(300, function () {
            $(this).remove();
        });
    });

    $('#blank-add').on('click', function () {
        $("#invoice_items").find('tbody')
                .append('<tr><td class="count"></td><td><input type="text" id="egPhone" name="egPhone" class="form-control input-medium item_name" value=""></td><td><input type="text" id="egType" name="egType" class="form-control input-small" value=""></td><td><input type="text" id="egAmount" name="egAmount" class="form-control input-small" value=""></td><td><input type="text" id="egModem" name="egModem" class="form-control input-small" value="1"></td></tr>');
    });

    $('#invoice_items').on('click', '.item_name', function () {
        $("tr").removeClass("info");
        $(this).closest('tr').addClass("info");
        item_remove.show();
    });

    function showGroup() {

        $.ajax({
            type: "POST",
            url: 'ShowGroup',
            success: function (data) {
                $("#glTable").html(data);
            }
        });
    }

    $("#submit").click(function (e) {

        e.preventDefault();

        var gName = ($("#groupName").val());

        if ((notEmpty(gName, "Group name can't be Empty")) && (forGroupName(gName, "Invalid group name")) && (lengthRestriction(gName, 4, 15, "group name"))) {

            var message = confirm("Do you want to update group ?");

            if (message == true) {

                document.getElementById('createLoadingImage').src = 'images/loading.gif';

                $.post('EditMRGroups', $('#ib_form').serialize(), function (data) {

                    setTimeout(function () {
                        $("#message").html(data);
                        document.getElementById('createLoadingImage').src = '';
                        showGroup();
                    }, 2000);
                });
            }
        }
    });

    function notEmpty(id, helperMsg) {
        if (id.length == 0) {
            alert(helperMsg);
            id.focus();
            return false;
        }
        return true;
    }

    function forGroupName(id, helperMsg) {
        var alphaExp = /^[0-9a-zA-Z_ ]+$/;
        if (id.match(alphaExp)) {
            return true;
        } else {
            alert(helperMsg);
            id.focus();
            return false;
        }
    }

    function lengthRestriction(id, min, max, msg) {
        var uInput = id;
        if (uInput.length >= min && uInput.length <= max) {
            return true;
        } else {
            alert("Please enter " + msg + " between " + min + " to " + max + " characters");
            id.focus();
            return false;
        }
    }
});