<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="mrGroupInfoList !=null">
    <s:if test="mrGroupInfoList.size() !=0">
        <s:iterator value="mrGroupInfoList">
            <tr>
                <td class="highlight">
                    <div class="success"></div>
                    <a href="#">
                        <s:property value="mrGroupName"/>
                    </a>
                </td>
                <td>
                    <a href="#" class="btn btn-xs purple-medium">
                        <i class="icon-paper-plane"></i>
                        Send Top Up
                    </a>
                    <a href="#" class="btn btn-primary btn-xs purple">
                        <i class="icon-pencil"></i>
                        View&nbsp;/&nbsp;Edit
                    </a>
                    <a href="#" class="btn btn-danger btn-xs purple">
                        <i class="icon-trash"></i>
                        Delete
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>