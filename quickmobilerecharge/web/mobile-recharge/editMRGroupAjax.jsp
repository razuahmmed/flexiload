<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleMRGroupInfoList !=null">
    <s:if test="singleMRGroupInfoList.size() !=0">
        <div class="form-group has-success">
            <label class="control-label">Group&nbsp;Name</label>
            <s:iterator value="singleMRGroupInfoList">
                <input type="text" id="groupName" name="groupName" value="<s:property value="mrGroupName"/>" class="form-control">
            </s:iterator>
        </div>
        <div class="table-scrollable">
            <table class="table table-condensed table-hover invoice-table" id="invoice_items">
                <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Phone*<br>[e.g. 01XXXXXXXXX ]</th>
                        <th>Type*<br>[Prepaid = 0 &amp; Postpaid = 1]</th>
                        <th>Amount*</th>
                        <th>Modem*</th>
                    </tr>
                </thead>
                <tbody id="groupTBody">
                    <s:iterator value="singleMRGroupInfoList">
                        <tr>
                            <td class="count">

                            </td>
                            <td>
                                <input type="text" id="phone0" name="phone0" class="form-control input-medium item_name pPhone" value="<s:property value="mrGroupInfo.mobileNumber"/>">
                            </td>
                            <td>
                                <input type="text" id="type0" name="type0" class="form-control input-small pType" value="<s:property value="mrGroupInfo.type"/>">
                            </td>
                            <td>
                                <input type="text" id="amount0" name="amount0" class="form-control input-small pAmount" value="<s:property value="mrGroupInfo.amount"/>">
                            </td>
                            <td>
                                <input type="text" id="modem0" name="modem0" class="form-control input-small pModem" value="<s:property value="mrGroupInfo.modemNumber"/>">
                            </td>
                        </tr>
                    </s:iterator>
                </tbody>
            </table>
        </div>
        <button type="button" class="btn green" id="blank-add">
            Add&nbsp;Number&nbsp;Row
        </button>
        <button type="button" class="btn btn-danger" id="item-remove">
            <i class="fa fa-minus-circle"></i>
            Delete
        </button>
        <hr>
        <button type="button" id="submitButton" onclick="createGroup();" class="btn btn-success">
            Edit&nbsp;Group
        </button>
        <hr>
    </s:if>
</s:if>