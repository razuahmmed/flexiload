// lazyload config
var MODULE_CONFIG = {
    easyPieChart:   [ 'ui/theme/flatkit/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ 'ui/theme/flatkit/libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ 'ui/theme/flatkit/libs/jquery/flot/jquery.flot.js',
                      'ui/theme/flatkit/libs/jquery/flot/jquery.flot.resize.js',
                      'ui/theme/flatkit/libs/jquery/flot/jquery.flot.pie.js',
                      'ui/theme/flatkit/libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      'ui/theme/flatkit/libs/jquery/flot-spline/js/jquery.flot.spline.min.js',
                      'ui/theme/flatkit/libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js'],
    vectorMap:      [ 'ui/theme/flatkit/libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      'ui/theme/flatkit/libs/jquery/bower-jvectormap/jquery-jvectormap.css', 
                      'ui/theme/flatkit/libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      'ui/theme/flatkit/libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js' ],
    dataTable:      [
                      'ui/theme/flatkit/libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                      'ui/theme/flatkit/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      'ui/theme/flatkit/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    footable:       [
                      'ui/theme/flatkit/libs/jquery/footable/dist/footable.all.min.js',
                      'ui/theme/flatkit/libs/jquery/footable/css/footable.core.css'
                    ],
    screenfull:     [
                      'ui/theme/flatkit/libs/jquery/screenfull/dist/screenfull.min.js'
                    ],
    sortable:       [
                      'ui/theme/flatkit/libs/jquery/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      'ui/theme/flatkit/libs/jquery/nestable/jquery.nestable.css',
                      'ui/theme/flatkit/libs/jquery/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      'ui/theme/flatkit/libs/jquery/summernote/dist/summernote.css',
                      'ui/theme/flatkit/libs/jquery/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      'ui/theme/flatkit/libs/jquery/parsleyjs/dist/parsley.css',
                      'ui/theme/flatkit/libs/jquery/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      'ui/theme/flatkit/libs/jquery/select2/dist/css/select2.min.css',
                      'ui/theme/flatkit/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      'ui/theme/flatkit/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      'ui/theme/flatkit/libs/jquery/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      'ui/theme/flatkit/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      'ui/theme/flatkit/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      'ui/theme/flatkit/libs/js/moment/moment.js',
                      'ui/theme/flatkit/libs/jquery/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    chart:          [
                      'ui/theme/flatkit/libs/js/echarts/build/dist/echarts-all.js',
                      'ui/theme/flatkit/libs/js/echarts/build/dist/theme.js',
                      'ui/theme/flatkit/libs/js/echarts/build/dist/jquery.echarts.js'
                    ],
    bootstrapWizard:[
                      'ui/theme/flatkit/libs/jquery/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      'ui/theme/flatkit/libs/jquery/moment/moment.js',
                      'ui/theme/flatkit/libs/jquery/fullcalendar/dist/fullcalendar.min.js',
                      'ui/theme/flatkit/libs/jquery/fullcalendar/dist/fullcalendar.css',
                      'ui/theme/flatkit/libs/jquery/fullcalendar/dist/fullcalendar.theme.css',
                      'scripts/plugins/calendar.js'
                    ],
    dropzone:       [
                      'ui/theme/flatkit/libs/js/dropzone/dist/min/dropzone.min.js',
                      'ui/theme/flatkit/libs/js/dropzone/dist/min/dropzone.min.css'
                    ],
    flmcs_recharge:       [
        'ui/lib/js/admin_recharge.js'
    ],
    flmcs_mm:       [
        'ui/lib/js/admin_mm.js'
    ],
    flmcs_add_fund:       [
        'ui/lib/js/admin_add_payment.js'
    ]
  };
