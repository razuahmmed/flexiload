<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <link href="<%= request.getContextPath()%>/my-css/comp-sep.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Modem <small>modem balance and latest sms info</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="tiles">
                                <div class="tile bg-blue-steel">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            GP
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-blue-steel">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Flexiload</h4>
                                        <p>     </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            GrameenPhone
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-red-thunderbird">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Robi
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-red-thunderbird">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>8383</h4>
                                        <p>     </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Robi
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-yellow-casablanca">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            BL
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-yellow-casablanca">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Topup</h4>
                                        <p>    </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Banglalink
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-red-flamingo">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Airtel
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-red-flamingo">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Recharge</h4>
                                        <p>       </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Airtel
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-green-turquoise">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            TL
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-green-turquoise">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Tele Recharge</h4>
                                        <p>      </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Teletalk
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="tiles">
                                <div class="tile bg-purple-seance">
                                    <div class="tile-body">
                                        <i class="icon-wallet"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            bKash
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-purple-seance">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>bKash</h4>
                                        <p>    </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            bKash
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-red-flamingo">
                                    <div class="tile-body">
                                        <i class="icon-wallet"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            DBBL
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-red-flamingo">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>DBBL</h4>
                                        <p>      </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            DBBL
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>


        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>

    </body>
</html>