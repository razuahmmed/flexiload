<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="allAdminUserInfoList !=null">
    <s:if test="allAdminUserInfoList.size() !=0">
        <s:iterator value="allAdminUserInfoList" var="adminUserInfo">
            <tr>
                <td class="highlight">
                    <div class="success"></div>
                    <a href="javascript:void(0);" onclick="adminUserDetailsView('<s:property value="adminUserId"/>');">
                        ${adminUserInfo.fullName}
                    </a>
                </td>
                <td>${adminUserInfo.phone}</td>
                <td>${adminUserInfo.email}</td>
                <td>${adminUserInfo.userName}</td>
                <td>${adminGroupInfo.adminGroupName}</td>
                <td>
                    <a href="javascript:void(0);" onclick="adminUserDetailsView('<s:property value="adminUserId"/>');" class="btn btn-primary btn-xs purple">
                        <i class="icon-pencil"></i>
                        View&nbsp;/&nbsp;Edit
                    </a>
                    <a href="javascript:void(0);" onclick="deleteAdminUser('<s:property value="adminUserId"/>');" class="btn btn-danger btn-xs purple">
                        <i class="icon-trash"></i>
                        Delete
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>