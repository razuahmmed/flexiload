<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Transaction&nbsp;<small>All Mobile Money Transaction history in your system</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span></span>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="5%">ID</th>
                                                    <th width="15%">Date</th>
                                                    <th width="13%">From</th>
                                                    <th width="12%">To</th>
                                                    <th width="10%">Amount</th>
                                                    <th width="8%">Type</th>
                                                    <th width="14%">Memo</th>
                                                    <th width="8%">Old Bal</th>
                                                    <th width="8%">New Bal</th>
                                                    <th width="7%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <s:if test="mmTransactionHInfoList !=null">
                                                    <s:if test="mmTransactionHInfoList.size() !=0">
                                                        <s:iterator value="mmTransactionHInfoList">
                                                            <tr>
                                                                <td><s:property value="mmTransactionHistoryId"/></td>
                                                                <td><s:property value="balanceGivenDate"/></td>
                                                                <td><s:property value="balanceGivenBy"/></td>
                                                                <td><s:property value="userInfo.userId"/></td>
                                                                <td><s:property value="addBalance"/></td>
                                                                <td>
                                                                    <s:if test="type=='Y'">
                                                                        Transfer
                                                                    </s:if>
                                                                    <s:else>
                                                                        <span style="color: #ff0033;">
                                                                            Waiting
                                                                        </span>
                                                                    </s:else>
                                                                </td>
                                                                <td><s:property value="mmBalanceDescription"/></td>
                                                                <td><s:property value="0"/></td>
                                                                <td><s:property value="0"/></td>
                                                                <td align="center">
                                                                    <a href="javascript:void(0);" id="<s:property value="mmTransactionHistoryId"/>" class="fview btn btn-xs blue btn-editable tooltips" data-container="body" data-placement="top" data-original-title="Show Details">
                                                                        <i class="icon-list"></i>
                                                                        View
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="4" style="text-align:right">Total:</th>
                                                    <th colspan="6" style="text-align: left"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/transactions_mobile_money.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable_orders').DataTable({
                    "order": [[0, "asc"]],
                    "orderCellsTop": true,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;
                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(4)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(4, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(4).footer()).html(
                                'Page Total ' + pageTotal + ' [ All Total ' + total + ' ]'
                                );
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                //initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                EcommerceOrders.init();
            });
        </script>
    </body>
</html>