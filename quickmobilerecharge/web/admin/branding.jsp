<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <link href="<%= request.getContextPath()%>/my-css/summernote.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/font-awesome.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                CMS <small>Edit Frontend Contents</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->


                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN EXTRAS PORTLET-->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-green-sharp bold uppercase">Edit Contents</span>
                                    </div>
                                </div>

                                <div class="portlet-body form">

                                    <form class="form-horizontal form-bordered">

                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="control-label col-xs-1">Contents</label>
                                                <div class="col-xs-11">

                                                    <div name="summernote" id="summernote_1">
                                                        <h4><span style="font-weight: bold; text-decoration: underline;">Welcome</span></h4>
                                                        Send Mobile Recharge instantly to friends &amp; family&nbsp;
                                                        <br>
                                                        Send bKash
                                                        <br>
                                                        Fast &amp; Most Advanced Billing Software
                                                        <br>
                                                        <br>
                                                        <h4 style="color: rgb(51, 51, 51);">
                                                            <span style="font-weight: bold; text-decoration: underline;">Safety Notice</span>
                                                        </h4>
                                                        <span style="line-height: 18.5714282989502px;">Use Strong Password,&nbsp;</span>
                                                        DO NOT share your password with anyone else
                                                        <span style="line-height: 18.5714282989502px;">&nbsp;</span>
                                                        <br style="line-height: 18.5714282989502px;">
                                                        <span style="line-height: 18.5714282989502px;">Use OTP for best security</span>
                                                        <br style="line-height: 18.5714282989502px;">
                                                        DO NOT use the save password option on your computer
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions fluid">
                                            <div class="col-xs-offset-3 col-xs-9">
                                                <!--<button type="submit" id="submit" class="btn blue">Save Contents</button>-->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>

        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART (Load javascripts at bottom, this will reduce page load time)-->
        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/summernote.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/branding.js" type="text/javascript"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>

    </body>
</html>