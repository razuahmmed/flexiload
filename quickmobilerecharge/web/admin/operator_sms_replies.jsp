<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                History <small>SMS Replies</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Operator SMS Replies
                                    </div>

                                    <div class="actions">
                                        <a href="#" class="btn btn-circle red btn-sm">
                                            <i class="icon-trash"></i> SMS Cleanup </a>
                                        <a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-hover" id="example">
                                                <thead>
                                                    <tr role="row" class="heading">
                                                        <th width="5%">ID</th>
                                                        <th width="12%">Logtime</th>
                                                        <th width="10%">Operator</th>
                                                        <th width="52%">SMS</th>
                                                        <th width="10%">Status</th>
                                                        <th width="2%">Modem</th>
                                                        <th width="7%">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1"></div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script type="text/javascript">

            $(document).ready(function () {

                //initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //UIIdleTimeout.init();

                //FOR DATA GRID
                onloadFunction();

                EcommerceOrders.init();
            });

            function onloadFunction() {
                $('#example').DataTable({
                    "pageLength": 10,
                    "order": [[0, "asc"], [2, "asc"]]
                });
            }
        </script>

    </body>
</html>