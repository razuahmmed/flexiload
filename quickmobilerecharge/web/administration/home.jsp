<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!-- PAGE HEADER -->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="page-title">
                                Home<small>&nbsp;Home&nbsp;Dashboard</small>
                            </h3>
                        </div>
                    </div>

                    <!-- BEGIN PAGE CONTENT-->

                    <!-- PAGE MARQUEE-->
                    <%@include file="/administration/marquee.jsp" %>

                    <div id="ms<s:property value="mes"/>" class="alert alert-<s:property value="messageColor"/> fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-times"></i>
                        <s:property value="messageString"/>
                    </div>

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div class="row">
                        <div class=" col-md-12" style="margin-bottom: 5px;">
                            <% if (PM03CM01 || PM04CM01) {%>
                            <% if (PERMISSION_MR || PERMISSION_MM) {%>
                            <h4>Quick&nbsp;Access</h4>
                            <% if (PM03CM01) {%>
                            <% if (PERMISSION_MR) {%>
                            <a href="MalaysiaMobileRecharge" class="icon-btn">
                                <i class="icon-screen-smartphone"></i>
                                <div>Malaysia MR</div>
                            </a>
                            <a href="SendMobileRecharge" class="icon-btn">
                                <i class="icon-paper-plane"></i>
                                <div>BD MR</div>
                            </a>
                            <a href="MobileRechargeHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MR&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>

                            <% if (PM04CM01) {%>
                            <% if (PERMISSION_MM) {%>
                            <a href="SendMobileMoney" class="icon-btn">
                                <i class="icon-wallet"></i>
                                <div>Send&nbsp;MM</div>
                            </a>
                            <a href="MobileMoneyHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MM&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>
                            <% }%>
                            <% }%>

                            <% if (groupId < 5) {%>
                            <a href="Payments" class="icon-btn">
                                <i class="icon-credit-card"></i>
                                <div>Payments</div>
                            </a>
                            <a href="ShowAllResellers" class="icon-btn">
                                <i class="icon-users"></i>
                                <div>Resellers</div>
                            </a>

                            <% if (groupId == 0) {%>
                            <a href="AddResellerSubAdmin" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 1) {%>
                            <a href="AddReseller4" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 2) {%>
                            <a href="AddReseller3" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 3) {%>
                            <a href="AddReseller2" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 4) {%>
                            <a href="AddReseller1" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% }%>
                        </div>
                    </div>

                    <% if (PM03CM01 || PM04CM01) {%>
                    <% if (PERMISSION_MR || PERMISSION_MM) {%>
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <hr/>
                            <h4>Service Option</h4>
                            <div class="leftpan services">
                                <ul class="radiogroup">
                                    <% if (PM03CM01 && PERMISSION_MR) {%>
                                    <li>
                                        <a href="MalaysiaMobileRecharge">
                                            <span class="purple"></span>
                                            <img src="<%= request.getContextPath()%>/images/malaysia-re.png" alt=""/>
                                            <span class="content">Malaysian Mobile Recharge</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="SendMobileRecharge">
                                            <span class="green"></span>
                                            <img src="<%= request.getContextPath()%>/images/bd-re.png" alt=""/>
                                            <span class="content">Bangladeshi Mobile Recharge</span>
                                        </a>
                                    </li>
                                    <% }%>

                                    <% if (PM04CM01 && PERMISSION_MM) {%>
                                    <li>
                                        <a href="SendMobileMoney">
                                            <span class="orange"></span>
                                            <img src="<%= request.getContextPath()%>/images/bd-mm.png" alt=""/>
                                            <span class="content">Send Mobile Money</span>
                                        </a>
                                    </li>
                                    <% }%>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <% }%>
                    <% }%>

                    <div class="row">
                        <% if (PM03CM01) {%>
                        <% if (PERMISSION_MR) {%>
                        <div class=" col-md-6">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">Last&nbsp;20&nbsp;Mobile&nbsp;Recharge&nbsp;Requests</div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Order</th>
                                                    <th>Sent&nbsp;By</th>
                                                    <th>Number</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>TrID</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lastMRHHomeTable">
                                                <s:if test="lastMRechargeHInfoList !=null">
                                                    <s:if test="lastMRechargeHInfoList.size() !=0">
                                                        <s:iterator value="lastMRechargeHInfoList" var="lastMRHInfo">
                                                            <tr>
                                                                <td>${lastMRHInfo.mobileRechargeId}</td>
                                                                <td>${lastMRHInfo.sender}</td>
                                                                <td>${lastMRHInfo.receiver}</td>
                                                                <td>
                                                                    <s:if test="type==0">
                                                                        Pepaid
                                                                    </s:if>
                                                                    <s:else>
                                                                        <span style="color: #009966;">
                                                                            Postpaid
                                                                        </span>
                                                                    </s:else>
                                                                </td>
                                                                <td>${lastMRHInfo.givenBalance}</td>
                                                                <td>
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>${lastMRHInfo.trid}</td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                                <s:if test="lastMRechargeHInfoList.size()==0">
                                                    <tr>
                                                        <td colspan="7" style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;">Haven't any mobile recharge requests</td>
                                                    </tr>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% }%>
                        <% }%>

                        <% if (PM04CM01) {%>
                        <% if (PERMISSION_MM) {%>
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">Last&nbsp;20&nbsp;Mobile&nbsp;Money&nbsp;Requests</div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>order</th>
                                                    <th>Sent&nbsp;By</th>
                                                    <th>Number</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>TrID</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lastMMHInHomeTable">
                                                <s:if test="lastMMoneyHInfoList !=null">
                                                    <s:if test="lastMMoneyHInfoList.size() !=0">
                                                        <s:iterator value="lastMMoneyHInfoList" var="lastMMHInfo">
                                                            <tr>
                                                                <td>${lastMMHInfo.mobileMoneyId}</td>
                                                                <td>${lastMMHInfo.sender}</td>
                                                                <td>${lastMMHInfo.receiver}</td>
                                                                <td>${lastMMHInfo.type}</td>
                                                                <td>${lastMMHInfo.givenBalance}</td>
                                                                <td>
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>${lastMMHInfo.trid}</td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                                <s:if test="lastMMoneyHInfoList.size()==0">
                                                    <tr>
                                                        <td colspan="7" style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;">Haven't any mobile money requests</td>
                                                    </tr>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% }%>
                        <% }%>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->

        <!-- PAGE FOOTER -->
        <%@include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/sendMRFromHome.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/sendMMFromHome.js" type="text/javascript"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {

                $('#mmOperatorList').on('change', function (e) {
                    var mmop = $("option:selected", this);
                    var valueSelected = this.value;
                    $('#mmOperator').text(valueSelected);
                });

                $('#ms').hide();

                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>

    </body>
</html>