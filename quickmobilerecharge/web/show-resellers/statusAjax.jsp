<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleResallerInfoList !=null">
    <s:if test="singleResallerInfoList.size() !=0">
        <s:iterator value="singleResallerInfoList">
            <s:if test="suspendActivity=='Y'">Active</s:if>
            <s:else>
                <b style="color: white;color: red;">Inactive</b>
            </s:else>
        </s:iterator>
    </s:if>
</s:if>