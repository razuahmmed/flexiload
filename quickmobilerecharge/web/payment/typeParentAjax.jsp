<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="singleResallerInfoList !=null">
    <s:iterator value="singleResallerInfoList">
        <div class="form-group has-success">
            <label class=" col-md-3   control-label">Reseller&nbsp;Type</label>
            <div class=" col-md-4  ">
                <div class="input-group">
                    <input type="text" name="" value="<s:property value="userGroupInfo.groupName"/>" disabled class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group has-success">
            <label class=" col-md-3   control-label">Parent&nbsp;This&nbsp;Reseller</label>
            <div class=" col-md-4  ">
                <div class="input-group">
                    <input type="text" name="" value="<s:property value="parentId"/>" disabled class="form-control">
                </div>
            </div>
        </div>
    </s:iterator>
</s:if>