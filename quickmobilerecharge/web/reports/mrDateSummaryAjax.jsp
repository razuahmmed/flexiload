<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<table class="table table-condensed table-hover">
    <thead>
        <tr>
            <th>Serial</th>
            <th>Sent By</th>
            <th>Number</th>
            <th>Type</th>
            <th>Amount</th>
            <th>TrID</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <% int id = 0;%>
        <s:if test="mrHByDateList !=null">
            <s:if test="mrHByDateList.size() !=0">
                <s:iterator value="mrHByDateList">
                    <% id++;%>
                    <tr  class="info">
                        <td><%= id%></td>
                        <td><s:property value="sender"/></td>
                        <td><s:property value="receiver"/></td>
                        <td>
                            <s:if test="type==0">
                                Pepaid
                            </s:if>
                            <s:else>
                                <span style="color: #009966;">
                                    Postpaid
                                </span>
                            </s:else>
                        </td>
                        <td><s:property value="givenBalance"/></td>
                        <td><s:property value="trid"/></td>
                        <td>
                            <s:if test="activeStatus=='Y'">
                                <span>
                                    Success
                                </span>
                            </s:if>
                            <s:elseif test="activeStatus=='N'">
                                <span>
                                    Pending
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='P'">
                                <span>
                                    Processing
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='W'">
                                <span>
                                    Waiting
                                </span>
                            </s:elseif>
                            <s:elseif test="activeStatus=='F'">
                                <span>
                                    Failed
                                </span>
                            </s:elseif>
                        </td>
                    </tr>
                </s:iterator>
            </s:if>
        </s:if>
        <s:if test="mrHByDateList.size() ==0">
            <tr>
                <td colspan="7" style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;">no history found</td>
            </tr>
        </s:if>
<!--        <tr style="font-size: 20px;font-weight: bold;">
            <th colspan="7" style="text-align: center;">Total Amount : <span id="total">0.00</span></th>
        </tr>-->
    </tbody>
</table>