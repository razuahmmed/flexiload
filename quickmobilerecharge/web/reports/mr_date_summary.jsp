<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <link href="<%= request.getContextPath()%>/my-css/comp-sep.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                History <small>mobile recharge history by date</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">


                        <div class="col-xs-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-body">
                                    <div id="dates">

                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>


                        <div class="col-xs-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Mobile Recharge History of : <span id="tdate"></span>
                                    </div>

                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-scrollable" id="result">

                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/moment.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-datepaginator.js" type="text/javascript"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {

                var g = $('#dates');

                $('#tdate').text(moment(g).format("dddd, MMMM Do YYYY"));

                var cDate = (moment(g).format("YYYY-MM-DD"));

                $.get("MobileRechargeSummaryByDate", {clickedDate: cDate}, function (data) {
                    $("#result").html(data);
                });

                var options = {
                    selectedDate: '2016-04-18',
                    endDate: '2016-04-18',
                    endDateFormat: '2016-04-18'
                }

                $('#dates').datepaginator({
                    endDate: '2016-04-18',
                    endDateFormat: '2016-04-18'
                });

                $('#dates').on('selectedDateChanged', function (event, date) {

                    $("#result").html("<h3>Loading.....</h3>");

                    $('#tdate').text(moment(date).format("dddd, MMMM Do YYYY"));

                    var fDate = date.format("YYYY-MM-DD");

                    $.get("MobileRechargeSummaryByDate", {clickedDate: fDate}, function (data) {
                        $("#result").html(data);
                    });
                });

                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>
    </body>
</html>