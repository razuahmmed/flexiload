<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Profile&nbsp;<small>Change&nbsp;Password</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row profile">
                        <div class=" col-md-12  ">
                            <!--BEGIN TABS-->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-green-sharp bold uppercase">Reset Resellers Password</span>
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable tabbable-custom tabbable-full-width">

                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_3" data-toggle="tab">Account</a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <!--tab_1_2-->
                                            <div class="tab-pane active" id="tab_1_3" >
                                                <div class=" col-md-4  ">
                                                    <div class="tab-content" id="pwd-container">

                                                        <div id="tab_3-3" class="tab-pane active">
                                                            <form id="reset-form">
                                                                <div class="form-group">
                                                                    <label class="control-label">Reseller ID</label>
                                                                    <s:if test="resellerId !=null">
                                                                        <input type="text" id="resellerId" name="resellerId" readonly value="<s:property value="resellerId"/>" class="form-control"/>
                                                                    </s:if>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New&nbsp;Password</label>
                                                                    <input type="password" id="newPassword" name="newPassword" class="form-control pws"/>
                                                                    <span class="help-block">Please Use a Strong Password</span>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="pwstrength_viewport_progress"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type&nbsp;New&nbsp;Password</label>
                                                                    <input type="password" id="confirmPassword" name="confirmPassword" class="form-control"/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <button type="button" id="btn-reset-pass" class="btn green">
                                                                        <i class="icon-check"></i>
                                                                        Reset Password
                                                                    </button>
                                                                    <img id="createLoadingImage" src="" alt="" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end tab-pane-->

                                            <!--end tab-pane-->

                                            <!--end tab-pane-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/passwordStrength.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/checkpw.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#btn-reset-pass").click(function () {
                    var newPassword = $("#newPassword").val();
                    var confirmPassword = $("#confirmPassword").val();
                    if ((notEmpty(newPassword, "Password can't be empty !")) & (forPassword(newPassword, "Invalid password Format !")) & (lengthRestriction(newPassword, 6, 18, "your password"))) {
                        if (newPassword != confirmPassword) {
                            alert("Password do not match !");
                            return false;
                        } else {
                            var message = confirm("Do you want to reset resellers password ?");
                            if (message == true) {
                                document.getElementById('createLoadingImage').src = 'images/loading.gif';
                                $.post("ResetRslPassByAdmin", $("#reset-form").serialize(), function (data) {
                                    document.getElementById('createLoadingImage').src = '';
                                    $("#message").html(data);
                                    showManageRate();
                                });
                            }
                        }
                    }
                });

                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function forPassword(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }
        </script>

    </body>
</html>