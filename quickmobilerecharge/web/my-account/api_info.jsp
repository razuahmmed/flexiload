<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                API <small>API settings & Information's</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cubes"></i>API
                                    </div>

                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="well">

                                        <h4>Your API Key</h4>

                                        <form role="form">
                                            <div class="row">
                                                <div class="col-xs-12">

                                                    <div class="input-group">
                                                        <input type="text" value="sj2y7p65o2gpkm7jw7153adkibv4ngv6qovo3rji"  class="form-control">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn green-haze dropdown-toggle" data-toggle="dropdown"> Action <i class="fa fa-angle-down"></i></button>

                                                            <ul class="dropdown-menu pull-right">
                                                                <li>
                                                                    <a href="#"> Regenerate Key </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#"> Disable API </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <!-- /btn-group -->
                                                    </div>
                                                    <!-- /input-group -->
                                                </div>
                                                <!-- /.col-xs-6 -->

                                                <!-- /.col-xs-6 -->
                                            </div>
                                            <!-- /.row -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cubes"></i>Documentation
                                    </div>

                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="well">

                                        <h4>API Documentation</h4>

                                        <p>
                                            API URL: easyflexiloadsystem.com [Product]/[Base 64 Encrypted Request]
                                            <br>
                                            API Parameters: [Product] : mr = mobile recharge, mm = mobile money<br>
                                            Use mr or mm. e.g. api/mr/... or api/mm/....
                                        </p>

                                        <h4>Example Data Encrypt for mr (Mobile Recharge)</h4>

                                        <code>
                                            $arr = array('user' => 'admin', 'key' => '$1$ZW/.uF5.$.rwCeLiguoBzYzf3waOnY1', 'phone' => '01XXXXXXXXX', 'type' => '0', 'amount' => '10');
                                            $req = json_encode($arr);
                                            $req = base64_encode($req);
                                        </code>

                                        <p>
                                            <br>
                                            <br>
                                            In above example: <br>
                                            user: Enter Username /Reseller ID <br>
                                            key: Enter API Key <br>
                                            phone: Enter Phone Number <br>
                                            type: 0 for Prepaid &amp; 1 for Postpiad <br>
                                            amount: Enter Amount <br>
                                            <br>
                                            <br>
                                            Example Call using GET method 
                                        </p>

                                        easyflexiloadsystem.com $req <br>
                                        Here $req is encrypted data using above example.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>


        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
            });
        </script>

    </body>
</html>