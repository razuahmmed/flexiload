<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/administration/resource_css.jsp" %>

        <script type="text/javascript">

            function changePassword() {

                var curPassword = document.getElementById("curPassword").value;

                var newPassword = document.getElementById("newPassword").value;

                var confirmPassword = document.getElementById("confirmPassword").value;

                if ((!notEmpty(newPassword, "Password can't be empty !")) & (!forPassword(newPassword, "Invalid password Format !")) & (!lengthRestriction(newPassword, 6, 18, "your password"))) {
                    return false;
                }

                if (newPassword != confirmPassword) {
                    alert("Password do not match !");
                    return false;
                } else {

                    var r = confirm("Do you want to change password ?");

                    if (r == true) {

                        var dataString = 'newPassword=' + newPassword;
                        dataString += '&curPassword=' + curPassword;

                        if ((confirmPassword.length != 0) && (newPassword.length != 0)) {

                            document.getElementById('createLoadingImage').src = 'images/loading.gif';

                            $.ajax({
                                type: "POST",
                                url: 'EditPassword',
                                data: dataString,
                                success: function (data) {
                                    document.getElementById('createLoadingImage').src = '';
                                    $("#message").html(data);
                                }
                            });
                        }
                    }
                }
            }
            //
            function forPassword(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //            
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Profile&nbsp;<small>Change&nbsp;Password</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row profile">
                        <div class=" col-md-12  ">
                            <!--BEGIN TABS-->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-green-sharp bold uppercase">Change&nbsp;Password</span>
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable tabbable-custom tabbable-full-width">

                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_3" data-toggle="tab">Account</a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <!--tab_1_2-->
                                            <div class="tab-pane active" id="tab_1_3" >
                                                <div class=" col-md-4  ">
                                                    <div class="tab-content" id="pwd-container">

                                                        <div id="tab_3-3" class="tab-pane active">
                                                            <form>
                                                                <div class="form-group">
                                                                    <label class="control-label">Current&nbsp;Password</label>
                                                                    <input type="text" id="curPassword" name="curPassword" class="form-control"/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New&nbsp;Password</label>
                                                                    <input type="password" id="newPassword" name="newPassword" class="form-control pws"/>
                                                                    <span class="help-block">Please Use a Strong Password</span>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="pwstrength_viewport_progress"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type&nbsp;New&nbsp;Password</label>
                                                                    <input type="password" id="confirmPassword" name="confirmPassword" class="form-control"/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <button type="button" onclick="changePassword();" class="btn green">
                                                                        <i class="icon-check"></i>
                                                                        Change&nbsp;Password
                                                                    </button>
                                                                    <img id="createLoadingImage" src="" alt="" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end tab-pane-->

                                            <!--end tab-pane-->

                                            <!--end tab-pane-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>


        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <%@include file="/administration/resource_js.jsp" %>

        <script src="<%= request.getContextPath()%>/my-js/passwordStrength.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/checkpw.js" type="text/javascript"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
            });
        </script>
    </body>
</html>